# Smart home - Progetto sistemi distribuiti

## Avvio e uso del software
E� possibile avviare il programma con i seguenti parametri:

- *--main*, � la prima opzione da usare, istanzia il MainContainer di Jade, il tuple centre e il SettingsAgent che carica le reactions e inserisce la tupla di una generica stanza in cui posizionare i device quando creati;
- *--gui*, istanzia l�interfaccia utente. E� possibile istanziarla su una macchina diversa da quella del main indicandone l�indirizzo IP tramite l�opzione --tc;
- *--device <device class full name>*, richiede l�istanziazione del device indicato come argomento. Ogni device � istanziato in un container jade apposito, l�uso di questo parametro richiede quindi che in precedenza o in concomitanza sia stato creato il tuple centre e il MainContainer tramite l�opzione --main;
- *--tc <IP address>*, permette di specificare l�indirizzo in cui � stato istanziato il main del programma. E� un attributo obbligatorio per poter avviare l�applicazione su una macchina diversa in maniera distribuita;

Avviare l�applicazione senza parametri corrisponde a usare i flag --main --gui. I parametri vengono gestiti dalla classe ProgramArgumentsHelper che nasconde la logica di parsing.

Di seguito vengono indicati alcuni esempi.

_Primo avvio del programma_

`java -jar sd-smarthome.jar --main`

_Istanziazione del device LightSimple sulla stessa macchina del main_

`java -jar sd-smarthome.jar --device it.unibo.smarthome.devices.impl.LightSimple`

_Istanziazione del device LightSimple sulla macchina locale mentre il main � attivo all�indirizzo 192.168.1.13 e apertura della gui_

`java -jar sd-smarthome.jar --gui --device it.unibo.smarthome.devices.impl.LightSimple --tc 192.168.1.13`


## JADE - Come avviare un agente manualmente

Aprire un terminale nella root del progetto e scrivere
`java -cp lib/jade.jar:bin/ jade.Boot -gui -agents <nome-agente>:<full-class-path>`

Ad esempio:
`java -cp lib/jade.jar:bin/ jade.Boot -gui -agents myTestAgent1:it.unibo.smarthome.agents.SimpleAgentExample`

Ad esempio (su windows non da quella merda di powershell):
`java -cp lib\jade.jar;bin\ jade.Boot -gui -agents myTestAgent1:it.unibo.smarthome.agents.SimpleAgentExample`

Altri agenti possono essere inseriti in fondo al comando separati da `;` oppure possono essere avviati direttamente dalla gui.


## RIASSUNTO INFRASTRUTTURA PROGETTO:

Ogni dispositivo concreto implementa la proprio libreria specifica (es IPhone) che implementa l'interfaccia IDevice, inoltre all'avvio istanzia ogni agente specifico che insieme ai suoi behaviour definisce il comportamento di una famiglia di dispositivi.

Possono essere creati dei dispositivi astratti che fungono da coordinatori di device i quali possono notificare allarmi generici con una logica di "pericolo" comune.

Si � pensato che usare il tc in stile database (es caricando tutte le temperature registrate) non sia corretto ma sia meglio offrire solo il dato corrente del termometro in modo che chi ne � interessato legga il valore quando ritiene opportuno farlo.


## LINK UTILI:

Tucson: http://apice.unibo.it/xwiki/bin/download/TuCSoN/Documents/tucson4jadequickguidepdf.pdf
		http://lia.deis.unibo.it/corsi/2006-2007/SMA-LS/pdf/other/TuCSoN.pdf
		
## Configurazione JAVADOC per Jade

Click destro sul progetto-> Build Path-> Configure Build Path... -> nella tab Libraries aprire Maven Dependencies cliccare su jade-4.5.0.jar selezionare "javadoc location" e cliccare il pulsante Edit presente sulla destra poi verificare che sia selezionata la radio della doc online e nel campo "javadoc location path" inserire http://jade.tilab.com/doc/api

## Creazione jar da Eclipse

Click destro sul progetto-> Export... -> Java-> Runnable JAR.
Il jar creato pu� essere avviato su Windows semplicemente con `java -jar nome-file.jar` oppure `java -jar nome-file.jar --main --gui`. 