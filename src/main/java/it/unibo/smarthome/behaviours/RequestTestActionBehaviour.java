package it.unibo.smarthome.behaviours;

import it.unibo.smarthome.dao.DeviceDAO;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class RequestTestActionBehaviour extends OneShotBehaviour {
	
	private static final long serialVersionUID = 1L;
	
	public static final String EXEC_TEST_ACTION_CMD = "exec-test-action";
	
	private AID deviceAid;
	private String action;
	
	public RequestTestActionBehaviour(DeviceDAO device, String action) {
		super();
		this.deviceAid = new AID(device.getAgent(), true);
		this.action = action;
	}

	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setContent(EXEC_TEST_ACTION_CMD);
		msg.addUserDefinedParameter("action", this.action);
		msg.addReceiver(deviceAid);
		
		this.getAgent().send(msg);
	}
}
