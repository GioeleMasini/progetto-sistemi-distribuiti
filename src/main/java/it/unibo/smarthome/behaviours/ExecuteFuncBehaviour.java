package it.unibo.smarthome.behaviours;

import it.unibo.smarthome.handlers.IHandlerFunc;
import jade.core.behaviours.OneShotBehaviour;

public class ExecuteFuncBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1846977316155835517L;
	private IHandlerFunc handler;

	public ExecuteFuncBehaviour(IHandlerFunc handler) {
		super();
		this.handler = handler;
	}
	
	@Override
	public void action() {
		this.handler.func();
	}
}
