package it.unibo.smarthome.behaviours;

import it.unibo.smarthome.dao.DeviceDAO;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class RequestStopDeviceBehaviour extends OneShotBehaviour {
	
	private static final long serialVersionUID = 1L;
	
	public static final String STOP_DEVICE_CMD = "stop-device";
	
	private AID deviceAid;
	
	public RequestStopDeviceBehaviour(DeviceDAO device) {
		super();
		this.deviceAid = new AID(device.getAgent(), true);
	}

	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setContent(STOP_DEVICE_CMD);
		msg.addReceiver(deviceAid);
		
		this.getAgent().send(msg);
	}
}
