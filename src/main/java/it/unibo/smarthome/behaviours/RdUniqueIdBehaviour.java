package it.unibo.smarthome.behaviours;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.Rdp;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.dao.UniqueIdDAO;
import it.unibo.smarthome.handlers.IHandlerStringResult;
import jade.core.behaviours.SimpleBehaviour;

public class RdUniqueIdBehaviour extends SimpleBehaviour {
	
	private static final long serialVersionUID = -6411005148206273023L;
	
	private boolean done = false;

	private String identifier;
	private IHandlerStringResult handler;
	
	public RdUniqueIdBehaviour(String identifier, IHandlerStringResult handler) {
		super();
		this.identifier = identifier;
		this.handler = handler;
	}

	@Override
	public void action() {
		ITucsonAgent agent = (ITucsonAgent) this.getAgent();
		UniqueIdDAO dao = new UniqueIdDAO(null, identifier);
		try {
			LogicTuple lt = dao.toTuple("X", null);
			agent.actionSync(Rdp.class, lt, this::handleResult, this);
		} catch (InvalidLogicTupleException e) {
			e.printStackTrace();
			done = true;
		}
	}
	
	private void handleResult(TucsonOpCompletionEvent e) {
		if (this.handler != null) {
			// Recupero l'id letto e lo ritorno tramite l'handler
			UniqueIdDAO dao = new UniqueIdDAO(e.getTuple());
			this.handler.handle(dao.getId());
		}
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}

}
