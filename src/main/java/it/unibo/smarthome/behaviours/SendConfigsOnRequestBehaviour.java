package it.unibo.smarthome.behaviours;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.DeviceAgent;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SendConfigsOnRequestBehaviour extends CyclicBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger();
	private static final MessageTemplate SEND_CONFIGS_REQUEST_TEMPLATE = MessageTemplate.and( 
			MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
			MessageTemplate.MatchContent(ReadConfigsBehaviour.SEND_CONFIGS_CMD)
			);
	
	private DeviceAgent agent;

	@Override
	public void onStart() {
		super.onStart();
		this.agent = (DeviceAgent) this.getAgent();
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				SEND_CONFIGS_REQUEST_TEMPLATE, 
				MessageTemplate.MatchReceiver(new AID[] {this.getAgent().getAID()})
				);
		
	    final ACLMessage msg = agent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
	        this.sendConfigs(msg);
	    } else {
	    	// message not received yet: wait
	        block();
	    }
	}

	private void sendConfigs(ACLMessage request) {
		ACLMessage msg = request.createReply();
		msg.setPerformative(ACLMessage.INFORM);
		msg.setLanguage("JavaSerialization");
		msg.setInReplyTo(request.getContent());

		try {
			msg.setContentObject(this.agent.getDevice().getConfigs());
		} catch (IOException e) {
			log.error("Errore nella serializzazione delle configurazioni da inviare: " + e.getMessage());
		}
		
		this.agent.send(msg);
	}
}
