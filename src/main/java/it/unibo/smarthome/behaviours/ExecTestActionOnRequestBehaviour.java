package it.unibo.smarthome.behaviours;

import it.unibo.smarthome.agents.impl.DeviceAgent;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ExecTestActionOnRequestBehaviour extends CyclicBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static final MessageTemplate EXEC_TEST_ACTION_REQUEST_TEMPLATE = MessageTemplate.and( 
			MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
			MessageTemplate.MatchContent(RequestTestActionBehaviour.EXEC_TEST_ACTION_CMD)
			);
	
	private DeviceAgent agent;

	@Override
	public void onStart() {
		super.onStart();
		this.agent = (DeviceAgent) this.getAgent();
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				EXEC_TEST_ACTION_REQUEST_TEMPLATE, 
				MessageTemplate.MatchReceiver(new AID[] {this.getAgent().getAID()})
				);
		
	    final ACLMessage msg = agent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
	        this.doTestAction(msg);
	    } else {
	    	// message not received yet: wait
	        block();
	    }
	}

	private void doTestAction(ACLMessage request) {
		this.agent.getDevice().doTestAction(request.getUserDefinedParameter("action"));
	}
}
