package it.unibo.smarthome.behaviours;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.dao.DeviceDAO;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.Serializable;

public class SaveDeviceConfigsBehaviour extends OneShotBehaviour {

	//public static final String SAVE_CONFIGS_CMD = "save-configs";

	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger();

	private Map<String, String> configs;
	private AID deviceAid;
	
	public SaveDeviceConfigsBehaviour(DeviceDAO device, Map<String, String> configs) {
		super();
		this.deviceAid = new AID(device.getAgent(), true);
		this.configs = configs;
	}

	@Override
	public void action() {
		if (this.configs == null) {
			return;
		}
		if (!(this.configs instanceof Serializable)) {
			log.error("Impossibile inviare le configurazioni del device perch� non serializzabili.");
			return;
		}

		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		//msg.setContent(SAVE_CONFIGS_CMD);
		try {
			msg.setContentObject((Serializable) this.configs);
		} catch (IOException e) {
			log.error("Errore nella serializzazione delle configurazioni da inviare: " + e.getMessage());
			return;
		}
		msg.setLanguage("JavaSerialization");
		msg.addReceiver(deviceAid);
		
		this.getAgent().send(msg);
	}
}
