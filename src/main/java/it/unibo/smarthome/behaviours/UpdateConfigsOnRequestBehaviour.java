package it.unibo.smarthome.behaviours;

import java.io.Serializable;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.DeviceAgent;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class UpdateConfigsOnRequestBehaviour extends CyclicBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger();
	private static final MessageTemplate UPDATE_CONFIGS_REQUEST_TEMPLATE = MessageTemplate.and( 
			MessageTemplate.MatchPerformative(ACLMessage.INFORM),
			MessageTemplate.MatchLanguage("JavaSerialization")
			);
	
	private DeviceAgent agent;

	@Override
	public void onStart() {
		super.onStart();
		this.agent = (DeviceAgent) this.getAgent();
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				UPDATE_CONFIGS_REQUEST_TEMPLATE, 
				MessageTemplate.MatchReceiver(new AID[] {this.getAgent().getAID()})
				);
		
	    final ACLMessage msg = agent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
	        try {
				this.updateConfigs(msg.getContentObject());
			} catch (UnreadableException e) {
				log.error("Errore nella deserializzazione delle config del device: " + e.getMessage());
			}
	    } else {
	    	// message not received yet: wait
	        block();
	    }
	}

	private void updateConfigs(Serializable object) {
		if (!(object instanceof Map)) {
			log.error("L'oggetto inviato non � una mappa. Impossibile aggiornare le configurazioni del device.");
			return;
		}

		@SuppressWarnings("unchecked")
		Map<String, String> newConfigs = (Map<String, String>) object;
		Map<String, String> currentConfigs = this.agent.getDevice().getConfigs();
		newConfigs.forEach((key, value) -> {
			currentConfigs.put(key, value);
		});
		
		// Avviso il device di aggiornarsi in base alle sue config
		this.agent.getDevice().updateConfigs();
	}
}
