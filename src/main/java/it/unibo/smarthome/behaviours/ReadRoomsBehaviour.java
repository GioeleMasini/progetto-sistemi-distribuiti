package it.unibo.smarthome.behaviours;

import java.util.ArrayList;
import java.util.Iterator;

import alice.logictuple.LogicTuple;
import alice.tucson.asynchSupport.actions.ordinary.bulk.RdAll;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.dao.RoomDAO;
import it.unibo.smarthome.handlers.IHandlerDaoList;
import jade.core.behaviours.SimpleBehaviour;

public class ReadRoomsBehaviour extends SimpleBehaviour {
	
	private static final long serialVersionUID = -6411005148206273023L;
	
	private boolean done = false;

	private IHandlerDaoList<RoomDAO> handler;
	
	public ReadRoomsBehaviour(IHandlerDaoList<RoomDAO> handler) {
		super();
		this.handler = handler;
	}

	@Override
	public void action() {
		ITucsonAgent agent = (ITucsonAgent) this.getAgent(); 
		agent.actionSync(RdAll.class, RoomDAO.TEMPLATE, this::listRooms, this);
	}
	
	private void listRooms(TucsonOpCompletionEvent res) {
		ArrayList<RoomDAO> rooms = new ArrayList<RoomDAO>();
		
		Iterator<LogicTuple> it = res.getTupleList().iterator();
		while(it.hasNext()) {
			RoomDAO room = new RoomDAO(it.next());
			rooms.add(room);
		}
		
		handler.handle(rooms);
		
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}

}
