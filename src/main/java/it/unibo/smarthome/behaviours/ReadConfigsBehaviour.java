package it.unibo.smarthome.behaviours;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.handlers.IHandlerConfigs;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReadConfigsBehaviour extends SimpleBehaviour {
	
	public static final String SEND_CONFIGS_CMD = "send-configs";
	
	private static final long serialVersionUID = -6411005148206273023L;
	private static Logger log = LogManager.getLogger();
	
	private static final MessageTemplate SEND_CONFIGS_INFORM_TEMPLATE = MessageTemplate.and(
			MessageTemplate.and(
	            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
	            MessageTemplate.MatchLanguage("JavaSerialization")
            ),
			MessageTemplate.MatchInReplyTo(SEND_CONFIGS_CMD)
            );
	
	private IHandlerConfigs handler; 
	private AID deviceAid;
	private boolean done = false;
	
	public ReadConfigsBehaviour(DeviceDAO device, IHandlerConfigs handler) {
		super();
		this.deviceAid = new AID(device.getAgent(), true);
		this.handler = handler;
	}

	@Override
	public void onStart() {
		super.onStart();

		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setContent(SEND_CONFIGS_CMD);
		msg.addReceiver(deviceAid);
		
		this.getAgent().send(msg);
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(SEND_CONFIGS_INFORM_TEMPLATE, MessageTemplate.MatchSender(this.deviceAid));
	    final ACLMessage msg = myAgent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
	        this.listConfigs(msg);
	    } else {
	    	// message not received yet: wait
	        block();
	    }
	}
	
	@SuppressWarnings("unchecked")
	private void listConfigs(ACLMessage msg) {
		Object content = null;
		Map<String, String> configs = new HashMap<String, String>();
		
		try {
			content = msg.getContentObject();
		} catch (UnreadableException e) {
			log.error("Non � arrivata una HashMap come risposta a send-configs: " + e.getMessage());
		}
		
		if (content != null && content instanceof Map) {
			configs = (Map<String, String>) content;
		}
		
		handler.handle(configs);
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}
}
