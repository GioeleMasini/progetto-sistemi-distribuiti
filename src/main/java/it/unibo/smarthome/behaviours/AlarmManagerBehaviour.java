package it.unibo.smarthome.behaviours;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import alice.tucson.asynchSupport.actions.ordinary.bulk.InAll;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.impl.AlarmAgent;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_ACTIVITY_STATE;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_FSM_EXIT_CODE;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_FSM_STATE;
import it.unibo.smarthome.dao.AlarmAlertNotifyDAO;
import it.unibo.smarthome.dao.AlarmChangeStateDAO;
import it.unibo.smarthome.dao.AlarmHomeActiveDAO;
import it.unibo.smarthome.dao.PhoneTextMsgDAO;
import it.unibo.smarthome.devices.IAlarm;
import it.unibo.smarthome.exception.AuthenticationFailException;
import it.unibo.tucson.jade.coordination.AsynchTucsonOpResult;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;

/**
 * <p>Finite State Machine Behaviour che gestisce l'intero processo di vita
 * dell'allarme.</p>
 * 
 * <p>WaitActiveStateBehaviour: rimane in attesa di tuple di accensione allarme (AlarmChangeStateDAO); 
 * 							 passa poi a TransitionToOnBehaviour.</p>
 * 
 * <p>TransitionToOnBehaviour: pulisce il tuple centre da vecchie tuple di attivazione allarme (AlarmAlertNotifyDAO)
 * 							e scrive la tupla di casa allarmata (AlarmHomeActiveDAO); passa poi a OnBehaviour.</p>
 * 
 * <p>OnBehaviour: un ParallelBehaviour, contiene:
 * <ul>
 * 	<li>   WaitAlarmBehaviour: rimane in ascolto di tuple di attivazione allarme (AlarmAlertNotifyDAO) per
 * 							   far suonare l'allarme; passa poi a RingBehaviour.</li>
 * 	<li>  WaitOffStateBehaviour: rimane in ascolto di tuple di spegnimento allarme (AlarmChangeStateDAO);
 * 								 passa poi a TransitionToOffBehaviour.</li>
 * </ul>
 * </p>
 * 
 * <p>RingBehaviour: WakerBehaviour, al suo risveglio (rimane addormentato per il tempo definito dalle config del device
 * 				  allarme) spegne in modo automatico l'allarme; passa poi a TransitionToOffBehaviour.</p>
 * 
 * <p>TransitionToOffBehaviour: pulisce il tuple centre da eventuali tuple di attivazione allarme (AlarmAlertNotifyDAO)
 * 							 e dalla tupla di casa allarmata (AlarmHomeActiveDAO); passa poi a WaitActiveStateBehaviour.</p>
 */
public class AlarmManagerBehaviour extends FSMBehaviour {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	private AlarmAgent agent;
	
	public AlarmManagerBehaviour(AlarmAgent agent) {
		this.agent = agent;
		
		this.registerStates();
		this.registerTransitions();
	}
	
	private void registerStates() {
		this.registerFirstState(new WaitActiveStateBehaviour(), ALARM_FSM_STATE.WAIT_ACTIVE_STATE.getValue());
		this.registerState(new TransitionToOnBehaviour(), ALARM_FSM_STATE.TRANSITION_TO_ON.getValue());
		this.registerState(new OnBehaviour(), ALARM_FSM_STATE.ON.getValue());
		this.registerState(new RingBehaviour(AlarmManagerBehaviour.this.agent.getAlarm().getRingingTime()), ALARM_FSM_STATE.RING.getValue());
		this.registerState(new TransitionToOffBehaviour(), ALARM_FSM_STATE.TRANSITION_TO_OFF.getValue());
	}
	
	private void registerTransitions() {
		// WAIT ACTIVE STATE -> TRANSITION TO ON
		this.registerTransition(
				ALARM_FSM_STATE.WAIT_ACTIVE_STATE.getValue(),
				ALARM_FSM_STATE.TRANSITION_TO_ON.getValue(),
				ALARM_FSM_EXIT_CODE.TRANSITION_TO_ON.getValue());
		
		// TRANSITION TO ON -> ON
		this.registerTransition(
				ALARM_FSM_STATE.TRANSITION_TO_ON.getValue(),
				ALARM_FSM_STATE.ON.getValue(),
				ALARM_FSM_EXIT_CODE.ON.getValue());
		
		// ON -> RING
		this.registerTransition(
				ALARM_FSM_STATE.ON.getValue(),
				ALARM_FSM_STATE.RING.getValue(),
				ALARM_FSM_EXIT_CODE.RING.getValue());
		
		// ON -> TRANSITION TO OFF
		this.registerTransition(
				ALARM_FSM_STATE.ON.getValue(),
				ALARM_FSM_STATE.TRANSITION_TO_OFF.getValue(),
				ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF.getValue());
		
		// RING -> TRANSITION TO OFF
		this.registerTransition(
				ALARM_FSM_STATE.RING.getValue(),
				ALARM_FSM_STATE.TRANSITION_TO_OFF.getValue(),
				ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF.getValue());
		
		// TRANSITION TO OFF -> WAIT ACTIVE STATE
		this.registerTransition(
				ALARM_FSM_STATE.TRANSITION_TO_OFF.getValue(),
				ALARM_FSM_STATE.WAIT_ACTIVE_STATE.getValue(),
				ALARM_FSM_EXIT_CODE.WAIT_ACTIVE_STATE.getValue());
		
	}

	public void updateRingingTime() {
		// Deregistro prima le transizioni verso e da il RingBehaviour
		this.deregisterTransition(
				ALARM_FSM_STATE.ON.getValue(), 
				ALARM_FSM_EXIT_CODE.RING.getValue());
		this.deregisterTransition(
				ALARM_FSM_STATE.RING.getValue(),
				ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF.getValue());
		
		// Deregistro poi lo stato RingBehaviour
		this.deregisterState(ALARM_FSM_STATE.RING.getValue());
		
		// Lo registro nuovamente, con il nuovo ringing time
		this.registerState(new RingBehaviour(AlarmManagerBehaviour.this.agent.getAlarm().getRingingTime()), ALARM_FSM_STATE.RING.getValue());

		// Registro nuovamente le transizioni verso e da il RingBehaviour
		// ON -> RING
		this.registerTransition(
				ALARM_FSM_STATE.ON.getValue(),
				ALARM_FSM_STATE.RING.getValue(),
				ALARM_FSM_EXIT_CODE.RING.getValue());

		// RING -> TRANSITION TO OFF
		this.registerTransition(
				ALARM_FSM_STATE.RING.getValue(),
				ALARM_FSM_STATE.TRANSITION_TO_OFF.getValue(),
				ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF.getValue());
		
		log.info("RingBehaviour aggiornato con successo.");
	}
	
	/////// CLASS BEHAVIOURS START ///////
	class WaitActiveStateBehaviour extends SimpleBehaviour {
		private static final long serialVersionUID = 6623873290836268222L;
		private ALARM_FSM_EXIT_CODE onEndExitState;

		@Override
		public void action() {
			this.onEndExitState = ALARM_FSM_EXIT_CODE.WAIT_ACTIVE_STATE;
			try {
				LogicTuple template = (new AlarmChangeStateDAO(ALARM_ACTIVITY_STATE.ON, null)).toTuple(null, "X");
				AlarmManagerBehaviour.this.agent.actionSync(In.class, template, this::handleChangeState, this);
			} catch (InvalidLogicTupleException e) {
				log.info("Template tupla allarme invalida.");
			}
		}
		
		private void handleChangeState(TucsonOpCompletionEvent res) {
			AlarmChangeStateDAO dao = new AlarmChangeStateDAO(res.getTuple());
			ALARM_ACTIVITY_STATE toStato = dao.getState();
			String pwd = dao.getPassword();
			
			IAlarm alarm = AlarmManagerBehaviour.this.agent.getAlarm();
			try {
				alarm.setState(toStato, pwd);
				this.onEndExitState = ALARM_FSM_EXIT_CODE.TRANSITION_TO_ON;
			} catch (AuthenticationFailException e) {
				log.info("Tentativo di cambio stato allarme fallito: Password errata.");
			}
		}

		@Override
		public boolean done() {
			return ALARM_FSM_EXIT_CODE.TRANSITION_TO_ON.equals(this.onEndExitState);
		}

		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}
	}
	
	class TransitionToOnBehaviour extends SimpleBehaviour {
		private static final long serialVersionUID = -3844982553144881276L;
		private boolean alreadyActed = false;
		private ALARM_FSM_EXIT_CODE onEndExitState;
		private AsynchTucsonOpResult alarmAlertNotifyRes;
		private AsynchTucsonOpResult alarmHomeActiveRes;
		private TucsonOpCompletionEvent alarmAlertNotifyEv;
		private TucsonOpCompletionEvent alarmHomeActiveEv;

		@Override
		public void action() {
			this.onEndExitState = ALARM_FSM_EXIT_CODE.TRANSITION_TO_ON;
			if (!this.alreadyActed) {
				// tolgo dal tuple centre eventuali tuple di attivazione allarme
				this.alarmAlertNotifyRes = AlarmManagerBehaviour.this.agent.actionAsync(InAll.class, AlarmAlertNotifyDAO.TEMPLATE);

				// scrivo sul tuple centre lo stato di allarme inserito
				this.alarmHomeActiveRes = AlarmManagerBehaviour.this.agent.actionAsync(Out.class, new AlarmHomeActiveDAO());
				this.alreadyActed = true;
			} else {
				if (this.alarmAlertNotifyEv == null) {
					this.alarmAlertNotifyEv = this.alarmAlertNotifyRes.getTucsonCompletionEvent(this.alarmAlertNotifyRes.getOpId());
				}
				if (this.alarmHomeActiveEv == null) {
					this.alarmHomeActiveEv = this.alarmHomeActiveRes.getTucsonCompletionEvent(this.alarmHomeActiveRes.getOpId());
				}
				if (this.alarmAlertNotifyEv != null && this.alarmHomeActiveEv != null) {
					this.onEndExitState = ALARM_FSM_EXIT_CODE.ON;
				}
			}
		}

		@Override
		public boolean done() {
			boolean isDone = ALARM_FSM_EXIT_CODE.ON.equals(this.onEndExitState);
			if(isDone) {
				this.alreadyActed = false;
				this.alarmAlertNotifyRes = null;
				this.alarmHomeActiveRes = null;
				this.alarmAlertNotifyEv = null;
				this.alarmHomeActiveEv  = null;
			}
			return isDone;
		}

		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}
	}
	
	class OnBehaviour extends ParallelBehaviour {
		private static final long serialVersionUID = 3571021590328318509L;

		public OnBehaviour() {
			super(ParallelBehaviour.WHEN_ANY);
			this.addSubBehaviour(new WaitAlarmBehaviour());
			this.addSubBehaviour(new WaitOffStateBehaviour());
		}
		
		@Override
		public int onEnd() {
			int exitState = -1;
			Iterator<?> it = this.getTerminatedChildren().iterator();
			while (it.hasNext()) {
				Object elem = it.next();
				if (elem instanceof Behaviour) {
					Behaviour b = (Behaviour) elem;
					int bExitState = b.onEnd();
					if (ALARM_FSM_EXIT_CODE.RING.equals(bExitState) || ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF.equals(bExitState)) {
						exitState = bExitState;
					}
				}
			}
			return exitState;
		}
	}
	
	class WaitAlarmBehaviour extends SimpleBehaviour {
		private static final long serialVersionUID = -6387111390034523914L;
		private ALARM_FSM_EXIT_CODE onEndExitState;
		private AsynchTucsonOpResult phoneTextMsgRes;
		private TucsonOpCompletionEvent phoneTextMsgEv;

		@Override
		public void action() {
			this.onEndExitState = ALARM_FSM_EXIT_CODE.ON;
			if (this.phoneTextMsgRes == null && this.phoneTextMsgEv == null) {
				AlarmManagerBehaviour.this.agent.actionSync(Rd.class, AlarmAlertNotifyDAO.TEMPLATE, this::handleAvvioAllarme, this);
			}
			
			if (this.phoneTextMsgRes != null && this.phoneTextMsgEv == null) {
				this.phoneTextMsgEv = this.phoneTextMsgRes.getTucsonCompletionEvent(this.phoneTextMsgRes.getOpId());
			}
			
			if (this.phoneTextMsgRes != null && this.phoneTextMsgEv != null) {
				this.onEndExitState = ALARM_FSM_EXIT_CODE.RING;
			}
		}
		
		public void handleAvvioAllarme(TucsonOpCompletionEvent res) {
			if (res != null && res.resultOperationSucceeded()) {
				System.out.println("AlarmAgent: allarme attivato - Avvio protoccolo allarme.");
				
				// AVVIO PROTOCOLLO ALLARME 
				IAlarm alarm = AlarmManagerBehaviour.this.agent.getAlarm();
				// TODO gestire qua lo spam del suono che va a bombazza
				alarm.enableAlarm();
				
				//invio SMS alla polizia
				PhoneTextMsgDAO dao = new PhoneTextMsgDAO(
						alarm.getEmergencyNumber(),
						alarm.getEmergencyText()
					);
				this.phoneTextMsgRes = AlarmManagerBehaviour.this.agent.actionAsync(Out.class, dao);
			}
		}

		@Override
		public boolean done() {
			boolean isDone = ALARM_FSM_EXIT_CODE.RING.equals(this.onEndExitState);
			if(isDone) {
				this.phoneTextMsgRes = null;
				this.phoneTextMsgEv = null;
			}
			return isDone;
		}

		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}
	}
	
	class WaitOffStateBehaviour extends SimpleBehaviour {
		private static final long serialVersionUID = 417084326952270132L;
		private ALARM_FSM_EXIT_CODE onEndExitState;

		@Override
		public void action() {
			this.onEndExitState = ALARM_FSM_EXIT_CODE.ON;
			try {
				LogicTuple template = (new AlarmChangeStateDAO(ALARM_ACTIVITY_STATE.OFF, null)).toTuple(null, "X");
				AlarmManagerBehaviour.this.agent.actionSync(In.class, template, this::handleChangeState, this);
			} catch (InvalidLogicTupleException e) {
				log.info("Template tupla allarme invalida.");
			}
		}
		
		private void handleChangeState(TucsonOpCompletionEvent res) {
			AlarmChangeStateDAO dao = new AlarmChangeStateDAO(res.getTuple());
			ALARM_ACTIVITY_STATE toStato = dao.getState();
			String pwd = dao.getPassword();
			
			IAlarm alarm = AlarmManagerBehaviour.this.agent.getAlarm();
			try {
				alarm.setState(toStato, pwd);
				this.onEndExitState = ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF;
			} catch (AuthenticationFailException e) {
				log.info("Tentativo di cambio stato allarme fallito: Password errata.");
			}
		}

		@Override
		public boolean done() {
			return ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF.equals(this.onEndExitState);
		}

		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}
	}
	
	class RingBehaviour extends WakerBehaviour {
		private static final long serialVersionUID = 365868492878447785L;
		private ALARM_FSM_EXIT_CODE onEndExitState;

		public RingBehaviour(long timeout) {
			super(AlarmManagerBehaviour.this.agent, timeout);
		}
		
		@Override
		protected void onWake() {
			log.info("Timeout. Disattivazione automatica allarme.");
			IAlarm alarm = AlarmManagerBehaviour.this.agent.getAlarm();
			try {
				alarm.setState(ALARM_ACTIVITY_STATE.OFF, alarm.getPassword());
				this.onEndExitState = ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF;
			} catch (AuthenticationFailException e) {
				log.info("Tentativo di cambio stato allarme fallito: Password errata.");
			}
		}

		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}	
		
	}
	
	class TransitionToOffBehaviour extends SimpleBehaviour {
		private static final long serialVersionUID = 2471722727382814323L;
		private boolean alreadyActed = false;
		private ALARM_FSM_EXIT_CODE onEndExitState;
		private AsynchTucsonOpResult alarmAlertNotifyRes;
		private AsynchTucsonOpResult alarmHomeActiveRes;
		private TucsonOpCompletionEvent alarmAlertNotifyEv;
		private TucsonOpCompletionEvent alarmHomeActiveEv;

		@Override
		public void action() {
			this.onEndExitState = ALARM_FSM_EXIT_CODE.TRANSITION_TO_OFF;
			if (!this.alreadyActed) {
				// tolgo dal tuple centre eventuali tuple di attivazione allarme
				this.alarmAlertNotifyRes = AlarmManagerBehaviour.this.agent.actionAsync(InAll.class, AlarmAlertNotifyDAO.TEMPLATE);

				// rimuovo sul tuple centre lo stato di allarme inserito
				this.alarmHomeActiveRes = AlarmManagerBehaviour.this.agent.actionAsync(In.class, new AlarmHomeActiveDAO());
				this.alreadyActed = true;
			} else {
				if (this.alarmAlertNotifyEv == null) {
					this.alarmAlertNotifyEv = this.alarmAlertNotifyRes.getTucsonCompletionEvent(this.alarmAlertNotifyRes.getOpId());
				}
				if (this.alarmHomeActiveEv == null) {
					this.alarmHomeActiveEv = this.alarmHomeActiveRes.getTucsonCompletionEvent(this.alarmHomeActiveRes.getOpId());
				}
				if (this.alarmAlertNotifyEv != null && this.alarmHomeActiveEv != null) {
					this.onEndExitState = ALARM_FSM_EXIT_CODE.WAIT_ACTIVE_STATE;
				}
			}
		}

		@Override
		public boolean done() {
			boolean isDone = ALARM_FSM_EXIT_CODE.WAIT_ACTIVE_STATE.equals(this.onEndExitState);
			if(isDone) {
				this.alreadyActed = false;
				this.alarmAlertNotifyRes = null;
				this.alarmHomeActiveRes = null;
				this.alarmAlertNotifyEv = null;
				this.alarmHomeActiveEv  = null;
				//effettuto il reset degli stati del ciclo che lo necessitano
				String[] temp = {ALARM_FSM_STATE.ON.getValue(), ALARM_FSM_STATE.RING.getValue()};
				AlarmManagerBehaviour.this.resetStates(temp);
			}
			return isDone;
		}

		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}		
	}
	/////// CLASS BEHAVIOURS END ///////
}
