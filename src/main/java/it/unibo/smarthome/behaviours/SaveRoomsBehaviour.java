package it.unibo.smarthome.behaviours;

import java.util.Iterator;
import java.util.List;

import alice.tucson.asynchSupport.actions.ordinary.bulk.OutAll;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.dao.RoomDAO;
import it.unibo.smarthome.handlers.IHandlerDaoList;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;

public class SaveRoomsBehaviour extends SequentialBehaviour {
	private static final long serialVersionUID = -783101280957163537L;

	public SaveRoomsBehaviour(List<RoomDAO> rooms, IHandlerDaoList<RoomDAO> handler) {
		super();
		
		// Imposto un id univoco a tutte le stanze che non ce l'hanno prima di salvarle
		ParallelBehaviour idsBehaviour = new ParallelBehaviour();
		Iterator<RoomDAO> it = rooms.iterator();
		while (it.hasNext()) {
			RoomDAO room = it.next();
			if (room.getId() == null) {
				RdUniqueIdBehaviour idBehaviour = new RdUniqueIdBehaviour("room", (id) -> {
					room.setId(id);
				});
				idsBehaviour.addSubBehaviour(idBehaviour);
			}
		}
		
		this.addSubBehaviour(idsBehaviour);
		this.addSubBehaviour(new SaveRoomsInternalBehaviour(rooms, handler));
	}
	
	private class SaveRoomsInternalBehaviour extends OneShotBehaviour {
		
		private static final long serialVersionUID = 7422714164653539428L;
		
		private List<RoomDAO> rooms;

		private IHandlerDaoList<RoomDAO> handler;
		
		public SaveRoomsInternalBehaviour(List<RoomDAO> rooms, IHandlerDaoList<RoomDAO> handler) {
			super();
			this.rooms = rooms;
			this.handler = handler;
		}

		@Override
		public void action() {
			this.saveRooms();
		}
		
		private void saveRooms() {
			// Aggiungo tutte le nuove tuple room nel tuple center
			// Le vecchie vengono tolte dalla reaction
			ITucsonAgent agent = (ITucsonAgent) this.getAgent();
			agent.actionAsync(OutAll.class, this.rooms);
		}

		@Override
		public int onEnd() {
			if (this.handler != null) {
				handler.handle(this.rooms);
			}
			return super.onEnd();
		}
	}
}
