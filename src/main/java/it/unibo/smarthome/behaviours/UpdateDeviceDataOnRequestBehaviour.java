package it.unibo.smarthome.behaviours;

import alice.tucson.asynchSupport.actions.ordinary.Out;
import it.unibo.smarthome.agents.impl.DeviceAgent;
import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.devices.IDevice;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class UpdateDeviceDataOnRequestBehaviour extends CyclicBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static final MessageTemplate UPDATE_DATA_REQUEST_TEMPLATE = MessageTemplate.and( 
			MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
			MessageTemplate.MatchContent(RequestUpdateDeviceDataBehaviour.UPDATE_DATA_CMD)
			);
	
	private DeviceAgent agent;

	@Override
	public void onStart() {
		super.onStart();
		this.agent = (DeviceAgent) this.getAgent();
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				UPDATE_DATA_REQUEST_TEMPLATE, 
				MessageTemplate.MatchReceiver(new AID[] {this.getAgent().getAID()})
				);
		
	    final ACLMessage msg = agent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
			this.updateDeviceData(msg);
	    } else {
	    	// message not received yet: wait
	        block();
	    }
	}

	private void updateDeviceData(ACLMessage msg) {
		IDevice device = this.agent.getDevice();
		String name = msg.getUserDefinedParameter(RequestUpdateDeviceDataBehaviour.DEVICE_NAME_PARAM);
		String roomId = msg.getUserDefinedParameter(RequestUpdateDeviceDataBehaviour.ROOM_ID_PARAM);

		if (name != null) {
			device.setName(name);
		}
		if (roomId != null) {
			device.setRoomId(roomId);
		}
		
		DeviceDAO dao = new DeviceDAO(device.getId(), device.getName(), device.getClass(), this.agent.getName(), device.getRoomId());
		agent.actionAsync(Out.class, dao);
	}
}
