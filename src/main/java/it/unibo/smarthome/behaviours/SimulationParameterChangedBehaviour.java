package it.unibo.smarthome.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.gui.SimulationManagerPane;
import jade.core.behaviours.SimpleBehaviour;

public class SimulationParameterChangedBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 2970166919003334538L;

	private static Logger log = LogManager.getLogger();
	
	private SimulationManagerPane simulationPane;
	private boolean done = false;
	
	public SimulationParameterChangedBehaviour(SimulationManagerPane simulationPane) {
		this.simulationPane = simulationPane;
	}

	@Override
	public void action() {
		String timeSpeed = this.simulationPane.getTimeSpeed();
		String todayDate = this.simulationPane.getTodayDate();
		String nowHour = this.simulationPane.getNowHour();
		
		log.info("Mi dicono che sono cambiati i settings, sarà vero?");
		log.info("Velocità " + timeSpeed);
		log.info("Velocità " + todayDate);
		log.info("Velocità " + nowHour);
		this.done = true;
	}

	@Override
	public boolean done() {
		return this.done;
	}

}
