package it.unibo.smarthome.behaviours;

import it.unibo.smarthome.agents.impl.DeviceAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class StopDeviceOnRequestBehaviour extends SimpleBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static final MessageTemplate STOP_DEVICE_REQUEST_TEMPLATE = MessageTemplate.and( 
			MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
			MessageTemplate.MatchContent(RequestStopDeviceBehaviour.STOP_DEVICE_CMD)
			);
	
	private DeviceAgent agent;
	private boolean done;

	@Override
	public void onStart() {
		super.onStart();
		this.agent = (DeviceAgent) this.getAgent();
		this.done = false;
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				STOP_DEVICE_REQUEST_TEMPLATE, 
				MessageTemplate.MatchReceiver(new AID[] {this.getAgent().getAID()})
				);
		
	    final ACLMessage msg = agent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
	    	this.done = true;
		} else {
	    	// message not received yet: wait
	        block();
	    }
	}
	
	@Override
	public int onEnd() {
		this.agent.getDevice().dispose();
		return 0;
	}

	@Override
	public boolean done() {
		return this.done;
	}
}
