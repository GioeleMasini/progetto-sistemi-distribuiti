package it.unibo.smarthome.behaviours;

import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.impl.DeviceAgent;
import it.unibo.smarthome.dao.DeviceDAO;
import jade.core.behaviours.SimpleBehaviour;

public class SetupDeviceBehaviour extends SimpleBehaviour {
	
	private static final long serialVersionUID = -6411005148206273023L;
	
	private boolean done;
	
	public SetupDeviceBehaviour() {
		super();
		this.done = false;
	}

	@Override
	public void action() {
		
		DeviceAgent deviceAgent = (DeviceAgent) this.getAgent();
		String deviceId = deviceAgent.getDevice().getId();
		String deviceName = deviceAgent.getDevice().getName();
		String roomId = deviceAgent.getDevice().getRoomId();
		//String deviceType = agent.getDevice().getClass().getSimpleName();
		DeviceDAO dao = new DeviceDAO(deviceId, deviceName, deviceAgent.getDevice().getClass(), deviceAgent.getName(), roomId);
		
		deviceAgent.actionSync(Out.class, dao, this::operationCompleted, this);
	}
	
	public void operationCompleted(TucsonOpCompletionEvent res) {
		if(res.operationSucceeded()) {
			this.done = true;
		}
	}

	@Override
	public boolean done() {
		return this.done;
	}

}
