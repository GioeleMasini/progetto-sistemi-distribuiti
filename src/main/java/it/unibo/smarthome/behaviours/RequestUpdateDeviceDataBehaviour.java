package it.unibo.smarthome.behaviours;

import it.unibo.smarthome.dao.DeviceDAO;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class RequestUpdateDeviceDataBehaviour extends OneShotBehaviour {
	
	private static final long serialVersionUID = 1L;

	public static final String UPDATE_DATA_CMD = "update-data";
	public static final String ROOM_ID_PARAM = "roomId";
	public static final String DEVICE_NAME_PARAM = "name";
	
	private AID deviceAid;
	private DeviceDAO device;
	
	public RequestUpdateDeviceDataBehaviour(DeviceDAO device) {
		super();
		this.deviceAid = new AID(device.getAgent(), true);
		this.device = device;
	}

	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setContent(UPDATE_DATA_CMD);
		msg.addUserDefinedParameter(ROOM_ID_PARAM, this.device.getRoomId());
		msg.addUserDefinedParameter(DEVICE_NAME_PARAM, this.device.getName());
		msg.addReceiver(deviceAid);
		
		this.getAgent().send(msg);
	}
}
