package it.unibo.smarthome.behaviours;

import java.util.ArrayList;
import java.util.Iterator;

import alice.logictuple.LogicTuple;
import alice.tucson.asynchSupport.actions.ordinary.bulk.RdAll;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.handlers.IHandlerDaoList;
import jade.core.behaviours.SimpleBehaviour;

public class ReadDevicesBehaviour extends SimpleBehaviour {
	private static final long serialVersionUID = 4846108254139279174L;

	private boolean done = false;

	private IHandlerDaoList<DeviceDAO> handler;
	
	public ReadDevicesBehaviour(IHandlerDaoList<DeviceDAO> handler) {
		super();
		this.handler = handler;
	}

	@Override
	public void action() {
		ITucsonAgent agent = (ITucsonAgent) this.getAgent(); 
		agent.actionSync(RdAll.class, DeviceDAO.TEMPLATE, this::listDevices, this);
	}
	
	private void listDevices(TucsonOpCompletionEvent res) {
		ArrayList<DeviceDAO> devices = new ArrayList<DeviceDAO>();
		
		Iterator<LogicTuple> it = res.getTupleList().iterator();
		while(it.hasNext()) {
			DeviceDAO device = new DeviceDAO(it.next());
			devices.add(device);
		}
		
		handler.handle(devices);
		
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}

}
