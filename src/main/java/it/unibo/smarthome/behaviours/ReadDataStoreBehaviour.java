package it.unibo.smarthome.behaviours;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.handlers.IHandlerDataStore;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReadDataStoreBehaviour extends SimpleBehaviour {
	
	public static final String SEND_DATASTORE_CMD = "send-datastore";
	
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger();
	
	private static final MessageTemplate SEND_DATASTORE_INFORM_TEMPLATE = MessageTemplate.and(
			MessageTemplate.and(
	            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
	            MessageTemplate.MatchLanguage("JavaSerialization")
            ),
			MessageTemplate.MatchInReplyTo(SEND_DATASTORE_CMD)
            );
	
	private IHandlerDataStore handler; 
	private AID deviceAid;
	private boolean done = false;
	
	public ReadDataStoreBehaviour(DeviceDAO device, IHandlerDataStore handler) {
		super();
		this.deviceAid = new AID(device.getAgent(), true);
		this.handler = handler;
	}

	@Override
	public void onStart() {
		super.onStart();

		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setContent(SEND_DATASTORE_CMD);
		msg.addReceiver(this.deviceAid);
		
		this.getAgent().send(msg);
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(SEND_DATASTORE_INFORM_TEMPLATE, MessageTemplate.MatchSender(this.deviceAid));
	    final ACLMessage msg = myAgent.receive(mt);
	    if (msg != null) {
	    	// message received: process it
	        this.listConfigs(msg);
	    } else {
	    	// message not received yet: wait
	        block();
	    }
	}
	
	@SuppressWarnings("unchecked")
	private void listConfigs(ACLMessage msg) {
		Object content = null;
		Map<String, Object> configs = new HashMap<String, Object>();
		
		try {
			content = msg.getContentObject();
		} catch (UnreadableException e) {
			log.error("Non � arrivata una HashMap come risposta a send-datastore: " + e.getMessage());
		}
		
		if (content != null && content instanceof Map) {
			configs = (Map<String, Object>) content;
		}
		
		handler.handle(configs);
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}
}
