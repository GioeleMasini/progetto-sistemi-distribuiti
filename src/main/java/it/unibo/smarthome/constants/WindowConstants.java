package it.unibo.smarthome.constants;

public class WindowConstants {

	public static enum WINDOW_STATE {
		OPEN("OPEN"),
		CLOSE("CLOSE");
		
		private String value;
		
		private WINDOW_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static WINDOW_STATE byValue(String value) {
			for (WINDOW_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum ALARM_MODE_STATE {
		ENABLE("ENABLE"),
		DISABLE("DISABLE");

		private String value;

		private ALARM_MODE_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static ALARM_MODE_STATE byValue(String value) {
			for (ALARM_MODE_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum WINDOW_ALARM_HOME_STATE {
		ENABLE("ENABLE"),
		DISABLE("DISABLE");

		private String value;

		private WINDOW_ALARM_HOME_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static WINDOW_ALARM_HOME_STATE byValue(String value) {
			for (WINDOW_ALARM_HOME_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum WINDOW_ALARM_EXIT_CODE {
		ENABLE(11),
		DISABLE(12);

		private int value;

		private WINDOW_ALARM_EXIT_CODE(int v) {
			value = v;
		}

		@Override
		public String toString() {
			return "" + getValue();
		}

		public int getValue() {
			return value;
		}

		public static WINDOW_ALARM_EXIT_CODE byValue(int value) {
			for (WINDOW_ALARM_EXIT_CODE enumItem : values()) {
				if (enumItem.getValue() == value) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum WINDOW_DATA_STORE_KEYS {
		STATE_WINDOW("modalità della finestra"),
		STATE_ALARM("modalità dell'allarme");

		private String value;

		private WINDOW_DATA_STORE_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static WINDOW_DATA_STORE_KEYS byValue(String value) {
			for (WINDOW_DATA_STORE_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
}
