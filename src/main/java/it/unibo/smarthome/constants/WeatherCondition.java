package it.unibo.smarthome.constants;

public enum WeatherCondition {
	SUNNY(0, 0, 0, 0), CLOUDY(10, 0, 20, 30), RAINY(25, 0, 80, 60), FOGGY(10, 0, 60, 60);
	
	private int temperature;
	private int humidity;
	private int wind;
	private int brightness;

	WeatherCondition(int temperature, int wind, int humidity, int brightness) {
		this.temperature = temperature;
		this.wind = wind;
		this.humidity = humidity;
		this.brightness = brightness;
	}
	
	public int getTemperature() {
		return temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public int getWind() {
		return wind;
	}

	public int getBrightness() {
		return brightness;
	}
}
