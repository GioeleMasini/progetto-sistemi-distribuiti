package it.unibo.smarthome.constants;

public class ThermometerConstants {

	public static enum THERMOMETER_DATA_STORE_KEYS {
		ACTUAL_TEMPERATURE("actual temperature");

		private String value;

		private THERMOMETER_DATA_STORE_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static THERMOMETER_DATA_STORE_KEYS byValue(String value) {
			for (THERMOMETER_DATA_STORE_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
}
