package it.unibo.smarthome.constants;

public enum EnvVariable {
	TEMPERATURE, WIND, HUMIDITY, BRIGHTNESS, TIME
}
