package it.unibo.smarthome.constants;

public class TemperatureConstants {

	public static final double TEMP_RANGE_UPDATE_DEFAULT = 0.5;
	
	public static enum TEMP_THERM_SMART_STATE {
		WATING("WATING"),
		UPDATING("UPDATING");

		private String value;

		private TEMP_THERM_SMART_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static TEMP_THERM_SMART_STATE byValue(String value) {
			for (TEMP_THERM_SMART_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum TEMP_THERM_SMART_EXIT_CODE {
		WAIT(10),
		UPDATE(11);

		private int value;

		private TEMP_THERM_SMART_EXIT_CODE(int v) {
			value = v;
		}

		@Override
		public String toString() {
			return "" + getValue();
		}

		public int getValue() {
			return value;
		}

		public static TEMP_THERM_SMART_EXIT_CODE byValue(int value) {
			for (TEMP_THERM_SMART_EXIT_CODE enumItem : values()) {
				if (enumItem.getValue() == value) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum SENSOR_TEMP_CONFIG_KEYS{
		TEMP_RANGE_UPDATE_CHANGE("temp range update change");
		
		private String value;
		
		private SENSOR_TEMP_CONFIG_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static SENSOR_TEMP_CONFIG_KEYS byValue(String value) {
			for (SENSOR_TEMP_CONFIG_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
}
