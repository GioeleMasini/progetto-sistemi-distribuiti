package it.unibo.smarthome.constants;

public class LightConstants {

	public static enum LIGHT_STATE {
		ON("ON"),
		OFF("OFF");
		
		private String value;
		
		private LIGHT_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static LIGHT_STATE byValue(String value) {
			for (LIGHT_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum LIGHT_EXIT_CODE {
		ON(1),
		OFF(2);
		
		private int value;
		
		private LIGHT_EXIT_CODE(int value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "" + getValue();
		}

		public int getValue() {
			return value;
		}

		public static LIGHT_EXIT_CODE byValue(int value) {
			for (LIGHT_EXIT_CODE enumItem : values()) {
				if (enumItem.getValue() == value) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum LIGHT_ACTIVATION {
		MOVEMENT("movement"),
		MANUAL("manual");
		
		private String value;
		
		private LIGHT_ACTIVATION(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static LIGHT_ACTIVATION byValue(String value) {
			for (LIGHT_ACTIVATION enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum LIGHT_CONFIG_KEYS{
		AUTOMATIC_ACTIVATION("automatic activation");
		
		private String value;
		
		private LIGHT_CONFIG_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static LIGHT_CONFIG_KEYS byValue(String value) {
			for (LIGHT_CONFIG_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum LIGHT_DATA_STORE_KEYS{
		LIGHT_STATE("light state");
		
		private String value;
		
		private LIGHT_DATA_STORE_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static LIGHT_DATA_STORE_KEYS byValue(String value) {
			for (LIGHT_DATA_STORE_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
}
