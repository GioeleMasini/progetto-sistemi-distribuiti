package it.unibo.smarthome.constants;

public class AlarmConstants {

	public static enum ALARM_ACTIVITY_STATE {
		ON("ON"),
		OFF("OFF");

		private String value;

		private ALARM_ACTIVITY_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static ALARM_ACTIVITY_STATE byValue(String value) {
			for (ALARM_ACTIVITY_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}

	public static enum ALARM_MODE_STATE {
		ENABLE("ENABLE"),
		DISABLE("DISABLE");

		private String value;

		private ALARM_MODE_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static ALARM_MODE_STATE byValue(String value) {
			for (ALARM_MODE_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}

	public static enum ALARM_FSM_STATE {
		WAIT_ACTIVE_STATE("WAIT_ACTIVE_STATE"),
		TRANSITION_TO_ON("TRANSITION_TO_ON"),
		ON("ON"),
		RING("RING"),
		TRANSITION_TO_OFF("TRANSITION_TO_OFF");

		private String value;

		private ALARM_FSM_STATE(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static ALARM_FSM_STATE byValue(String value) {
			for (ALARM_FSM_STATE enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum ALARM_FSM_EXIT_CODE {
		WAIT_ACTIVE_STATE, 
		TRANSITION_TO_ON, 
		ON, 
		RING, 
		TRANSITION_TO_OFF;

		private int value;

		public int getValue() {
			return value;
		}
		
		private void setValue(int v) {
			this.value = v;
		}

		public static ALARM_FSM_EXIT_CODE byValue(int value) {
			for (ALARM_FSM_EXIT_CODE enumItem : values()) {
				if (enumItem.getValue() == value) {
					return enumItem;
				}
			}
			return null;
		}
		
		public boolean equals(int value) {
			return this.getValue() == value;
		}
		
		/**
		 * Assegno un valore univoco ad ogni enumeratore.
		 * @see https://stackoverflow.com/a/536461/3687018
		 */
		static
		{
			int nextValue = 1;
			for (ALARM_FSM_EXIT_CODE enumItem: values()) {
				enumItem.setValue(nextValue);
				nextValue++;
			}
		}
	}
	
	public static enum ALARM_CONFIG_KEYS {
		EMERGENCY_NUMBER("emergency number"),
		EMERGENCY_TEXT("emergency text"),
		RINGING_TIME("ringing_time"),
		PASSWORD("password");

		private String value;

		private ALARM_CONFIG_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static ALARM_CONFIG_KEYS byValue(String value) {
			for (ALARM_CONFIG_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
	public static enum ALARM_DATA_STORE_KEYS {
		ALARM_STATE("alarm state"),
		ALARM_MODE("alarm mode");

		private String value;

		private ALARM_DATA_STORE_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static ALARM_DATA_STORE_KEYS byValue(String value) {
			for (ALARM_DATA_STORE_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
	
}
