package it.unibo.smarthome.constants;

public enum Season {
	SUMMER, WINTER, AUTUMN, SPRING
}
