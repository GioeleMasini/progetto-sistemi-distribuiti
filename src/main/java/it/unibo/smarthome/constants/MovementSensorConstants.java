package it.unibo.smarthome.constants;

public class MovementSensorConstants {
	
	public static enum MOVEMENT_SENSOR_DATA_STORE_KEYS {
		THERE_IS_MOVEMENT("there is movement");

		private String value;

		private MOVEMENT_SENSOR_DATA_STORE_KEYS(String v) {
			value = v;
		}

		@Override
		public String toString() {
			return getValue();
		}

		public String getValue() {
			return value;
		}

		public static MOVEMENT_SENSOR_DATA_STORE_KEYS byValue(String value) {
			for (MOVEMENT_SENSOR_DATA_STORE_KEYS enumItem : values()) {
				if (enumItem.getValue() == null) {
					if (value == null) {
						return enumItem;
					}
				} else if (enumItem.getValue().equals(value)) {
					return enumItem;
				}
			}
			return null;
		}
	}
}
