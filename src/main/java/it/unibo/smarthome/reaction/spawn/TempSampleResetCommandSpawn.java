package it.unibo.smarthome.reaction.spawn;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractSpawnActivity;
import alice.tucson.api.exceptions.TucsonInvalidLogicTupleException;
import it.unibo.smarthome.dao.TemperatureDeviceDeregistrationDAO;
import it.unibo.smarthome.dao.TemperatureDeviceSampleDAO;
import it.unibo.smarthome.dao.TemperatureRoomDAO;
import it.unibo.smarthome.dao.TemperaureSampleDeviceResetCommandDAO;
import it.unibo.smarthome.dao.TemperaureSampleResetCommandDAO;

public class TempSampleResetCommandSpawn extends AbstractSpawnActivity {
	
	private static final long serialVersionUID = -3562959607625493330L;
	private static Logger log = LogManager.getLogger();
	
	@Override
	public void doActivity() {
		log.debug("reaction handler spawnato");
		
		try {
			//in del comando di reset utile per recuperare RoomId
			TemperaureSampleResetCommandDAO resetCmdIn = new TemperaureSampleResetCommandDAO();
			LogicTuple tupleResetCmdIn = resetCmdIn.toTuple("R");
			LogicTuple inRes = this.in(tupleResetCmdIn);
			
			TemperaureSampleResetCommandDAO resetCmdResult = new TemperaureSampleResetCommandDAO(inRes);
			String roomId = resetCmdResult.getRoomID();
			
			//in_all di tutti i vecchi reset degli agenti scritti in precedenza (per pulizia)
			TemperaureSampleDeviceResetCommandDAO tempSDRCOld = new TemperaureSampleDeviceResetCommandDAO();
			LogicTuple tupleTempSDRCOld = tempSDRCOld.toTuple("D", toProlog(roomId));
			this.inAll(tupleTempSDRCOld);
			
			//in_all di eventuali deregistrazioni da parte dei sensori di temperatura
			TemperatureDeviceDeregistrationDAO tempDD = new TemperatureDeviceDeregistrationDAO();
			LogicTuple tupleTempDD = tempDD.toTuple("D", toProlog(roomId));
			this.inAll(tupleTempDD);
			
			//rimozione temperatura media della stanza
			TemperatureRoomDAO avgTempRoom = new TemperatureRoomDAO();
			LogicTuple tupleAvgTempRoom = avgTempRoom.toTuple("V", toProlog(roomId));
			this.in(tupleAvgTempRoom);
			
			//in_all di tutte le temperature sample device
			TemperatureDeviceSampleDAO tempSDOld = new TemperatureDeviceSampleDAO();
			LogicTuple tupleTempSDOld = tempSDOld.toTuple("S", "D", toProlog(roomId));
			List<LogicTuple> allSample = this.inAll(tupleTempSDOld);
			
			//out dei comandi di reset ai singoli device
			if(!allSample.isEmpty()) {
				for(LogicTuple tp : allSample) {
					TemperatureDeviceSampleDAO tpSampleDevice = new TemperatureDeviceSampleDAO(tp);
					String deviceId = tpSampleDevice.getDeviceID();
					TemperaureSampleDeviceResetCommandDAO resetDevCmd = new TemperaureSampleDeviceResetCommandDAO(deviceId, roomId);
					this.out(resetDevCmd.toTuple());
				}
			}
			
			log.info("Gestione comando di reset per la stanza con id: " + roomId + ", terminato correttamente");
			
		} catch (InvalidLogicTupleException | TucsonInvalidLogicTupleException e) {
			log.error("Errore durante gestione comando di reset: " + e.getMessage());
		}
	}
	
	private String toProlog(String text) {
		return "'" + text + "'";
	}

}
