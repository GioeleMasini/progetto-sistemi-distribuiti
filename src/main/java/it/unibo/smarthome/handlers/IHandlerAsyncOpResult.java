package it.unibo.smarthome.handlers;

import it.unibo.tucson.jade.coordination.AsynchTucsonOpResult;

public interface IHandlerAsyncOpResult {

	public void handle(AsynchTucsonOpResult toce);
}
