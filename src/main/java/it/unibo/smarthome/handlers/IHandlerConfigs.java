package it.unibo.smarthome.handlers;

import java.util.Map;

public interface IHandlerConfigs {
	public void handle(Map<String, String> configs);
}
