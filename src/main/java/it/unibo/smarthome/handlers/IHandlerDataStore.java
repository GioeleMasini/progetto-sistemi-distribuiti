package it.unibo.smarthome.handlers;

import java.util.Map;

public interface IHandlerDataStore {
	public void handle(Map<String, Object> dataStore);
}
