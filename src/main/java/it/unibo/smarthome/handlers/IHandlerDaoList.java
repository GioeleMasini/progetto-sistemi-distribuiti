package it.unibo.smarthome.handlers;

import java.util.List;
import it.unibo.smarthome.dao.BaseDAO;

public interface IHandlerDaoList<T extends BaseDAO> {
	public void handle(List<T> daoList);
}
