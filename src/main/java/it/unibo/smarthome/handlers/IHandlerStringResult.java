package it.unibo.smarthome.handlers;

public interface IHandlerStringResult {

	public void handle(String result);
}
