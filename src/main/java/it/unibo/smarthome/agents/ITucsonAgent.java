package it.unibo.smarthome.agents;

import java.util.List;

import alice.logictuple.LogicTuple;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.asynchSupport.actions.AbstractTucsonAction;
import it.unibo.smarthome.dao.ITuplable;
import it.unibo.smarthome.handlers.IHandlerOpCompletion;
import it.unibo.tucson.jade.coordination.AsynchTucsonOpResult;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.behaviours.Behaviour;

public interface ITucsonAgent extends IBaseAgent {
	public BridgeToTucson getBridge();
	public TucsonTupleCentreId getTucsonTupleCentreId();
	
	public void actionSync(Class<? extends AbstractTucsonAction> actionClass, List<? extends ITuplable> daoList, IHandlerOpCompletion hand, Behaviour beha);
	public AsynchTucsonOpResult actionAsync(Class<? extends AbstractTucsonAction> actionClass, List<? extends ITuplable> daoList);
	public void actionSync(Class<? extends AbstractTucsonAction> actionClass, ITuplable dao, IHandlerOpCompletion hand, Behaviour beha);
	public AsynchTucsonOpResult actionAsync(Class<? extends AbstractTucsonAction> actionClass, ITuplable dao);
	public void actionSync(Class<? extends AbstractTucsonAction> actionClass, String tuple, IHandlerOpCompletion hand, Behaviour beha);
	public AsynchTucsonOpResult actionAsync(Class<? extends AbstractTucsonAction> actionClass, String tuple);
	public void actionSync(Class<? extends AbstractTucsonAction> actionClass, LogicTuple lt, IHandlerOpCompletion hand, Behaviour beha);
	public AsynchTucsonOpResult actionAsync(Class<? extends AbstractTucsonAction> actionClass, LogicTuple lt);
}
