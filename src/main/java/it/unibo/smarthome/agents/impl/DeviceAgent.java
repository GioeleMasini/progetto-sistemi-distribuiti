package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.In;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.behaviours.ExecTestActionOnRequestBehaviour;
import it.unibo.smarthome.behaviours.ExecuteFuncBehaviour;
import it.unibo.smarthome.behaviours.RdUniqueIdBehaviour;
import it.unibo.smarthome.behaviours.SendConfigsOnRequestBehaviour;
import it.unibo.smarthome.behaviours.SendDataStoreOnRequestBehaviour;
import it.unibo.smarthome.behaviours.SetupDeviceBehaviour;
import it.unibo.smarthome.behaviours.StopDeviceOnRequestBehaviour;
import it.unibo.smarthome.behaviours.UpdateConfigsOnRequestBehaviour;
import it.unibo.smarthome.behaviours.UpdateDeviceDataOnRequestBehaviour;
import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IDevice;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;

public class DeviceAgent extends TucsonAgent {

	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger();

	protected void setup() {
		super.setup();
		
		ParallelBehaviour runActions = new ParallelBehaviour();
		runActions.addSubBehaviour(new SendConfigsOnRequestBehaviour());
		runActions.addSubBehaviour(new SendDataStoreOnRequestBehaviour());
		runActions.addSubBehaviour(new UpdateConfigsOnRequestBehaviour());
		runActions.addSubBehaviour(new UpdateDeviceDataOnRequestBehaviour());
		runActions.addSubBehaviour(new ExecTestActionOnRequestBehaviour());
		runActions.addSubBehaviour(new StopDeviceOnRequestBehaviour());
		
		SequentialBehaviour setupSequence = new SequentialBehaviour();
		setupSequence.addSubBehaviour(new RdUniqueIdBehaviour("device", this::setDeviceId));
		setupSequence.addSubBehaviour(new SetupDeviceBehaviour());
		setupSequence.addSubBehaviour(new ExecuteFuncBehaviour(this.getDevice()::notifyBaseDeviceSetupCompleted));
		setupSequence.addSubBehaviour(runActions);
		this.addBehaviour(setupSequence);
	}
	
	private void setDeviceId(String id) {
		this.getDevice().setId(id);
	}

	public BaseDevice getDevice() {
		return (BaseDevice) this.getArguments()[1];
	}

	@Override
	public void takeDown() {
		IDevice d = this.getDevice();
		try {
			LogicTuple dao = new DeviceDAO(d.getId(), null, null, null, null).toTuple(null, "W", "X", "Y", "Z");
			this.actionAsync(In.class, dao);
		} catch (InvalidLogicTupleException e) {
			log.error("Errore nella creazione della tupla device() durante la kill di DeviceAgent: " + e.getMessage());
		}
		
		super.takeDown();
	}
	
	
}
