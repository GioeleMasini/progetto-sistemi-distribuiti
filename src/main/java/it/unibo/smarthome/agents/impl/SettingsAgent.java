package it.unibo.smarthome.agents.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.tucson.asynchSupport.actions.specification.SetS;
import alice.tucson.utilities.Utils;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.behaviours.SaveRoomsBehaviour;
import it.unibo.smarthome.constants.Settings;
import it.unibo.smarthome.dao.RoomDAO;
import jade.core.ServiceException;

/**
 * Che avvia
 */
public class SettingsAgent extends TucsonAgent {

	private static final long serialVersionUID = 1L;

	private static Logger log = LogManager.getLogger();

	@Override
	protected void setup() {
		
		// 1. init tucson interaction
		super.setup();
		
		// 2. load reactions
		loadReaction();
		log.info("Reactions loaded successfully");
		
		// 3. load default settings
		loadDefaultSettings();
		log.info("Default config loaded successfully");
	}
	
	/**
	 * Carica le configurazioni di default
	 */
	private void loadDefaultSettings(){
		List<RoomDAO> rooms = new ArrayList<>(1);
		rooms.add(new RoomDAO("Default room"));
		this.addBehaviour(new SaveRoomsBehaviour(rooms, null));
	}
	
	/**
	 * Carica le reaction della simulazione
	 */
	private void loadReaction(){
		try {
			String utility = Utils.fileToString(Settings.UTILITY_REACTION_FILE);
			String room = Utils.fileToString(Settings.ROOM_REACTION_FILE);
			String temperature = Utils.fileToString(Settings.TEMPERATURE_REACTION_FILE);
			String movement = Utils.fileToString(Settings.MOVEMENT_REACTION_FILE);
			String uniqueId = Utils.fileToString(Settings.UNIQUE_ID_REACTION_FILE);
			String device = Utils.fileToString(Settings.DEVICE_REACTION_FILE);
			
			String value = utility + room + temperature + movement + uniqueId + device;
			
			final LogicTuple specT = new LogicTuple("spec", new Value(value));
			SetS op = new SetS(getTucsonTupleCentreId(), specT);
			SettingsAgent.this.getBridge().asynchronousInvocation(op);
			
		} catch (ServiceException | IOException e) {
			log.error("Errore durante caricamento reactions: " + e.getMessage());
		}
	}

}
