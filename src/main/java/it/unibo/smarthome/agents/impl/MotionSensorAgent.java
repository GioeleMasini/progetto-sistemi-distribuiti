package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.dao.MovementRoomDAO;
import it.unibo.smarthome.devices.IMotionSensor;
import it.unibo.smarthome.devices.events.DeviceEvent;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class MotionSensorAgent extends TucsonAgent {

	private static final long serialVersionUID = 385023804252043979L;
	private static Logger log = LogManager.getLogger();
	private IMotionSensor motionSensor;

	protected void setup() {
		super.setup();
		this.motionSensor = (IMotionSensor) getArguments()[1];
		
		this.motionSensor.addListener(this::onDeviceEvent);
	}
	
	@Override
	public void takeDown() {
		super.takeDown();
		if (this.motionSensor.isThereMovement()) {
			// Devo pulire il TC dalla tupla di movimento
			String agentName = this.getName();
			String roomId = this.motionSensor.getRoomId();
			MovementRoomDAO movementDAO = new MovementRoomDAO(agentName, roomId);
			actionAsync(In.class, movementDAO);
		}
	}
	
	private void onDeviceEvent(DeviceEvent event) {
		if (event.getName().equals("movement-true")) {
			this.addBehaviour(new WriteMovementBehaviour(this, this.motionSensor));
		} else if (event.getName().equals("movement-false")) {
			this.addBehaviour(new CleanMovementBehaviour(this, this.motionSensor));
		}
	}
	
	/**
	 * Behaviour che scrive sul tuple centre quale device ha registrato il movimento e in quale stanza
	 * 
	 * @author Andrea Pruccoli
	 *
	 */
	class WriteMovementBehaviour extends OneShotBehaviour {

		private static final long serialVersionUID = 4577398407236788239L;

		private IMotionSensor motionSensor;
		
		public WriteMovementBehaviour(Agent a, IMotionSensor motionSensor) {
			super(a);
			this.motionSensor = motionSensor;
		}

		@Override
		public void action() {
			String agentName = this.getAgent().getName();
			String roomId = this.motionSensor.getRoomId();
			MovementRoomDAO movementDAO = new MovementRoomDAO(agentName, roomId);
			actionAsync(Out.class, movementDAO);
			log.info("Inserita tupla movimento; "
					+ "Agente: " + agentName + " - Stanza: " + roomId);
		}
	}
	
	/**
	 * Behaviour che elimina dal tuple centre la tupla del movimento precedentemente registrata
	 * 
	 * @author Andrea Pruccoli
	 *
	 */
	class CleanMovementBehaviour extends SimpleBehaviour {
		
		private static final long serialVersionUID = -2448840522289571405L;
		private IMotionSensor motionSensor;
		private boolean done = false;

		public CleanMovementBehaviour(Agent a, IMotionSensor motionSensor) {
			super(a);
			this.motionSensor = motionSensor;
		}

		@Override
		public void action() {
			String agentName = this.getAgent().getName();
			String roomId = this.motionSensor.getRoomId();
			MovementRoomDAO movementDAO = new MovementRoomDAO(agentName, roomId);
			actionSync(In.class, movementDAO, this::operationDone, this);
		}
		
		public void operationDone(TucsonOpCompletionEvent res) {
			if (res != null && res.resultOperationSucceeded()) {
				log.info("Eliminata tupla movimento; "
						+ "Agente: " + this.getAgent().getName() + " - Stanza: " + this.motionSensor.getRoomId());
				this.done = true;
			}
		}
		
		@Override
		public boolean done() {
			return this.done;
		}
	}
}
