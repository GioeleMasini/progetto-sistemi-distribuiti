package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.behaviours.AlarmManagerBehaviour;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_ACTIVITY_STATE;
import it.unibo.smarthome.dao.AlarmChangeStateDAO;
import it.unibo.smarthome.dao.AlarmHomeActiveDAO;
import it.unibo.smarthome.devices.IAlarm;
import it.unibo.smarthome.devices.events.DeviceEvent;

public class AlarmAgent extends TucsonAgent {

	private static final long serialVersionUID = 6007271312954593547L;
	private static Logger log = LogManager.getLogger();

	private IAlarm alarm;
	private AlarmManagerBehaviour alarmManager;

	protected void setup() {
		super.setup();
		this.alarm = (IAlarm) getArguments()[1];
		this.alarmManager = new AlarmManagerBehaviour(this);
		this.addBehaviour(this.alarmManager);
		
		this.alarm.addListener(this::onDeviceEvent);
	}
	
	@Override
	public void takeDown() {
		super.takeDown();
		// TODO se allarme acceso, togliere tupla homeAlarmStateActive
		// TODO se allarme sta suonando? Richiamare qua eventuali metodi di spegnimento del suono
		if (this.alarm.isActive()) {
			// Devo pulire il TC dalla tupla di homeAlarmStateActive (questo se c'è un solo allarme in casa)
			if (this.alarm.isAlarmEnable()) {
				// Mandare il segnale di spegnimento dei segnalatori acustici?
			}
			actionAsync(In.class, AlarmHomeActiveDAO.TEMPLATE);
		}
	}
	
	public IAlarm getAlarm() {
		return this.alarm;
	}
	
	private void onDeviceEvent(DeviceEvent e) {
		log.info("Evento " + e.getName() + " catturato dal listener in AlarmAgent.");
		if (e.getName().equals("toggle-state-alarm")) {			
			ALARM_ACTIVITY_STATE toStato = this.alarm.isActive() ? 
					ALARM_ACTIVITY_STATE.OFF : ALARM_ACTIVITY_STATE.ON;
			
			log.info("Cambio stato allarme in: " + toStato.getValue());
			
			// Scrivo sul TC, così il mio behaviour può agire come se non fosse un caso di test
			AlarmChangeStateDAO changeAlarmStateDao = new AlarmChangeStateDAO(toStato, this.alarm.getPassword());
			actionAsync(Out.class, changeAlarmStateDao);
		} else if (e.getName().equals("ringing-time-updated")) {
			this.alarmManager.updateRingingTime();
		}
	}	
}
