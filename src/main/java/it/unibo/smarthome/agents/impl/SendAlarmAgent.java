package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.asynchSupport.actions.ordinary.Out;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.dao.AlarmAlertNotifyDAO;
import it.unibo.smarthome.devices.IWindowAlarmed;
import jade.core.behaviours.CyclicBehaviour;

public class SendAlarmAgent extends TucsonAgent {

	private static final long serialVersionUID = 4087175912100436279L;
	private static Logger log = LogManager.getLogger();
	
	private IWindowAlarmed window;

	protected void setup() {
		super.setup();
		this.window = (IWindowAlarmed) getArguments()[1];
		this.addBehaviour(new NotifyAlarmBehaviour(this));
	}
	
	class NotifyAlarmBehaviour extends CyclicBehaviour {

		private static final long serialVersionUID = 8497227466302639635L;

		private NotifyAlarmBehaviour(TucsonAgent a) {
			super(a);
		}

		@Override
		public void action() {
			log.debug("NotifyAlarmBehaviour::action, inizio");
			
			TucsonAgent agent = ((TucsonAgent)this.getAgent());
			AlarmAlertNotifyDAO msg = new AlarmAlertNotifyDAO(window.getId(), window.getRoomId());
			agent.actionAsync(Out.class, msg);
			log.debug("NotifyAlarmBehaviour::action, messaggio inviato");
			
			agent.doSuspend();
		}
	}
}
