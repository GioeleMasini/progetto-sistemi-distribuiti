package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Inp;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.constants.TemperatureConstants.TEMP_THERM_SMART_EXIT_CODE;
import it.unibo.smarthome.constants.TemperatureConstants.TEMP_THERM_SMART_STATE;
import it.unibo.smarthome.dao.TemperatureRoomRequestUpdateDAO;
import it.unibo.smarthome.dao.TemperatureRoomUpdatedNotifyDAO;
import it.unibo.smarthome.dao.TemperatureRoomDAO;
import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.devices.IThermometer;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.devices.events.IDeviceEventListener;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class ThermometerSmartAgent  extends TucsonAgent implements IDeviceEventListener {

	private static final long serialVersionUID = 2224044008948888223L;
	private static Logger log = LogManager.getLogger();
	
	private IThermometer thermometer;
	private WaitUpdateTemperatureRoomBehaviour waitBehaviour;
	private UpdateTemperatureRoomBehaviour updateBehaviour;
	
	protected void setup() {
		super.setup();
		this.thermometer = (IThermometer) getArguments()[1];
		this.thermometer.addListener(this);
		
		Behaviour mainBehaviour = createFSM();
		this.addBehaviour(mainBehaviour);
	}
	
	private Behaviour createFSM() {
		FSMBehaviour fsm = new FSMBehaviour(this);
		this.waitBehaviour = new WaitUpdateTemperatureRoomBehaviour(this, this.thermometer);
		this.updateBehaviour = new UpdateTemperatureRoomBehaviour(this, this.thermometer);
		
		fsm.registerFirstState(this.updateBehaviour, TEMP_THERM_SMART_STATE.UPDATING.getValue());
		fsm.registerState(this.waitBehaviour, TEMP_THERM_SMART_STATE.WATING.getValue());
				
		fsm.registerTransition(TEMP_THERM_SMART_STATE.WATING.getValue(), TEMP_THERM_SMART_STATE.UPDATING.getValue(), TEMP_THERM_SMART_EXIT_CODE.UPDATE.getValue());
		fsm.registerTransition(TEMP_THERM_SMART_STATE.UPDATING.getValue(), TEMP_THERM_SMART_STATE.WATING.getValue(), TEMP_THERM_SMART_EXIT_CODE.WAIT.getValue());
		
		return fsm;
	}

	@Override
	public void onNotify(DeviceEvent e) {
		this.waitBehaviour.restart();
		this.updateBehaviour.restart();
	}
	
	@Override
	public void takeDown() {
		super.takeDown();
		// rimuovo la richiesta di update della temperatura
		TemperatureRoomRequestUpdateDAO reqDAO = new TemperatureRoomRequestUpdateDAO( this.thermometer.getId(), this.thermometer.getRoomId());
		this.actionAsync(Inp.class, reqDAO);
	}
	
	class WaitUpdateTemperatureRoomBehaviour extends SimpleBehaviour {
		
		private static final long serialVersionUID = -891798281430593985L;
		private IDevice device;
		private TEMP_THERM_SMART_EXIT_CODE onEndExitState;

		public WaitUpdateTemperatureRoomBehaviour(TucsonAgent a, IDevice device) {
			super(a);
			this.device = device;
		}

		@Override
		public void action() {
			this.onEndExitState = TEMP_THERM_SMART_EXIT_CODE.WAIT;
			
			// attendo la notifica di update
			TemperatureRoomUpdatedNotifyDAO notifyDAO = new TemperatureRoomUpdatedNotifyDAO( this.device.getId(), this.device.getRoomId());
			ITucsonAgent agent = (TucsonAgent) this.getAgent();
			agent.actionSync(In.class, notifyDAO, this::gestioneNotifyUpdate, this);
		}
		
		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}
		
		private void gestioneNotifyUpdate(TucsonOpCompletionEvent res) {
			log.debug("WaitUpdateTemperatureRoomBehaviour::gestioneNotifyUpdate, attivato, resOpSucc: " + res.resultOperationSucceeded());
			if(res.resultOperationSucceeded()) {
				this.onEndExitState = TEMP_THERM_SMART_EXIT_CODE.UPDATE;
				log.debug("WaitUpdateTemperatureRoomBehaviour::gestioneNotifyUpdate, completato correttamente");
			}
		}

		@Override
		public boolean done() {
			return this.onEndExitState.getValue() == TEMP_THERM_SMART_EXIT_CODE.UPDATE.getValue();
		}
	}
	
	class UpdateTemperatureRoomBehaviour extends SimpleBehaviour {

		private static final long serialVersionUID = 2128201943618933973L;
		private IDevice device;
		private TEMP_THERM_SMART_EXIT_CODE onEndExitState;

		public UpdateTemperatureRoomBehaviour(TucsonAgent a, IDevice device) {
			super(a);
			this.device = device;
		}
		
		@Override
		public void action() {
			this.onEndExitState = TEMP_THERM_SMART_EXIT_CODE.UPDATE;
			
			try {
				TemperatureRoomDAO dao = new TemperatureRoomDAO(null, this.device.getRoomId());
				ITucsonAgent agent = (TucsonAgent) this.getAgent();
				agent.actionSync(Rd.class, dao.toTuple("V", null), this::gestioneTempUpdate, this);
			} catch (InvalidLogicTupleException e) {
				log.error("Errore durante la lettura della temperatura: " + e.getMessage());
			}
		}
		
		@Override
		public int onEnd() {
			return this.onEndExitState.getValue();
		}
		
		private void gestioneTempUpdate(TucsonOpCompletionEvent res) {
			log.debug("UpdateTemperatureRoomBehaviour::gestioneTempUpdate, attivato, resOpSucc: " + res.resultOperationSucceeded());
			if(res.resultOperationSucceeded()) {
				// aggiorno la temperatura del device
				TemperatureRoomDAO tempRD = new TemperatureRoomDAO(res.getTuple());
				thermometer.updateTemperature(tempRD.getValue());
				
				// effettuo nuovamente la richiesta di update della temperatura
				TemperatureRoomRequestUpdateDAO reqDAO = new TemperatureRoomRequestUpdateDAO( this.device.getId(), this.device.getRoomId());
				ITucsonAgent agent = (TucsonAgent) this.getAgent();
				agent.actionAsync(Out.class, reqDAO);
				
				this.onEndExitState = TEMP_THERM_SMART_EXIT_CODE.WAIT;
				log.debug("UpdateTemperatureRoomBehaviour::gestioneTempUpdate, completato correttamente");
			}
		}

		@Override
		public boolean done() {
			return this.onEndExitState.getValue() == TEMP_THERM_SMART_EXIT_CODE.WAIT.getValue();
		}
	}
}
