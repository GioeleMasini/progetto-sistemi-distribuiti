package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.asynchSupport.actions.ordinary.No;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.constants.WindowConstants.WINDOW_ALARM_EXIT_CODE;
import it.unibo.smarthome.constants.WindowConstants.WINDOW_ALARM_HOME_STATE;
import it.unibo.smarthome.dao.AlarmHomeActiveDAO;
import it.unibo.smarthome.devices.IWindowAlarmed;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class WindowAlarmStateAgent  extends TucsonAgent {

	private static final long serialVersionUID = 2514991050671324530L;
	private static Logger log = LogManager.getLogger();
	
	private IWindowAlarmed windowDevice;
	
	protected void setup() {
		super.setup();
		this.windowDevice = (IWindowAlarmed) getArguments()[1];
		
		Behaviour mainBehaviour = createFSM();
		this.addBehaviour(mainBehaviour);
	}
	
	private Behaviour createFSM() {
		FSMBehaviour fsm = new FSMBehaviour(this);
		Behaviour enable = new CheckAlarmStateOnBehaviour(this, this.windowDevice);
		Behaviour disable = new CheckAlarmStateOffBehaviour(this, this.windowDevice);

		fsm.registerFirstState(enable, WINDOW_ALARM_HOME_STATE.ENABLE.getValue());
		fsm.registerState(disable, WINDOW_ALARM_HOME_STATE.DISABLE.getValue());
				
		fsm.registerTransition(WINDOW_ALARM_HOME_STATE.DISABLE.getValue(), WINDOW_ALARM_HOME_STATE.ENABLE.getValue(), WINDOW_ALARM_EXIT_CODE.DISABLE.getValue());
		fsm.registerTransition(WINDOW_ALARM_HOME_STATE.ENABLE.getValue(), WINDOW_ALARM_HOME_STATE.DISABLE.getValue(), WINDOW_ALARM_EXIT_CODE.ENABLE.getValue());
		
		return fsm;
	}
	
	class CheckAlarmStateOnBehaviour extends SimpleBehaviour {
		
		private static final long serialVersionUID = 1610220378302746862L;
		
		private IWindowAlarmed device;
		private int onEndExitState;

		public CheckAlarmStateOnBehaviour(TucsonAgent a, IWindowAlarmed device) {
			super(a);
			this.device = device;
		}
		
		@Override
		public void action() {
			this.onEndExitState = WINDOW_ALARM_EXIT_CODE.DISABLE.getValue();
			AlarmHomeActiveDAO tempSRCD = new AlarmHomeActiveDAO();
			ITucsonAgent agent = (ITucsonAgent) this.getAgent(); 
			agent.actionSync(Rd.class, tempSRCD, this::gestioneAttivazione, this);
		}
		
		private void gestioneAttivazione(TucsonOpCompletionEvent res) {
			log.info("CheckAlarmStateOnBehaviour, gestioneAttivazione inizio");
			if(res.resultOperationSucceeded()) {
				this.device.enableAlarm();
				this.onEndExitState = WINDOW_ALARM_EXIT_CODE.ENABLE.getValue();
			}
		}

		@Override
		public boolean done() {
			return this.onEndExitState == WINDOW_ALARM_EXIT_CODE.ENABLE.getValue();
		}
		
		@Override
		public int onEnd() {
			return this.onEndExitState;
		}
	}
	
	class CheckAlarmStateOffBehaviour extends SimpleBehaviour {

		private static final long serialVersionUID = 7163370456050472609L;
		
		private IWindowAlarmed device;
		private int onEndExitState;

		public CheckAlarmStateOffBehaviour(TucsonAgent a, IWindowAlarmed device) {
			super(a);
			this.device = device;
		}
		
		@Override
		public void action() {
			this.onEndExitState = WINDOW_ALARM_EXIT_CODE.ENABLE.getValue();
			AlarmHomeActiveDAO tempSRCD = new AlarmHomeActiveDAO();
			ITucsonAgent agent = (ITucsonAgent) this.getAgent(); 
			agent.actionSync(No.class, tempSRCD, this::gestioneDisattivazione, this);
		}
		
		private void gestioneDisattivazione(TucsonOpCompletionEvent res) {
			log.info("CheckAlarmStateOnBehaviour, gestioneDisattivazione inizio");
			if(res.resultOperationSucceeded()) {
				this.device.disableAlarm();
				this.onEndExitState = WINDOW_ALARM_EXIT_CODE.DISABLE.getValue();
			}
		}

		@Override
		public boolean done() {
			return this.onEndExitState == WINDOW_ALARM_EXIT_CODE.DISABLE.getValue();
		}
		
		@Override
		public int onEnd() {
			return this.onEndExitState;
		}
	}
}
