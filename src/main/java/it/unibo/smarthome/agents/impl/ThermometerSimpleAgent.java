package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.constants.Settings;
import it.unibo.smarthome.dao.TemperatureRoomDAO;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.devices.IThermometer;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.devices.events.IDeviceEventListener;
import jade.core.behaviours.TickerBehaviour;

public class ThermometerSimpleAgent extends TucsonAgent implements IDeviceEventListener {

	private static final long serialVersionUID = 2224044008948888223L;
	private static Logger log = LogManager.getLogger();
	
	private IThermometer thermometer;
	private CheckTemperatureRoomBehaviour checkTemperatureBehaviour;
	
	protected void setup() {
		super.setup();
		this.thermometer = (IThermometer) getArguments()[1];
		this.thermometer.addListener(this);
		
		this.checkTemperatureBehaviour = new CheckTemperatureRoomBehaviour(this, this.thermometer, Settings.TICK_TIME_SLOW);
		
		this.addBehaviour(this.checkTemperatureBehaviour);
	}

	@Override
	public void onNotify(DeviceEvent e) {
		if (BaseDevice.EVENT_ROOMID_UPDATED.equals(e.getName())) {
			this.checkTemperatureBehaviour.restart();
		}
	}
	
	class CheckTemperatureRoomBehaviour extends TickerBehaviour {

		private static final long serialVersionUID = -8616957855511181410L;
		private IDevice device;

		public CheckTemperatureRoomBehaviour(TucsonAgent a, IDevice device, long time) {
			super(a, time);
			this.device = device;
		}

		@Override
		protected void onTick() {
			try {
				log.debug("onTick, inizio");
				TemperatureRoomDAO dao = new TemperatureRoomDAO(null, this.device.getRoomId());
				ITucsonAgent agent = (ITucsonAgent) this.getAgent();
                agent.actionSync(Rd.class, dao.toTuple("V", null), this::gestioneTempUpdate, this);
			} catch (InvalidLogicTupleException e) {
				log.error("Errore durante la lettura delle temperatura: " + e.getMessage());
			}
		}
		
        private void gestioneTempUpdate(TucsonOpCompletionEvent res) {
            if(res.resultOperationSucceeded()) {
                TemperatureRoomDAO tempRD = new TemperatureRoomDAO(res.getTuple());
                thermometer.updateTemperature(tempRD.getValue());
            }
        }
	}
}
