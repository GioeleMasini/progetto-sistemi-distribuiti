package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.No;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.constants.LightConstants.LIGHT_EXIT_CODE;
import it.unibo.smarthome.constants.LightConstants.LIGHT_STATE;
import it.unibo.smarthome.dao.MovementRoomDAO;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.ILight;
import it.unibo.smarthome.devices.events.DeviceEvent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class LightAgent extends TucsonAgent {

	private static final long serialVersionUID = 7499652277518222197L;
	private static Logger log = LogManager.getLogger();
	
	private ILight light;
	private FSMBehaviour automaticTurnOn;
	private AutomaticTurnLightOnBehaviour autoTurnOn;
	private AutomaticTurnLightOffBehaviour autoTurnOff;
	
	protected void setup() {
		super.setup();
		this.light = (ILight) getArguments()[1];
		
		this.light.addListener(this::onDeviceEvent);
	}
	
	private void onDeviceEvent(DeviceEvent event) {
		if (event.getName().equals("automatic-mode")) {
			// Creo il behaviour che si mette in ascolto sulle tuple
			// di movimento nella stanza
			this.automaticTurnOn = this.configFSM();
			// this.automaticTurnOn = new AutomaticTurnLightOnBehaviour(this, this.light.getRoomId());
			this.addBehaviour(this.automaticTurnOn);
		} else if (event.getName().equals("manual-mode")) {
			// Rimuovo il behaviour, poiché non ce n'è più bisogno
			this.removeBehaviour(this.automaticTurnOn);
			this.automaticTurnOn = null;
		} else if (BaseDevice.EVENT_ROOMID_UPDATED.equals(event.getName())) {
			this.autoTurnOn.restart();
			this.autoTurnOff.restart();
		}
	}
	
	private FSMBehaviour configFSM() {
		FSMBehaviour tmp = new FSMBehaviour();
		
		this.autoTurnOn = new AutomaticTurnLightOnBehaviour(this, this.light);
		this.autoTurnOff = new AutomaticTurnLightOffBehaviour(this, this.light);
		
		tmp.registerFirstState(this.autoTurnOn, LIGHT_STATE.OFF.getValue());
		tmp.registerState(this.autoTurnOff, LIGHT_STATE.ON.getValue());
		
		tmp.registerTransition(LIGHT_STATE.OFF.getValue(), LIGHT_STATE.ON.getValue(), LIGHT_EXIT_CODE.ON.getValue());
		tmp.registerTransition(LIGHT_STATE.ON.getValue(), LIGHT_STATE.OFF.getValue(), LIGHT_EXIT_CODE.OFF.getValue());
		
		return tmp;
	}
	
	/**
	 * Behaviour per l'accensione della luce in base alle tuple di movimento
	 * presenti sul tuple centre che fanno riferimento alla stanza dove si
	 * trova anche la stanza.
	 * 
	 * @author Andrea Pruccoli
	 *
	 */
	class AutomaticTurnLightOnBehaviour extends SimpleBehaviour {

		private static final long serialVersionUID = -2091329040503846193L;
		private int onEndExitState;
		private ILight light;

		private AutomaticTurnLightOnBehaviour(TucsonAgent a, ILight light) {
			super(a);
			this.light = light;
		}
		
		@Override
		public void action() {
			this.onEndExitState = LIGHT_EXIT_CODE.OFF.getValue();
			LogicTuple checkToTurnOn = null;
			try {
				checkToTurnOn = (new MovementRoomDAO(null, this.light.getRoomId())).toTuple("X", null);
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			actionSync(Rd.class, checkToTurnOn, this::turnOnLight, this);
		}
		
		private void turnOnLight(TucsonOpCompletionEvent res) {
			if (res != null && res.resultOperationSucceeded()) {
				log.info("Accensione automatica luce.");
				this.light.turnOn();
				this.onEndExitState = LIGHT_EXIT_CODE.ON.getValue();
			}
		}

		@Override
		public boolean done() {
			return this.onEndExitState == LIGHT_EXIT_CODE.ON.getValue();
		}
		
		@Override
		public int onEnd() {
			return this.onEndExitState;
		}
	}

	
	/**
	 * Behaviour per lo spegnimento della luce in base alle tuple di movimento
	 * presenti sul tuple centre che fanno riferimento alla stanza dove si
	 * trova anche la stanza.
	 * 
	 * @author Andrea Pruccoli
	 *
	 */
	class AutomaticTurnLightOffBehaviour extends SimpleBehaviour {

		private static final long serialVersionUID = -9123669806319354773L;
		private int onEndExitState;	
		private ILight light;

		private AutomaticTurnLightOffBehaviour(TucsonAgent a, ILight light) {
			super(a);
			this.light = light;
		}
		
		@Override
		public void action() {
			this.onEndExitState = LIGHT_EXIT_CODE.ON.getValue();
			LogicTuple checkToTurnOff = null;
			try {
				checkToTurnOff = (new MovementRoomDAO(null, this.light.getRoomId())).toTuple("X", null);
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			actionSync(No.class, checkToTurnOff, this::turnOffLight, this);
		}
		
		private void turnOffLight(TucsonOpCompletionEvent res) {
			if (res != null && res.resultOperationSucceeded()) {
				log.info("Spegnimento automatico luce.");
				this.light.turnOff();
				this.onEndExitState = LIGHT_EXIT_CODE.OFF.getValue();
			}
		}

		@Override
		public boolean done() {
			return this.onEndExitState == LIGHT_EXIT_CODE.OFF.getValue();
		}
		
		@Override
		public int onEnd() {
			return this.onEndExitState;
		}		
	}
}
