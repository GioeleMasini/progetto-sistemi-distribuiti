package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.dao.PhoneTextMsgDAO;
import it.unibo.smarthome.devices.IPhone;
import jade.core.behaviours.SimpleBehaviour;

public class PhoneAgent extends TucsonAgent {

	private static final long serialVersionUID = -3417326215008280965L;
	private static Logger log = LogManager.getLogger();

	private IPhone phone;
	
	protected void setup() {
		super.setup();
		phone = (IPhone) getArguments()[1];
		this.addBehaviour(new MessageBehaviour(this));
	}

	class MessageBehaviour extends SimpleBehaviour {

		private static final long serialVersionUID = 3484176122154917756L;
		
		private MessageBehaviour(TucsonAgent a) {
			super(a);
		}

		@Override
		public void action() {
			ITucsonAgent agent = (ITucsonAgent) this.getAgent(); 
			agent.actionSync(In.class, PhoneTextMsgDAO.TEMPLATE, this::gestioneMessaggio, this);
		}
		
		private void gestioneMessaggio(TucsonOpCompletionEvent res) {
			log.info("Tupla messaggio recuperata");
			PhoneTextMsgDAO msgPhone = new PhoneTextMsgDAO(res.getTuple());
			phone.sendSMS(msgPhone.getNumber(), msgPhone.getMsg());
		}

		@Override
		public boolean done() {
			return false;
		}
	}

}