package it.unibo.smarthome.agents.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.smarthome.agents.ITucsonAgent;
import it.unibo.smarthome.agents.TucsonAgent;
import it.unibo.smarthome.constants.Settings;
import it.unibo.smarthome.dao.TemperatureDeviceDeregistrationDAO;
import it.unibo.smarthome.dao.TemperatureDeviceSampleDAO;
import it.unibo.smarthome.dao.TemperaureSampleDeviceResetCommandDAO;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.devices.ITemperatureSensor;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.devices.events.IDeviceEventListener;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;;

public class TemperatureSensorAgent extends TucsonAgent implements IDeviceEventListener {
	
	private static final long serialVersionUID = -6281046012923008815L;
	private static Logger log = LogManager.getLogger();
	
	private ITemperatureSensor temperatureSensor;
	private double storedSample;
	private CheckTemperatureResetBehaviour checkTemperatureBehaviour;

	protected void setup() {
		super.setup();
		this.temperatureSensor = (ITemperatureSensor) getArguments()[1];
		this.temperatureSensor.addListener(this);
		
		this.checkTemperatureBehaviour = new CheckTemperatureResetBehaviour(this, this.temperatureSensor);
		
		ParallelBehaviour mainBehaviour = new ParallelBehaviour();
		mainBehaviour.addSubBehaviour(this.checkTemperatureBehaviour);
		mainBehaviour.addSubBehaviour(new ReadTemperatureBehaviour(this, Settings.TICK_TIME_SLOW));
		this.addBehaviour(mainBehaviour);
	}
	
	@Override
	public void takeDown() {
		super.takeDown();
		TemperatureDeviceDeregistrationDAO tempDD = new TemperatureDeviceDeregistrationDAO(this.temperatureSensor.getId(), this.temperatureSensor.getRoomId());
		this.actionAsync(Out.class, tempDD);
	}
	
	private synchronized boolean outTempDeviceSample(boolean forced) {
		boolean send = false;
		if(forced) {
			outTemp(this.temperatureSensor.readTemperature());
			send = true;
		} else {
			double newSample = this.temperatureSensor.readTemperature();
			double tempRangeUpdate = this.temperatureSensor.getRangeUpdateChange();
			double upperBound = this.storedSample + tempRangeUpdate;
			double lowerBound = this.storedSample - tempRangeUpdate;
			if( newSample > upperBound || newSample < lowerBound ) {
				outTemp(newSample);
				send = true;
			}
		}
		return send;
	}
	
	private synchronized void outTemp(double sampleTemp) {
		TemperatureDeviceSampleDAO tempDSD = new TemperatureDeviceSampleDAO(sampleTemp, this.temperatureSensor.getId(), this.temperatureSensor.getRoomId());
		this.actionAsync(Out.class, tempDSD);
		this.storedSample = sampleTemp;
		log.info("Valore di temperatura aggiornata: " + sampleTemp);
	}

	@Override
	public void onNotify(DeviceEvent e) {
		if (BaseDevice.EVENT_ROOMID_UPDATED.equals(e.getName())) {
			this.checkTemperatureBehaviour.restart();
		}
	}

	class ReadTemperatureBehaviour extends TickerBehaviour {
		
		private static final long serialVersionUID = 6627537843662246545L;

		public ReadTemperatureBehaviour(TucsonAgent a, long period) {
			super(a, period);
			boolean sendedTemp = outTempDeviceSample(true);
			if(sendedTemp)
				log.debug("ReadTemperatureBehaviour, Prima temperatura inviata");
		}

		@Override
		protected void onTick() {
			outTempDeviceSample(false);
		}
	}
	
	class CheckTemperatureResetBehaviour extends CyclicBehaviour {

		private static final long serialVersionUID = -6392949908192742147L;
		private IDevice device;

		public CheckTemperatureResetBehaviour(TucsonAgent a, IDevice device) {
			super(a);
			this.device = device;
		}
		
		@Override
		public void action() {
			TemperaureSampleDeviceResetCommandDAO tempSRCD = new TemperaureSampleDeviceResetCommandDAO(this.device.getId(), this.device.getRoomId());
			ITucsonAgent agent = (ITucsonAgent) this.getAgent(); 
			agent.actionSync(In.class, tempSRCD, this::gestioneReset, this);
		}
		
		private void gestioneReset(TucsonOpCompletionEvent res) {
			if (res.resultOperationSucceeded()) {
				boolean sendedTemp = outTempDeviceSample(true);
				if(sendedTemp)
					log.info("Procedura di reset avviata, nuovo valore inviato");
			}
		}
	}
}
