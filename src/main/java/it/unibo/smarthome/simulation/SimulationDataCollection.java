package it.unibo.smarthome.simulation;

import java.util.Calendar;
import java.util.HashMap;

public class SimulationDataCollection<K> {
	
	private final HashMap<Integer, K> collection;

	public SimulationDataCollection() {
		this.collection = new HashMap<Integer, K>();
	}
	
	public K get(Calendar day) {
		return collection.get(buildKey(day));
	}
	
	public void put(Calendar day, K data) {
		collection.put(buildKey(day), data);
	}
	
	public boolean has(Calendar day) {
		return collection.containsKey(buildKey(day));
	}

	private static int buildKey(Calendar day) {
		int year = day.get(Calendar.YEAR);
		int dayOfYear = day.get(Calendar.DAY_OF_YEAR);
		return year*1000 + dayOfYear;
	}
}
