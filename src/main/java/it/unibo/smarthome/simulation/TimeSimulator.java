package it.unibo.smarthome.simulation;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import it.unibo.smarthome.constants.EnvVariable;
import it.unibo.smarthome.simulation.Environment.Day;

public class TimeSimulator implements IValueSimulator<Calendar> {

	private Calendar time;
	private Double speedMultiplier;

	public TimeSimulator() {
		this.time = Calendar.getInstance();
		this.speedMultiplier = 1.0;
		
		Runnable timeCalculator = () -> {
	    	time.add(Calendar.MILLISECOND, (int) (1000 * speedMultiplier));
		};

		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(timeCalculator, 0, 1, TimeUnit.SECONDS);
	}
	
	public Calendar getValue() {
		return (Calendar) this.time.clone();
	}

	public Calendar getValue(Calendar time, Day today) {
		return this.getValue();
	}
	
	void setSpeed(Double speedMultiplier) {
		this.speedMultiplier = speedMultiplier;
	}
	
	void setValue(Calendar time) {
		this.time = time;
	}

	public EnvVariable getHandledVariable() {
		return EnvVariable.TIME;
	}
}
