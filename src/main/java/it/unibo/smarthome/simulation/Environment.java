package it.unibo.smarthome.simulation;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.constants.EnvVariable;
import it.unibo.smarthome.constants.WeatherCondition;
import it.unibo.smarthome.utils.DateUtils;

/**
 * Classe che permette la lettura delle variabili in un ambiente simulato.
 * 
 * @author Gioele Masini
 *
 */
public class Environment {
	private static Environment SINGLETON;
	private static Logger log = LogManager.getLogger();
	
	public static Environment getInstance() {
		if (SINGLETON == null) {
			SINGLETON = new Environment();
		}
		return SINGLETON;
	}
	
	private final SimulationDataCollection<Day> data;
	private final HashMap<EnvVariable, IValueSimulator<?>> variableSimulators;
	
	private Environment() {
		this.data = new SimulationDataCollection<Day>();
		this.variableSimulators = new HashMap<EnvVariable, IValueSimulator<?>>();
		this.variableSimulators.put(EnvVariable.TEMPERATURE, new TemperatureSimulator());
		this.variableSimulators.put(EnvVariable.TIME, new TimeSimulator());
		
		this.setTimeSpeed(300.0); // Un secondo reale corrisponde a 5 minuti simulati (300 secondi)
	}

	private Day simulateDay(Calendar date) {
		log.info("Calculating simulation data for day " + DateUtils.dateString(date));
		
		Map<WeatherCondition, Integer> conditions = new HashMap<WeatherCondition, Integer>();
		Day day = new Day(conditions);
		
		return day;
	}
	
	public Double getTemperature() {
		Calendar now = this.getTime();
		return ((TemperatureSimulator) this.variableSimulators.get(EnvVariable.TEMPERATURE))
				.getValue(now, getDayData(now));
	}
	
	public Calendar getTime() {
		return ((TimeSimulator) this.variableSimulators.get(EnvVariable.TIME))
				.getValue();
	}
	
	public void setTimeSpeed(Double speedMultiplier) {
		((TimeSimulator) this.variableSimulators.get(EnvVariable.TIME))
		.setSpeed(speedMultiplier);
	}

	public Day getDayData(Calendar day) {
		if (!this.data.has(day)) {
			this.data.put(day, this.simulateDay(day));
		}
		return this.data.get(day);
	}
	
	public class Day {
		private final Map<WeatherCondition, Integer> conditions;

		public Day(Map<WeatherCondition, Integer> conditions) {
			this.conditions = conditions;
		}

		public Map<WeatherCondition, Integer> getConditions() {
			return conditions;
		}
	}
	
	/**
	 * Semplice metodo di test che mostra il funzionamento di questa classe
	 */
	public static void testMe() {
		Environment env = Environment.getInstance();
		env.setTimeSpeed(3600.0);	// Ogni secondo è un'ora

		log.info("Hour   Temperature");
		
		Runnable tempReader = () -> {
			// SimpleDateFormat non è thread-safe, trasformo la data in stringa manualmente
			// https://stackoverflow.com/questions/18589986/date-conversion-with-threadlocal
			Double temp = env.getTemperature();
			Calendar time = env.getTime();
			log.info(DateUtils.hourString(time) + "  " + String.format("%1$.2f°C", temp));
		};

		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(tempReader, 0, 1, TimeUnit.SECONDS);
	}
}
