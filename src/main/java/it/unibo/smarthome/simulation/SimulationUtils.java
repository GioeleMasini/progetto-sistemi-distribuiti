package it.unibo.smarthome.simulation;

public class SimulationUtils {
	
	static public Point getValueByTime(Point a, Point b, Point c) {
		if (c.getX() == null) {
			// Devo trovare la X
			// x = (y - y1) / (y2 - y1) * (x2 - x1) + x1
			double x = (c.getY() - a.getY()) / (b.getY() - a.getY()) * (b.getX() - a.getX()) + a.getX();
			return new Point(x, c.getY());
		}
		if (c.getY() == null) {
			// Devo trovare la Y
			// y = (x - x1) / (x2 - x1) * (y2 - y1) + y1
			double y = (c.getX() - a.getX()) / (b.getX() - a.getX()) * (b.getY() - a.getY()) + a.getY();
			return new Point(c.getX(), y);
		}
		
		return c;
	}
	
	static public class Point {
		private final Double x;
		private final Double y;

		public Point(Double x, Double y) {
			this.x = x;
			this.y = y;
		}
		
		public Double getX() {
			return x;
		}
		public Double getY() {
			return y;
		}
	}
}
