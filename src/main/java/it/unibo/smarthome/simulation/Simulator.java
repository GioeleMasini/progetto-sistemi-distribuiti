package it.unibo.smarthome.simulation;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.service.TucsonNodeService;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_ACTIVITY_STATE;
import it.unibo.smarthome.constants.Settings;
import it.unibo.smarthome.devices.IAlarm;
import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.devices.impl.AlarmSimple;
import it.unibo.smarthome.devices.impl.PhoneSimple;
import it.unibo.smarthome.exception.AuthenticationFailException;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.StaleProxyException;

public class Simulator {

	private static Logger log = LogManager.getLogger();
	private AgentContainer mainContainer;
	private String tname;
	private static Map<Integer, TucsonNodeService> tnsMap;	
	
	public Simulator() {
		init();
	}
	
	private void init() {
		
		// ---TUCSON NODE SERVICE---
		tname = startNS(Settings.TNS_PORT);
		
		// ---JADE PLATFORM---
		jade.core.Runtime rt = jade.core.Runtime.instance();

		Profile profile = new ProfileImpl(Settings.MC_HOST, Settings.MC_PORT, null);
		
		// abilita Tucson4Jade :)
		profile.setParameter(Profile.SERVICES, "it.unibo.tucson.jade.service.TucsonService");
		
		// DF and AMS are included
		mainContainer = rt.createMainContainer(profile);
		
		try {
			 //avviaTennisti();
			avviaSimPhone();
		} catch (StaleProxyException e) {
			log.error("Errore durante creazione agenti: " + e.getMessage());
		}
		
	}
	
	@SuppressWarnings("unused")
	private void avviaSimPhone() throws StaleProxyException {
		IDevice simple = new PhoneSimple(mainContainer, tname);
		simple.start();
		
		IAlarm alarmSim = new AlarmSimple(mainContainer, tname);
		alarmSim.start();
		try {
			alarmSim.setState(ALARM_ACTIVITY_STATE.ON, "DEFAULT_PASS");
		} catch (AuthenticationFailException e) {
			log.info("password dell'allarme errata");
		}
	}
	
	/**
	 * Avvia il tucson node service su localhost
	 * 
	 * @param port
	 *            the listening port for the TuCSoN Node service
	 * @return boolean indicante il successo dell'operazione
	 */
	private static String startNS(int port) {		
		
		if (tnsMap == null) {
			tnsMap = new HashMap<Integer, TucsonNodeService>();
		}

		if (tnsMap.containsKey(port)) {
			log.error("Un altro tns potrebbe essere attivo su questa porta.");
		}

		try {
			TucsonNodeService ns = new TucsonNodeService(port);
			ns.install();
			tnsMap.put(port, ns);
		} catch (Exception e) {
			// E.g. another TuCSoN Node is running on same port.
			log.error("Errore: " + e.getMessage());
			return null;
		}
		
		log.info("TNS avviato correttamente sulla porta " + port);
		
		String currentHostname = "localhost";
		
		try {
			currentHostname = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.error("Errore: " + e.getMessage());
		}
		
		return "default@"+ currentHostname +":" + port;
	}
}
