package it.unibo.smarthome.simulation;

import java.util.Calendar;

import it.unibo.smarthome.constants.EnvVariable;
import it.unibo.smarthome.simulation.Environment.Day;

public interface IValueSimulator<VALUE_TYPE> {
	
	EnvVariable getHandledVariable();
	
	VALUE_TYPE getValue(Calendar time, Day today);
}
