package it.unibo.smarthome.simulation;

import java.util.Calendar;
import java.util.Map;

import it.unibo.smarthome.constants.EnvVariable;
import it.unibo.smarthome.constants.WeatherCondition;
import it.unibo.smarthome.simulation.Environment.Day;
import it.unibo.smarthome.simulation.SimulationUtils.Point;
import it.unibo.smarthome.utils.DateUtils;

public class TemperatureSimulator implements IValueSimulator<Double> {
	private static final double HOUR_WITH_MAX_TEMPERATURE = 16;
	private static final double HOUR_WITH_MIN_TEMPERATURE = 4;

	private final SimulationDataCollection<TemperatureData> data;

	public TemperatureSimulator() {
		this.data = new SimulationDataCollection<TemperatureData>();
	}

	public Double getValue(Calendar now, Day today) {
		if (!this.data.has(now)) {
			this.generateData(now);
		}
		TemperatureData todayData = this.data.get(now);

		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minutes = now.get(Calendar.MINUTE);
		int totalMinutes = hour * 60 + minutes;

		// Genero un punto con la sola variabile conosciuta: il tempo
		Point currentPoint = new Point(null, Double.valueOf(totalMinutes));

		// Calcolo la variabile sconosciuta: la temperatura corrispondente al tempo scelto
		if (hour >= HOUR_WITH_MIN_TEMPERATURE && hour < HOUR_WITH_MAX_TEMPERATURE) {
			// Fase di calore crescente - Tra mattina e pomeriggio
			currentPoint = SimulationUtils.getValueByTime(todayData.getMinPoint(), todayData.getMaxPoint(),
					currentPoint);
		} else if (hour < HOUR_WITH_MIN_TEMPERATURE) {
			// Fase di calore decrescente - Mattina presto
			Calendar yesterday = DateUtils.getYesterday(now);
			if (!this.data.has(yesterday)) {
				this.generateData(yesterday);
			}

			currentPoint = SimulationUtils.getValueByTime(this.data.get(yesterday).getMaxPointAsYesterday(),
					todayData.getMinPoint(), currentPoint);
		} else {
			// Fase di calore decrescente - Sera
			Calendar tomorrow = DateUtils.getTomorrow(now);
			if (!this.data.has(tomorrow)) {
				this.generateData(tomorrow);
			}

			currentPoint = SimulationUtils.getValueByTime(todayData.getMaxPoint(),
					this.data.get(tomorrow).getMinPointAsTomorrow(), currentPoint);
		}

		Double currentTemperature = currentPoint.getX();
		
		// Applico i modificatori delle condizioni ambientali
		Map<WeatherCondition, Integer> conditions = today.getConditions();
		for (Map.Entry<WeatherCondition, Integer> entry : conditions.entrySet()) {
			WeatherCondition condition = entry.getKey();
			Integer intensity = entry.getValue();

			currentTemperature *= 100 - (condition.getTemperature() * intensity);
		}

		return currentTemperature;
	}
	
	private void generateData(Calendar day) {
		Point minPoint = new Point(25.0, HOUR_WITH_MIN_TEMPERATURE * 60);
		Point maxPoint = new Point(38.0, HOUR_WITH_MAX_TEMPERATURE * 60);
		TemperatureData dayData = new TemperatureData(minPoint, maxPoint);
		this.data.put(day, dayData);
	}

	public EnvVariable getHandledVariable() {
		return EnvVariable.TEMPERATURE;
	}
	

	private class TemperatureData {
		private final Point minPoint;
		private final Point maxPoint;

		public TemperatureData(Point minPoint, Point maxPoint) {
			super();
			this.minPoint = minPoint;
			this.maxPoint = maxPoint;
		}

		public Point getMinPoint() {
			return minPoint;
		}

		public Point getMaxPoint() {
			return maxPoint;
		}
		
		public Point getMinPointAsTomorrow() {
			Double temperature = this.minPoint.getX();
			Double time = this.minPoint.getY() + 24*60;
			
			return new Point(temperature, time);
		}
		
		public Point getMaxPointAsYesterday() {
			Double temperature = this.maxPoint.getX();
			Double time = this.maxPoint.getY() - 24*60;
			
			return new Point(temperature, time);
		}
	}
}
