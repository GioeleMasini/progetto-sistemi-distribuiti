package it.unibo.smarthome.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.devices.impl.AlarmSimple;
import it.unibo.smarthome.devices.impl.LightSimple;
import it.unibo.smarthome.devices.impl.PhoneSimple;
import it.unibo.smarthome.devices.impl.MotionSensor;
import it.unibo.smarthome.devices.impl.TemperatureSensor;
import it.unibo.smarthome.devices.impl.ThermometerSimple;
import it.unibo.smarthome.devices.impl.ThermometerSmart;
import it.unibo.smarthome.devices.impl.WindowAlarmedSmart;
import it.unibo.smarthome.utils.DeviceUtils;
import it.unibo.smarthome.utils.TucsonUtils;

public class DevicesInstantiatorPane extends JPanel {

	private static final long serialVersionUID = -6406393058343303754L;
	
	private static final List<Class<? extends IDevice>> devices = new ArrayList<>();
	static {
		devices.add(AlarmSimple.class);
		devices.add(PhoneSimple.class);
		devices.add(LightSimple.class);
		devices.add(TemperatureSensor.class);
		devices.add(MotionSensor.class);
		devices.add(ThermometerSimple.class);
		devices.add(ThermometerSmart.class);
		devices.add(WindowAlarmedSmart.class);
	}
	
	private JTable roomsTable;

	public DevicesInstantiatorPane() {
		super();
		
		initUI();
	}
	
	private void initUI() {
		
		this.setLayout(new BorderLayout());
		
        this.initButtonsBar();
        
        Object[][] tableData = 
        		devices.stream()
        		.map(c -> new String[] {c.getSimpleName(), c.getName()})
        		.toArray(Object[][]::new);
        
        this.roomsTable = new JTable(tableData, new String[] {"Name", "Class"});
        this.add(new JScrollPane(this.roomsTable));
	}
	
	private void initButtonsBar() {
		JPanel btnsBar = new JPanel();
		btnsBar.setLayout(new BoxLayout(btnsBar, BoxLayout.X_AXIS));
		
		final JButton startDeviceButton = new JButton("Start device");
        startDeviceButton.addActionListener((ActionEvent ev) -> {
        	int selectedRowId = this.roomsTable.getSelectedRow();
        	if( selectedRowId >= 0 && selectedRowId < devices.size() ){
	        	Class<? extends IDevice> deviceClass = devices.get(selectedRowId);
	        	
				DeviceUtils.createInstance(deviceClass, TucsonUtils.getNSId());
        	}
        });
		
		btnsBar.add(startDeviceButton);

		this.add(btnsBar, BorderLayout.NORTH);
	}
}
