package it.unibo.smarthome.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import it.unibo.smarthome.dao.DeviceDAO;

public class DevicesTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = -353519702637595796L;

	private final List<DeviceDAO> devicesList;
     
    private final String[] columnNames = new String[] {
            "Id", "Name", "Room Id"
    };
    private final Class<?>[] columnClass = new Class<?>[] {
    	String.class, String.class, String.class
    };
    
    private HashMap<String, DeviceDAO> modifiedDevices;
    
    public DevicesTableModel()
    {
        this(new ArrayList<DeviceDAO>());
    }
 
    public DevicesTableModel(List<DeviceDAO> devicesList)
    {
        this.devicesList = devicesList;
        this.modifiedDevices = new HashMap<String, DeviceDAO>();
    }
     
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
 
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
 
    @Override
    public int getRowCount()
    {
        return devicesList.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        DeviceDAO room = devicesList.get(rowIndex);
        if (0 == columnIndex) {
            return room.getId();
        }
        else if (1 == columnIndex) {
            return room.getName();
        }
        else if (2 == columnIndex) {
            return room.getRoomId();
        }
        return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int colIndex) {
    	DeviceDAO room = devicesList.get(rowIndex);
        switch (colIndex) {
        case 0:
    	    room.setId((String) aValue);
    	    break;
        case 1:
    	    room.setName((String) aValue);
    	    break;
        case 2:
    	    room.setRoomId((String) aValue);
    	    break;
        }
    	this.modifiedDevices.put(room.getId(), room);
    }

	@Override
	public boolean isCellEditable(int rowIndex, int colIndex) {
		if (colIndex == 0) {
			return false;
		}
		return true;
	}

	public void removeRow(int rowIndex) {
		this.devicesList.remove(rowIndex);
		this.fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public void addRow() {
		this.devicesList.add(new DeviceDAO());
		this.fireTableRowsInserted(this.devicesList.size(), this.devicesList.size());
	}
	
	public List<DeviceDAO> getData() {
		return this.devicesList;
	}
	
	public List<DeviceDAO> getModifiedData() {
		List<DeviceDAO> modifiedList = new ArrayList<DeviceDAO>(this.modifiedDevices.values());
		this.modifiedDevices = new HashMap<String, DeviceDAO>();
		return modifiedList;
	}
}
