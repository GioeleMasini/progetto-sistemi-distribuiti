package it.unibo.smarthome.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.WriterAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.layout.PatternLayout;

public class LogPane extends JPanel {

	private static final long serialVersionUID = -8120731257443318769L;
	
	public LogPane() {
		super();
		initUI();
	}

	private void initUI() {

		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) screenSize.getWidth();
    	int height = (int) screenSize.getHeight();
    	this.setPreferredSize(new Dimension(width/4, (2*height)/5));
    	
    	TextAreaWriter logAreaWriter = createLog();
    	JScrollPane scrollPane = new JScrollPane(logAreaWriter.getTextArea());
    	this.add(scrollPane, BorderLayout.CENTER);
	}
	
	private TextAreaWriter createLog() {
		
		TextAreaWriter logAreaWriter = new TextAreaWriter();
		final LoggerContext context = LoggerContext.getContext(false);
	    final Configuration config = context.getConfiguration();
	    
	    PatternLayout layout = PatternLayout.createLayout("%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n", null, config, null, null, false, false, null, null);
	    final Appender appender = WriterAppender.createAppender(layout, null, logAreaWriter, "loggerJTArea", false, true);
	    appender.start();
	    config.addAppender(appender);
		config.getRootLogger().addAppender(appender, null, null);
		
		return logAreaWriter;
	}
}
