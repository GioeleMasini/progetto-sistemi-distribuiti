package it.unibo.smarthome.gui;

import java.awt.Color;
import java.io.IOException;
import java.io.Writer;

import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public final class TextAreaWriter extends Writer {

	private JTextArea area;
	
	public TextAreaWriter() {
		this.area = new JTextArea();
		DefaultCaret caret = (DefaultCaret) this.area.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.area.setEditable(false);
		this.area.setBackground(Color.BLACK);
		this.area.setForeground(Color.WHITE);
	}
	
	public TextAreaWriter(JTextArea area) {
		this.area = area;
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		area.append(new String(cbuf, off, len));
	}

	@Override
	public void close() throws IOException {}

	@Override
	public void flush() throws IOException {}

	public JTextArea getTextArea() {
		return area;
	}
}
