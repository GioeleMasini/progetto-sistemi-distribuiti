package it.unibo.smarthome.gui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import it.unibo.smarthome.dao.RoomDAO;

public class RoomsTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = -353519702637595796L;

	private final List<RoomDAO> roomsList;
     
    private final String[] columnNames = new String[] {
            "Id", "Name", "Floor"
    };
    private final Class<?>[] columnClass = new Class<?>[] {
        String.class, String.class, Integer.class
    };
 
    public RoomsTableModel(List<RoomDAO> roomsList)
    {
        this.roomsList = roomsList;
    }
     
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
 
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
 
    @Override
    public int getRowCount()
    {
        return roomsList.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        RoomDAO room = roomsList.get(rowIndex);
        if (0 == columnIndex) {
            return room.getId();
        }
        else if (1 == columnIndex) {
            return room.getName();
        }
        else if (2 == columnIndex) {
            return room.getFloor();
        }
        return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int colIndex) {
       RoomDAO room = roomsList.get(rowIndex);
       switch (colIndex) {
       case 0:
    	   room.setId((String) aValue);
    	   break;
       case 1:
    	   room.setName((String) aValue);
    	   break;
       case 2:
    	   room.setFloor((Integer) aValue);
    	   break;
       }
    }

	@Override
	public boolean isCellEditable(int rowIndex, int colIndex) {
		return colIndex > 0;
	}

	public void removeRow(int rowIndex) {
		this.roomsList.remove(rowIndex);
		this.fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public void addRow() {
		this.roomsList.add(new RoomDAO());
		this.fireTableRowsInserted(this.roomsList.size(), this.roomsList.size());
	}
	
	public List<RoomDAO> getData() {
		return this.roomsList;
	}
}
