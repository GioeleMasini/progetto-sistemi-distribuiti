package it.unibo.smarthome.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import it.unibo.smarthome.dao.DeviceDAO;
import jade.gui.GuiEvent;

public class DevicesManagerPane extends JPanel {
	private static final long serialVersionUID = 1L;

	private IMainGuiController ctrl;
	
	private JTable devicesTable;
	private DevicesTableModel devicesModel;
	private JTable configsTable;
	private ConfigTableModel configsModel;

	public DevicesManagerPane(IMainGuiController ctrl) {
		super();
		
		this.ctrl = ctrl;
		
		initUI();
	}
	
	private void initUI() {
		
		this.setLayout(new BorderLayout());
		
		this.initButtonsBar();

        this.devicesModel = new DevicesTableModel();
        this.devicesTable = new JTable(this.devicesModel);
        this.devicesTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        this.devicesTable.getSelectionModel().addListSelectionListener((e) -> {
        	if (!e.getValueIsAdjusting() && this.devicesTable.getSelectedRow() != -1) {
        		// Pass to the agent the event.
                final GuiEvent ge = new GuiEvent(this.devicesTable, MainWindowEvent.READ_DEVICE_CONFIGS.getValue());
                ge.addParameter(this.getSelectedDevice());
                this.ctrl.postGuiEvent(ge);
        	}
        });
        this.add(new JScrollPane(this.devicesTable), BorderLayout.WEST);
        
        this.configsModel = new ConfigTableModel();
        this.configsTable = new JTable(this.configsModel);
        this.configsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        this.add(new JScrollPane(this.configsTable), BorderLayout.CENTER);
        
        this.validate();
	}
	
	private void initButtonsBar() {
		JPanel btnsBar = new JPanel();
		btnsBar.setLayout(new BoxLayout(btnsBar, BoxLayout.X_AXIS));
		
		final JButton refreshDeviceButton = new JButton("Refresh devices");
		refreshDeviceButton.addActionListener((ActionEvent ev) -> {
        	// Pass to the agent the event.
            final GuiEvent ge = new GuiEvent(refreshDeviceButton, MainWindowEvent.READ_DEVICES.getValue());
            this.ctrl.postGuiEvent(ge);
        });
		
		btnsBar.add(refreshDeviceButton);
		
        final JButton deleteDeviceButton = new JButton("Stop device");
        deleteDeviceButton.addActionListener((ActionEvent ev) -> {
        	DeviceDAO device = this.getSelectedDevice();
        	if (device != null) {
	        	this.devicesModel.removeRow(this.devicesTable.getSelectedRow());
	        	// Pass to the agent the event.
	            final GuiEvent ge = new GuiEvent(deleteDeviceButton, MainWindowEvent.STOP_DEVICE.getValue());
	            ge.addParameter(device);
	            this.ctrl.postGuiEvent(ge);
        	}
        });
		
        btnsBar.add(deleteDeviceButton);
		
        final JButton saveDevicesButton = new JButton("Save devices");
        saveDevicesButton.addActionListener((ActionEvent ev) -> {
        	// Pass to the agent the event.
            final GuiEvent ge = new GuiEvent(saveDevicesButton, MainWindowEvent.SAVE_DEVICES.getValue());
            ge.addParameter(this.devicesModel.getModifiedData());
            this.ctrl.postGuiEvent(ge);
        });
		
        btnsBar.add(saveDevicesButton);
		
        final JButton saveConfigsButton = new JButton("Save device configs");
        saveConfigsButton.addActionListener((ActionEvent ev) -> {
        	DeviceDAO device = this.getSelectedDevice();
        	if (device != null) {
	        	// Pass to the agent the event.
	            final GuiEvent ge = new GuiEvent(saveConfigsButton, MainWindowEvent.SAVE_DEVICE_CONFIGS.getValue());
	            ge.addParameter(device);
	            this.ctrl.postGuiEvent(ge);
        	}
        });
		
        btnsBar.add(saveConfigsButton);

		this.add(btnsBar, BorderLayout.NORTH);
	}
	
	public void setDevices(List<DeviceDAO> devices) {
        this.devicesModel = new DevicesTableModel(devices);
        this.devicesTable.setModel(this.devicesModel);
	}
	
	public void setConfigs(Map<String, String> configs) {
        this.configsModel = new ConfigTableModel(configs);
        this.configsTable.setModel(this.configsModel);
	}
	
	public List<DeviceDAO> getDevices() {
		return this.devicesModel.getData();
	}
	
	public HashMap<String, String> getConfigs() {
		return this.configsModel.getData();
	}
	
	private DeviceDAO getSelectedDevice() {
		int selectedIndex = this.devicesTable.getSelectedRow();
		if (selectedIndex != -1) {
    		return this.getDevices().get(selectedIndex);
    	}
		return null;
	}
}
