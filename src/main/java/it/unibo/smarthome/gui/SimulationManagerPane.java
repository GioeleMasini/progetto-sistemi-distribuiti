package it.unibo.smarthome.gui;

import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jade.gui.GuiEvent;

public class SimulationManagerPane extends JPanel {

	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String HOUR_FORMAT = "HH:mm";

	private static final long serialVersionUID = 1257234738824319509L;
	private IMainGuiController ctrl;
	
	// Elementi del pannello
	
	// Velocità del tempo
	private JLabel timeSpeedLabel;
	private NumberFormat timeSpeedFormat;
	private JFormattedTextField timeSpeedField;
	
	// Data odierna
	private JLabel todayDateLabel;
	private DateFormat todayDateFormat;
	private JFormattedTextField todayDateField;
	
	// Ora
	private JLabel nowHourLabel;
	private DateFormat nowHourFormat;
	private JFormattedTextField nowHourField;

	// Bottone salva
	private JButton saveSettingsButton;
	
	public SimulationManagerPane(IMainGuiController ctrl) {
		super();

		this.ctrl = ctrl;

		initFormatters();
		initUI();
	}

	private void initUI() {

		// TIME SPEED
		this.timeSpeedLabel = new JLabel("Set time speed"); 
		this.add(this.timeSpeedLabel);
		this.timeSpeedField = new JFormattedTextField(this.timeSpeedFormat);
		timeSpeedField.setValue(Double.valueOf(1));
		timeSpeedField.setColumns(3);
		this.add(timeSpeedField);

		Date today = new Date();
		
		// TODAY DATE
		this.todayDateLabel = new JLabel("Set today date");
		this.add(todayDateLabel);
		this.todayDateField = new JFormattedTextField(this.todayDateFormat);
		this.todayDateField.setColumns(8);
		Date todayWithZeroTime;
		try {
			todayWithZeroTime = this.todayDateFormat.parse(this.todayDateFormat.format(today));
			this.todayDateField.setValue(todayWithZeroTime);
		} catch (Exception exp) {
			System.out.println("Impossibile impostare data");
		}
		this.add(this.todayDateField);

		// NOW HOUR
		this.nowHourLabel = new JLabel("Set hour");
		this.add(this.nowHourLabel);
		this.nowHourField = new JFormattedTextField(this.nowHourFormat);
		this.nowHourField.setColumns(6);
		Date nowHour;
		try {
			nowHour = this.nowHourFormat.parse(this.nowHourFormat.format(today));
			this.nowHourField.setValue(nowHour);
		} catch (Exception exp) {
			System.out.println("Impossibile impostare ora");
		}
		this.add(this.nowHourField);
		
		/*
		 * Pass to the agent the event.
		 */
		this.saveSettingsButton = new JButton("Save settings");
		this.saveSettingsButton.addActionListener((ActionEvent ev) -> {
			final GuiEvent ge = new GuiEvent(this.saveSettingsButton, MainWindowEvent.SIM_PARAMS_CHANGED.getValue());
			this.ctrl.postGuiEvent(ge);
		});

		this.add(this.saveSettingsButton);
	}
	
	private void initFormatters() {
		this.timeSpeedFormat = NumberFormat.getNumberInstance();
		this.timeSpeedFormat.setMaximumFractionDigits(2);
		
		this.todayDateFormat = new SimpleDateFormat(DATE_FORMAT);
		
		this.nowHourFormat = new SimpleDateFormat(HOUR_FORMAT);
	}
	
	public String getTimeSpeed() {
		return this.timeSpeedField.getValue().toString();
	}
	
	public String getTodayDate() {
		return this.todayDateField.getValue().toString();
	}
	
	public String getNowHour() {
		return this.nowHourField.getValue().toString();
	}
}
