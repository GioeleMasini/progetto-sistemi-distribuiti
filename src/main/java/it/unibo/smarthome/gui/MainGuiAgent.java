package it.unibo.smarthome.gui;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.TucsonGuiAgent;
import it.unibo.smarthome.behaviours.ReadConfigsBehaviour;
import it.unibo.smarthome.behaviours.ReadDataStoreBehaviour;
import it.unibo.smarthome.behaviours.ReadDevicesBehaviour;
import it.unibo.smarthome.behaviours.ReadRoomsBehaviour;
import it.unibo.smarthome.behaviours.RequestStopDeviceBehaviour;
import it.unibo.smarthome.behaviours.RequestTestActionBehaviour;
import it.unibo.smarthome.behaviours.RequestUpdateDeviceDataBehaviour;
import it.unibo.smarthome.behaviours.SaveDeviceConfigsBehaviour;
import it.unibo.smarthome.behaviours.SaveRoomsBehaviour;
import it.unibo.smarthome.behaviours.SimulationParameterChangedBehaviour;
import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.dao.RoomDAO;
import jade.gui.GuiEvent;
import jade.wrapper.AgentContainer;

public class MainGuiAgent extends TucsonGuiAgent implements IMainGuiController {

	private static final long serialVersionUID = 7459623051523250299L;
	private MainWindow mainWindow;
	private static Logger log = LogManager.getLogger();
	
	public MainGuiAgent() {
		this.mainWindow = new MainWindow(this);
		this.addBehaviour(new ReadRoomsBehaviour(this::updateRoomsTable));
		// this.addBehaviour(new ReadDevicesBehaviour(this::updateDevicesTable));
	}

    /*
     * Automagically called by JADE platform to handle GUI-generated events. In
     * particular, it is called right after method <postGuiEvent()> returns.
     */
	@Override
	protected void onGuiEvent(GuiEvent e) {
		if (MainWindowEvent.SIM_PARAMS_CHANGED.equals(e.getType())) 
		{
        	this.addBehaviour(new SimulationParameterChangedBehaviour(this.mainWindow.getSimulationPane()));
		}
		else if (MainWindowEvent.SAVE_ROOMS.equals(e.getType())) 
		{
			@SuppressWarnings("unchecked")
			List<RoomDAO> rooms = (List<RoomDAO>) e.getParameter(0);
			this.addBehaviour(new SaveRoomsBehaviour(rooms, this::updateRoomsTable));
        }
		else if (MainWindowEvent.READ_DEVICES.equals(e.getType())) 
		{
    		this.addBehaviour(new ReadDevicesBehaviour(this::updateDevicesTable));
        }
		else if (MainWindowEvent.STOP_DEVICE.equals(e.getType())) 
		{
        	DeviceDAO selectedDevice = (DeviceDAO) e.getParameter(0);
    		this.addBehaviour(new RequestStopDeviceBehaviour(selectedDevice));
        }
		else if (MainWindowEvent.SAVE_DEVICES.equals(e.getType())) 
		{
        	@SuppressWarnings("unchecked")
			List<DeviceDAO> devices = (List<DeviceDAO>) e.getParameter(0);
        	for (DeviceDAO d : devices) {
            	this.addBehaviour(new RequestUpdateDeviceDataBehaviour(d));
        	}
        }
		else if (MainWindowEvent.READ_DEVICE_CONFIGS.equals(e.getType())) 
		{
        	DeviceDAO selectedDevice = (DeviceDAO) e.getParameter(0);
    		this.addBehaviour(new ReadConfigsBehaviour(selectedDevice, this::updateConfigsTable));
        }
		else if (MainWindowEvent.SAVE_DEVICE_CONFIGS.equals(e.getType())) 
		{
        	DeviceDAO selectedDevice = (DeviceDAO) e.getParameter(0);
        	Map<String, String> configs = this.mainWindow.getDevicesManagerPane().getConfigs();
    		this.addBehaviour(new SaveDeviceConfigsBehaviour(selectedDevice, configs));
        }
		else if (MainWindowEvent.READ_DEVICE_DATASTORE.equals(e.getType())) 
		{
			DeviceDAO selectedDevice = (DeviceDAO) e.getParameter(0);
    		this.addBehaviour(new ReadDataStoreBehaviour(selectedDevice, this::updateDataStoreTable));
        }
		else if (MainWindowEvent.TEST_ACTION.equals(e.getType())) 
		{
			DeviceDAO selectedDevice = (DeviceDAO) e.getParameter(0);
			String action = (String) e.getParameter(1);
    		this.addBehaviour(new RequestTestActionBehaviour(selectedDevice, action));
        }
		else if (MainWindowEvent.CLOSE_GUI.equals(e.getType())) 
		{
            log.info("Closing GUI.");
			this.doDelete();
        }
		else 
		{
            log.warn("Unknown GUI event: " + e.getType());
            // this.mainWindow.dispose();
            // this.doDelete();
        }
    }
	
	private void updateRoomsTable(List<RoomDAO> rooms) {
		this.mainWindow.getRoomsManagerPane().setRooms(rooms);
	}
	
	private void updateDevicesTable(List<DeviceDAO> devices) {
		this.mainWindow.getDevicesManagerPane().setDevices(devices);
		this.mainWindow.getTestingPane().setDevices(devices);
	}
	
	private void updateConfigsTable(Map<String, String> configs) {
		this.mainWindow.getDevicesManagerPane().setConfigs(configs);
	}
	
	private void updateDataStoreTable(Map<String, Object> dataStore) {
		this.mainWindow.getTestingPane().setDataStore(dataStore);
	}

	@Override
	public AgentContainer getMainAgentContainer() {
		return this.getContainerController();
	}
}
