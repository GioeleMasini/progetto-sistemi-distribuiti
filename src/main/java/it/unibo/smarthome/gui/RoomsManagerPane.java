package it.unibo.smarthome.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import it.unibo.smarthome.dao.RoomDAO;
import jade.gui.GuiEvent;

public class RoomsManagerPane extends JPanel {
	private static final long serialVersionUID = 7879622589306192837L;

	private IMainGuiController ctrl;
	
	private JTable roomsTable;
	private RoomsTableModel roomsModel;

	public RoomsManagerPane(IMainGuiController ctrl) {
		super();
		
		this.ctrl = ctrl;
		
		initUI();
	}
	
	private void initUI() {
		
		this.setLayout(new BorderLayout());
		
        this.initButtonsBar();
        
        this.roomsTable = new JTable();
        this.add(new JScrollPane(this.roomsTable));
	}
	
	private void initButtonsBar() {
		JPanel btnsBar = new JPanel();
		btnsBar.setLayout(new BoxLayout(btnsBar, BoxLayout.X_AXIS));
		
        final JButton createRoomButton = new JButton("Create room");
        createRoomButton.addActionListener((ActionEvent ev) -> {
        	this.roomsModel.addRow();
        });
		
        btnsBar.add(createRoomButton);
		
        final JButton deleteRoomButton = new JButton("Delete room");
        deleteRoomButton.addActionListener((ActionEvent ev) -> {
        	this.roomsModel.removeRow(this.roomsTable.getSelectedRow());
        });
		
        btnsBar.add(deleteRoomButton);
		
        final JButton saveRoomsButton = new JButton("Save");
        saveRoomsButton.addActionListener((ActionEvent ev) -> {
        	// Pass to the agent the event.
            final GuiEvent ge = new GuiEvent(saveRoomsButton, MainWindowEvent.SAVE_ROOMS.getValue());
            ge.addParameter(this.roomsModel.getData());
            this.ctrl.postGuiEvent(ge);
        });
		
        btnsBar.add(saveRoomsButton);

		this.add(btnsBar, BorderLayout.NORTH);
	}
	
	public void setRooms(List<RoomDAO> rooms) {
        this.roomsModel = new RoomsTableModel(rooms);
        this.roomsTable.setModel(this.roomsModel);
	}
}
