package it.unibo.smarthome.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import it.unibo.smarthome.utils.Pair;

public class ConfigTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = -353519702637595796L;

	private final ArrayList<Pair<String, String>> configsList;
     
    private final String[] columnNames = new String[] {
            "Name", "Value"
    };
    private final Class<?>[] columnClass = new Class<?>[] {
        String.class, String.class
    };
    
    public ConfigTableModel()
    {
        this(new HashMap<String, String>());
    }
 
    public ConfigTableModel(Map<String, String> configs)
    {
    	ArrayList<Pair<String, String>> configsList = new ArrayList<Pair<String, String>>();
        configs.forEach((key, value) -> {
        	configsList.add(new Pair<String, String>(key, value));
        });
        this.configsList = configsList;
    }
     
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
 
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
 
    @Override
    public int getRowCount()
    {
        return configsList.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
    	Pair<String, String> config = configsList.get(rowIndex);
        if (0 == columnIndex) {
            return config.getFirst();
        }
        else if (1 == columnIndex) {
            return config.getSecond();
        }
        return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int colIndex) {
    	Pair<String, String> room = configsList.get(rowIndex);
        switch (colIndex) {
        //case 0:
        	//room.setFirst((String) aValue);
    	    //break;
        case 1:
    	    room.setSecond((String) aValue);
    	    break;
        }
    }

	@Override
	public boolean isCellEditable(int rowIndex, int colIndex) {
		return colIndex > 0;
	}

	/* public void removeRow(int rowIndex) {
		this.configsList.remove(rowIndex);
		this.fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public void addRow() {
		this.configsList.add(new RoomDAO());
		this.fireTableRowsInserted(this.configsList.size(), this.configsList.size());
	} */
	
	public HashMap<String, String> getData() {
		HashMap<String, String> data = new HashMap<String, String>();
		this.configsList.forEach((pair) -> {
			data.put(pair.getFirst(), pair.getSecond());
		});
		return data;
	}
}
