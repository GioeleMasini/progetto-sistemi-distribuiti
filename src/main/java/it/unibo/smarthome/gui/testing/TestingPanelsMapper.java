package it.unibo.smarthome.gui.testing;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.devices.impl.AlarmSimple;
import it.unibo.smarthome.devices.impl.LightSimple;
import it.unibo.smarthome.devices.impl.MotionSensor;
import it.unibo.smarthome.devices.impl.WindowAlarmedSmart;
import it.unibo.smarthome.gui.IMainGuiController;

public class TestingPanelsMapper {
	
	private static Logger log = LogManager.getLogger();
	
	private static final HashMap<Class<? extends IDevice>, Class<? extends TestActionsPane>> mapping = new HashMap<Class<? extends IDevice>, Class<? extends TestActionsPane>>();
	static {
		mapping.put(LightSimple.class, LightTestActionsPane.class);
		mapping.put(WindowAlarmedSmart.class, WindowAlarmSmartTestActionPane.class);
		mapping.put(AlarmSimple.class, AlarmTestActionsPane.class);
		mapping.put(MotionSensor.class, MotionSensorTestActionsPane.class);
	}
	
	public static JPanel createTestActionsPanel(DeviceDAO device, IMainGuiController ctrl) {
		Class<? extends TestActionsPane> mappedClass = mapping.get(device.getDeviceClass());
		JPanel panel = null;
		if (mappedClass != null) {
			try {
				panel = mappedClass.getConstructor(IMainGuiController.class, DeviceDAO.class).newInstance(ctrl, device);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				log.error("Problema nell'istanziazione del pannello per il device " + device.getDeviceClass().getSimpleName() + ": " + e.getMessage());
			}
		}
		return panel;
	}
}
