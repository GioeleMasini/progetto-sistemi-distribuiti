package it.unibo.smarthome.gui.testing;

import java.awt.AWTEvent;
import javax.swing.JPanel;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.gui.IMainGuiController;
import it.unibo.smarthome.gui.MainWindowEvent;
import jade.gui.GuiEvent;

public abstract class TestActionsPane extends JPanel {
	private static final long serialVersionUID = -1753641339426495547L;

	private final IMainGuiController ctrl;
	private final DeviceDAO device;
	
	public TestActionsPane(IMainGuiController ctrl, DeviceDAO device) {
		super();
		
		this.ctrl = ctrl;
		this.device = device;
		
		initUI();
	}
	
	public IMainGuiController getGuiCtrl() {
		return this.ctrl;
	}

	public DeviceDAO getDevice() {
		return device;
	}
	
	public void requestAction(String action, AWTEvent ev) {
    	// Pass to the agent the event.
        final GuiEvent ge = new GuiEvent(ev.getSource(), MainWindowEvent.TEST_ACTION.getValue());
        ge.addParameter(this.getDevice());
        ge.addParameter(action);
        this.getGuiCtrl().postGuiEvent(ge);
	}
	
	protected abstract void initUI();
}
