package it.unibo.smarthome.gui.testing;

import java.awt.FlowLayout;

import javax.swing.JButton;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.gui.IMainGuiController;

public class WindowAlarmSmartTestActionPane  extends TestActionsPane {

	private static final long serialVersionUID = -1634395078381146670L;

	public WindowAlarmSmartTestActionPane(IMainGuiController ctrl, DeviceDAO device) {
		super(ctrl, device);
	}

	protected void initUI() {
		
		this.setLayout(new FlowLayout());
		
		JButton btnTestAlarm = new JButton("Test Alarm");
		btnTestAlarm.addActionListener(ev -> this.requestAction("test-alarm", ev));
		
		JButton btnOpenWindow = new JButton("Open Window");
		btnOpenWindow.addActionListener(ev -> this.requestAction("open-window", ev));
		
		JButton btnCloseWindow = new JButton("Close Window");
		btnCloseWindow.addActionListener(ev -> this.requestAction("close-window", ev));

		this.add(btnTestAlarm);
		this.add(btnCloseWindow);
		this.add(btnOpenWindow);
        
        this.validate();
	}
}
