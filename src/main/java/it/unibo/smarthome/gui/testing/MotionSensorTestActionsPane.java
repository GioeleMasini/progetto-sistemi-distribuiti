package it.unibo.smarthome.gui.testing;

import java.awt.FlowLayout;

import javax.swing.JButton;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.gui.IMainGuiController;

public class MotionSensorTestActionsPane extends TestActionsPane {

	private static final long serialVersionUID = -1573660638911275450L;

	public MotionSensorTestActionsPane(IMainGuiController ctrl, DeviceDAO device) {
		super(ctrl, device);
	}

	@Override
	protected void initUI() {
		
		this.setLayout(new FlowLayout());
		
		JButton btnToggleMovement = new JButton("Toggle movement");
		btnToggleMovement.addActionListener(ev -> this.requestAction("toggle-movement", ev));
		
		this.add(btnToggleMovement);
        
        this.validate();
	}

}
