package it.unibo.smarthome.gui.testing;

import java.awt.FlowLayout;

import javax.swing.JButton;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.gui.IMainGuiController;

public class LightTestActionsPane extends TestActionsPane {

	private static final long serialVersionUID = -4356577888561666128L;

	public LightTestActionsPane(IMainGuiController ctrl, DeviceDAO device) {
		super(ctrl, device);
	}

	protected void initUI() {
		
		this.setLayout(new FlowLayout());
		
		JButton btnToggleLight = new JButton("Toggle light");
		btnToggleLight.addActionListener(ev -> this.requestAction("toggle-light", ev));
		
		JButton btnToggleAutomatic = new JButton("Toggle automatic mode");
		btnToggleAutomatic.addActionListener(ev -> this.requestAction("toggle-automatic-mode", ev));
		
		this.add(btnToggleLight);
		this.add(btnToggleAutomatic);
        
        this.validate();
	}
}
