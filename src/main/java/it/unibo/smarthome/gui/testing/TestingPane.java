package it.unibo.smarthome.gui.testing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.gui.DevicesTableModel;
import it.unibo.smarthome.gui.IMainGuiController;
import it.unibo.smarthome.gui.MainWindowEvent;
import jade.gui.GuiEvent;

public class TestingPane extends JPanel {
	private static final long serialVersionUID = -1753641339426495547L;

	private IMainGuiController ctrl;
	
	private JTable devicesTable;
	private DevicesTableModel devicesModel;
	private JTable datastoreTable;
	private DataStoreTableModel datastoreModel;
	private JPanel actionsPanel;

	public TestingPane(IMainGuiController ctrl) {
		super();
		
		this.ctrl = ctrl;
		
		initUI();
	}
	
	private void initUI() {
		
		this.setLayout(new BorderLayout());
		
		this.initButtonsBar();

		// Left column
        this.devicesModel = new DevicesTableModel();
        this.devicesTable = new JTable(this.devicesModel);
        this.devicesTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        this.devicesTable.getSelectionModel().addListSelectionListener((e) -> {
        	if (!e.getValueIsAdjusting() && this.devicesTable.getSelectedRow() != -1) {
        		// Pass to the agent the event.
                final GuiEvent ge = new GuiEvent(this.devicesTable, MainWindowEvent.READ_DEVICE_DATASTORE.getValue());
                DeviceDAO device = this.getSelectedDevice();
                ge.addParameter(device);
                this.ctrl.postGuiEvent(ge);
                
                this.setActionsPanel(device);
        	}
        });
        this.add(new JScrollPane(this.devicesTable), BorderLayout.WEST);
        
        // Righ column
        JPanel rightColumn = new JPanel();
        rightColumn.setLayout(new BoxLayout(rightColumn, BoxLayout.Y_AXIS));
        
        this.datastoreModel = new DataStoreTableModel();
        this.datastoreTable = new JTable(this.datastoreModel);
        this.datastoreTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        this.datastoreTable.setPreferredScrollableViewportSize(new Dimension(Integer.MAX_VALUE, 200));
        rightColumn.add(new JScrollPane(this.datastoreTable));
        
        this.actionsPanel = new JPanel();
        this.actionsPanel.setMaximumSize(new Dimension(520, Integer.MAX_VALUE));
        this.actionsPanel.setLayout(new BoxLayout(this.actionsPanel, BoxLayout.Y_AXIS));
        rightColumn.add(this.actionsPanel);
        
        this.add(rightColumn, BorderLayout.CENTER);
        
        this.validate();
	}
	
	private void initButtonsBar() {
		JPanel btnsBar = new JPanel();
		btnsBar.setLayout(new BoxLayout(btnsBar, BoxLayout.X_AXIS));
		
		final JButton refreshDeviceButton = new JButton("Refresh devices");
		refreshDeviceButton.addActionListener((ActionEvent ev) -> {
        	// Pass to the agent the event.
            final GuiEvent ge = new GuiEvent(refreshDeviceButton, MainWindowEvent.READ_DEVICES.getValue());
            this.ctrl.postGuiEvent(ge);
        });
		
		btnsBar.add(refreshDeviceButton);

		this.add(btnsBar, BorderLayout.NORTH);
	}
	
	public void setDevices(List<DeviceDAO> devices) {
        this.devicesModel = new DevicesTableModel(devices);
        this.devicesTable.setModel(this.devicesModel);
	}
	
	public void setDataStore(Map<String, Object> datastore) {
        this.datastoreModel = new DataStoreTableModel(datastore);
        this.datastoreTable.setModel(this.datastoreModel);
	}
	
	public List<DeviceDAO> getDevices() {
		return this.devicesModel.getData();
	}
	
	public HashMap<String, Object> getDataStore() {
		return this.datastoreModel.getData();
	}
	
	private DeviceDAO getSelectedDevice() {
		int selectedIndex = this.devicesTable.getSelectedRow();
		if (selectedIndex != -1) {
    		return this.getDevices().get(selectedIndex);
    	}
		return null;
	}
	
	private void setActionsPanel(DeviceDAO device) {
		JPanel devicePanel = TestingPanelsMapper.createTestActionsPanel(device, this.ctrl);
		this.actionsPanel.removeAll();
		if (devicePanel != null) {
			this.actionsPanel.add(devicePanel);
		}
		this.actionsPanel.revalidate();
		this.actionsPanel.repaint();
	}
}
