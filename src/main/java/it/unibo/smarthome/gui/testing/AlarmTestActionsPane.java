package it.unibo.smarthome.gui.testing;

import java.awt.FlowLayout;

import javax.swing.JButton;

import it.unibo.smarthome.dao.DeviceDAO;
import it.unibo.smarthome.gui.IMainGuiController;

public class AlarmTestActionsPane extends TestActionsPane {

	private static final long serialVersionUID = -4752122476010675355L;

	public AlarmTestActionsPane(IMainGuiController ctrl, DeviceDAO device) {
		super(ctrl, device);
	}

	@Override
	protected void initUI() {
		
		this.setLayout(new FlowLayout());
		
		JButton btnToggleStateAlarm = new JButton("Toggle state alarm");
		btnToggleStateAlarm.addActionListener(ev -> this.requestAction("toggle-state-alarm", ev));
		
		this.add(btnToggleStateAlarm);
        
        this.validate();
	}

}
