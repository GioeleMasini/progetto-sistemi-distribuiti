package it.unibo.smarthome.gui.testing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import it.unibo.smarthome.utils.Pair;

public class DataStoreTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = -353519702637595796L;

	private final ArrayList<Pair<String, Object>> configsList;
     
    private final String[] columnNames = new String[] {
            "Name", "Value"
    };
    private final Class<?>[] columnClass = new Class<?>[] {
        String.class, String.class
    };

	private Map<String, Object> configsMap;
    
    public DataStoreTableModel()
    {
        this(new HashMap<String, Object>());
    }
 
    public DataStoreTableModel(Map<String, Object> configs)
    {
    	this.configsMap = configs;
    	ArrayList<Pair<String, Object>> configsList = new ArrayList<Pair<String, Object>>();
        configs.forEach((key, value) -> {
        	configsList.add(new Pair<String, Object>(key, value));
        });
        this.configsList = configsList;
    }
     
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
 
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
 
    @Override
    public int getRowCount()
    {
        return configsList.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
    	Pair<String, Object> config = configsList.get(rowIndex);
        if (0 == columnIndex) {
            return config.getFirst();
        }
        else if (1 == columnIndex) {
            return config.getSecond().toString();
        }
        return null;
    }
    
    /*@Override
    public void setValueAt(Object aValue, int rowIndex, int colIndex) {
    	Pair<String, String> room = configsList.get(rowIndex);
        switch (colIndex) {
        case 0:
        	room.setFirst((String) aValue);
    	    break;
        case 1:
    	    room.setSecond((String) aValue);
    	    break;
        }
    }*/

	@Override
	public boolean isCellEditable(int rowIndex, int colIndex) {
		return false;
	}

	/* public void removeRow(int rowIndex) {
		this.configsList.remove(rowIndex);
		this.fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public void addRow() {
		this.configsList.add(new RoomDAO());
		this.fireTableRowsInserted(this.configsList.size(), this.configsList.size());
	} */
	
	public HashMap<String, Object> getData() {
		return new HashMap<String, Object>(this.configsMap);
	}
}
