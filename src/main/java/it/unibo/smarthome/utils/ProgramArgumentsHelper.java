package it.unibo.smarthome.utils;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.devices.IDevice;

public class ProgramArgumentsHelper {

	private static final Logger log = LogManager.getRootLogger();
	private static final String PARAM_SIMULATED = "--simulated";
	private static final String PARAM_GUI = "--gui";
	private static final String PARAM_MAIN = "--main";
	private static final String PARAM_TC = "--tc";
	private static final String PARAM_DEVICE = "--device";
	
	private List<String> argsList;
	
	private boolean simulated;
	private boolean gui;
	private boolean main;
	private String tc;
	private Class<? extends IDevice> device;

	@SuppressWarnings("unchecked")
	public ProgramArgumentsHelper(String[] args) {
		this.argsList = Arrays.asList(args);
		
		if (this.argsList.size() == 0) {
			this.gui = true;
			this.main = true;
			this.simulated = true;
		} else {
			// Booleani
			this.simulated = argsList.contains(PARAM_SIMULATED);
			this.gui = argsList.contains(PARAM_GUI);
			this.main = argsList.contains(PARAM_MAIN);
			
			// Coppia --argument valore
			this.tc = this.getArgValue(PARAM_TC);
			
			String deviceClassName = this.getArgValue(PARAM_DEVICE);
			if (deviceClassName != null) {
				try {
					Class<?>deviceClass = Class.forName(deviceClassName);
					this.device = (Class<IDevice>) deviceClass;
				} catch (ClassNotFoundException | ClassCastException e) {
					log.error("Classe " + deviceClassName + " non trovata o non estende IDevice. " + e.getMessage());
				}
			}
		}
	}
	
	public boolean isSimulated() {
		return this.simulated;
	}
	
	public boolean isGui() {
		return this.gui;
	}
	
	public String getTC() {
		return this.tc;
	}
	
	public boolean isMain() {
		return this.main;
	}
	
	public Class<? extends IDevice> getDevice() {
		return this.device;
	}
	
	private String getArgValue(String argName) {
		int tcIndex = argsList.indexOf(argName); 
		if (tcIndex >= 0 && argsList.size() > tcIndex + 1 && argsList.get(tcIndex + 1) instanceof String) {
			return argsList.get(tcIndex + 1).toString();
		}
		return null;
	}
}
