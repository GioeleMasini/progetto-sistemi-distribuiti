package it.unibo.smarthome.utils;

import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.devices.IDevice;
import jade.wrapper.AgentContainer;

public class DeviceUtils {
	private static Logger log = LogManager.getLogger();
	
	public static IDevice createInstance(Class<? extends IDevice> deviceClass, String tname) {
		AgentContainer agentsContainer = JadeUtils.startPeripheralContainer();
		
		IDevice device = null;
		try {
			// Istanzio il device
			device = deviceClass
					.getConstructor(AgentContainer.class, String.class)
					.newInstance(agentsContainer, tname);
			
			// E lo avvio
			device.start();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			log.error("Errore durante l'istanziazione del device " + deviceClass + ": " + e.getMessage());
			e.printStackTrace();
		}
		
		return device;
	}
}
