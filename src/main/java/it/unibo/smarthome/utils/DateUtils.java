package it.unibo.smarthome.utils;

import java.util.Calendar;

public class DateUtils {

	public static String hourString(Calendar date) {
		return date.get(Calendar.HOUR_OF_DAY) + ":" + date.get(Calendar.MINUTE);
	}
	
	public static String dateString(Calendar date) {
		return date.get(Calendar.DAY_OF_MONTH) + "/" 
				+ (date.get(Calendar.MONTH) + 1) + "/"
				+ date.get(Calendar.YEAR);
	}
	
	public static String fullDateString(Calendar date) {
		return dateString(date) + " " + hourString(date);
	}

	public static Calendar getTomorrow(Calendar today) {
		Calendar tomorrow = ((Calendar) today.clone());
		tomorrow.add(Calendar.HOUR_OF_DAY, 24);
		return tomorrow;
	}
	
	public static Calendar getYesterday(Calendar today) {
		Calendar yesterday = ((Calendar) today.clone());
		yesterday.add(Calendar.HOUR_OF_DAY, -24);
		return yesterday;
	}
}
