package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class UniqueIdDAO extends BaseDAO {

	private static final long serialVersionUID = 9125825413877912156L;

	public static final String TEMPLATE = asTupleString("X", "Y");

	private String id;
	private String identifier;
	
	public UniqueIdDAO(String identifier) {
		this(null, identifier);
	}
	
	public UniqueIdDAO(String id, String identifier) {
		super();
		this.setId(id);
		this.setIdentifier(identifier);
	}

	public UniqueIdDAO(LogicTuple tupla) {
		super(tupla);
	}

	public UniqueIdDAO() {
		this((String) null);
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null);
	}
	
	public LogicTuple toTuple(String tId, String tIdentifier) throws InvalidLogicTupleException {
		String id = toProlog(this.getId(), tId);
		String name = toProlog(this.getIdentifier(), tIdentifier);
		
		return LogicTuple.parse(asTupleString(id, name));
	}
	
	public static String asTupleString(String id, String identifier) {
		return "nextUniqueId(" + identifier + "," + id + ")";
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.setId(TucsonDataUtils.stringValue(tuple.getArg(1)));
		this.setIdentifier(TucsonDataUtils.stringValue(tuple.getArg(0)));
	}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}

}
