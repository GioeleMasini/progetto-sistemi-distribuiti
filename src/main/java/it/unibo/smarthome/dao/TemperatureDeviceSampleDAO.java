package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class TemperatureDeviceSampleDAO  extends BaseDAO {

	private static final long serialVersionUID = -4979486048389411046L;

	public static final String TEMPLATE = asTupleString("S", "D", "R");
	
	private double sampleValue;
	private String roomID;
	private String deviceID;
	
	public TemperatureDeviceSampleDAO() {}
	
	public TemperatureDeviceSampleDAO(double sampleValue, String deviceID, String roomID) {
		this.sampleValue = sampleValue;
		this.deviceID = deviceID;
		this.roomID = roomID;
	}
	
	public TemperatureDeviceSampleDAO(LogicTuple tup){
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null, null);
	}
	
	public LogicTuple toTuple(String tSample, String tDevice, String tRoom) throws InvalidLogicTupleException {
		String value = toProlog(this.getSampleValue(), tSample);
		String device = toProlog(this.getDeviceID(), tDevice);
		String room = toProlog(this.getRoomID(), tRoom);
		
		return LogicTuple.parse(asTupleString(value, device, room));
	}
	
	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.sampleValue = tuple.getArg("value").getArg(0).doubleValue();
		this.deviceID = TucsonDataUtils.stringValue(tuple.getArg("device").getArg(0));
		this.roomID = TucsonDataUtils.stringValue(tuple.getArg("room").getArg(0));
	}
	
	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public static String asTupleString(String sample, String device, String room) {
		return "temperatureSample(value(" + sample + "),device(" + device + "),room(" + room + "))";
	}

	public double getSampleValue() {
		return sampleValue;
	}
	
	public String getRoomID() {
		return roomID;
	}

	public String getDeviceID() {
		return deviceID;
	}
}
