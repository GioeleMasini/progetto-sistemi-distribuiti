package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class TemperatureRoomUpdatedNotifyDAO extends BaseDAO {

	private static final long serialVersionUID = 846448468748622633L;

	public static final String TEMPLATE = asTupleString("D", "R");
	
	private String roomID;
	private String deviceID;
	
	public TemperatureRoomUpdatedNotifyDAO() {}
	
	public TemperatureRoomUpdatedNotifyDAO(String deviceID, String roomID) {
		this.deviceID = deviceID;
		this.roomID = roomID;
	}
	
	public TemperatureRoomUpdatedNotifyDAO(LogicTuple tup){
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null);
	}
	
	public LogicTuple toTuple(String tDevice, String tRoom) throws InvalidLogicTupleException {
		String device = toProlog(this.getDeviceID(), tDevice);
		String room = toProlog(this.getRoomID(), tRoom);
		
		return LogicTuple.parse(asTupleString(device, room));
	}
	
	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.deviceID = TucsonDataUtils.stringValue(tuple.getArg("device").getArg(0));
		this.roomID = TucsonDataUtils.stringValue(tuple.getArg("room").getArg(0));
	}
	
	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public static String asTupleString(String device, String room) {
		return "tempRoomUpdatedNotify(device(" + device + "),room(" + room + "))";
	}
	
	public String getRoomID() {
		return roomID;
	}

	public String getDeviceID() {
		return deviceID;
	}
}