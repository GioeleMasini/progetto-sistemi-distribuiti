package it.unibo.smarthome.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.devices.IDevice;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class DeviceDAO extends BaseDAO {

	private static final long serialVersionUID = 9125825413877912156L;
	private static Logger log = LogManager.getLogger();

	public static final String TEMPLATE = asTupleString("V", "W", "X", "Y", "Z");

	private String id;
	private String name;
	private Class<? extends IDevice> deviceClass;
	private String agent;
	private String roomId;
	
	public DeviceDAO(String id, String name, Class <? extends IDevice> deviceClass, String agent, String roomId) {
		super();
		this.id = id;
		this.name = name;
		this.setDeviceClass(deviceClass);
		this.agent = agent;
		this.roomId = roomId;
	}

	public DeviceDAO(LogicTuple tupla) {
		super(tupla);
	}

	public DeviceDAO() {
		this(null, null, null, null, null);
	}

	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null, null, null, null);
	}
	
	public LogicTuple toTuple(String tId, String tName, String tClassName, String tAgent, String tRoomId) throws InvalidLogicTupleException {
		String id = toProlog(this.getId(), tId);
		String name = toProlog(this.getName(), tName);
		Class<? extends IDevice> deviceClass = this.getDeviceClass();
		String className = toProlog(deviceClass == null ? null : deviceClass.getName(), tClassName);
		String agent = toProlog(this.getAgent(), tAgent);
		String roomId = toProlog(this.getRoomId(), tRoomId);
		
		return LogicTuple.parse(asTupleString(id, name, className, agent, roomId));
	}
	
	public static String asTupleString(String id, String name, String className, String agent, String roomId) {
		return "device(id(" + id + "),name(" + name + "),class(" + className + "),agent(" + agent + "),roomId(" + roomId + "))";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.id = TucsonDataUtils.stringValue(tuple.getArg("id").getArg(0));
		this.name = TucsonDataUtils.stringValue(tuple.getArg("name").getArg(0));
		this.agent = TucsonDataUtils.stringValue(tuple.getArg("agent").getArg(0));
		this.roomId = TucsonDataUtils.stringValue(tuple.getArg("roomId").getArg(0));
		
		String deviceClassName = TucsonDataUtils.stringValue(tuple.getArg("class").getArg(0));
		try {
			this.deviceClass = deviceClassName == null ? null : (Class<? extends IDevice>) Class.forName(deviceClassName);
		} catch (ClassNotFoundException | ClassCastException e) {
			log.error("Classe device " + deviceClassName + " non trovata: " + e.getMessage());
		}
	}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public Class<? extends IDevice> getDeviceClass() {
		return deviceClass;
	}

	public void setDeviceClass(Class<? extends IDevice> deviceClass) {
		this.deviceClass = deviceClass;
	}

}
