package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_ACTIVITY_STATE;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class AlarmChangeStateDAO extends BaseDAO {

	private static final long serialVersionUID = 4743417536851330277L;

	public static final String TEMPLATE = asTupleString("X", "Y");
	
	private ALARM_ACTIVITY_STATE state;
	private String pass;
	
	/**
	 * DAO per cambiare stato allarme ALARM_ACTIVITY_STATE
	 */
	public AlarmChangeStateDAO() {}
	
	/**
	 * DAO per cambiare stato allarme ALARM_ACTIVITY_STATE
	 * 
	 * @param state
	 * 				stato allarme
	 * @param pass
	 * 				password del device
	 */
	public AlarmChangeStateDAO(ALARM_ACTIVITY_STATE state, String pass) {
		this.state = state;
		this.pass = pass;
	}
	
	/**
	 * DAO per cambiare stato allarme ALARM_ACTIVITY_STATE
	 * 
	 * @param tup
	 * 				tupla di tipo TUPLE_CHANGE_ALARM__STATE_TEMPLATE
	 */
	public AlarmChangeStateDAO(LogicTuple tup){
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null);
	}
	
	public LogicTuple toTuple(String tState, String tPass) throws InvalidLogicTupleException {
		String state = toProlog(this.getState().getValue(), tState);
		String pass = toProlog(this.getPassword(), tPass);
		
		return LogicTuple.parse(asTupleString(state, pass));
	}
	
	public static String asTupleString(String state, String pass) {
		return "alarmChangeState(alarmState(" + state + "),pass(" + pass + "))";
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {
		String stato = TucsonDataUtils.stringValue(tuple.getArg("alarmState").getArg(0));
		this.state = ALARM_ACTIVITY_STATE.byValue(stato);
		this.pass = TucsonDataUtils.stringValue(tuple.getArg("pass").getArg(0));
	}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public ALARM_ACTIVITY_STATE getState() {
		return this.state;
	}

	public String getPassword() {
		return this.pass;
	}
}
