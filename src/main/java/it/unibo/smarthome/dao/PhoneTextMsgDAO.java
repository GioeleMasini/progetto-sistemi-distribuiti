package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class PhoneTextMsgDAO extends BaseDAO {

	private static final long serialVersionUID = 1308066115544949453L;
	
	public static final String TEMPLATE = asTupleString("X", "Y");
	
	private String number;
	private String msg;
	
	/**
	 * DAO per creazione messaggi di testo semplici
	 */
	public PhoneTextMsgDAO(){}
	
	/**
	 * DAO per creazione messaggi di testo semplici
	 * 
	 * @param num
	 * 				numero di telefono al quale recapitare il messaggio
	 * @param msg
	 * 				testo contenuto nel messaggio
	 */
	public PhoneTextMsgDAO(String num, String msg){
		this.number = num;
		this.msg = msg;
	}
	
	/**
	 * DAO per creazione messaggi di testo semplici
	 * 
	 * @param tup
	 * 				tupla di tipo TUPLE_PHONE_SMS_TEMPLATE
	 */
	public PhoneTextMsgDAO(LogicTuple tup){
		super(tup);
	}

	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null);
	}
	
	public LogicTuple toTuple(String tNumber, String tMsg) throws InvalidLogicTupleException {
		String number = toProlog(this.getNumber(), tNumber);
		String msg = toProlog(this.getMsg(), tMsg);
		
		return LogicTuple.parse(asTupleString(number, msg));
	}
	
	public static String asTupleString(String number, String msg) {
		return "phoneTextMsg(num(" + number + "),msg(" + msg + "))";
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.number = TucsonDataUtils.stringValue(tuple.getArg("num").getArg(0));
		this.msg = TucsonDataUtils.stringValue(tuple.getArg("msg").getArg(0));
	}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public String getNumber() {
		return this.number;
	}

	public String getMsg() {
		return this.msg;
	}
}
