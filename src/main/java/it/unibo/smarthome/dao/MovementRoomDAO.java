package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class MovementRoomDAO extends BaseDAO {

	private static final long serialVersionUID = -8719695375544425686L;

	public static final String TEMPLATE = asTupleString("X", "Y");
	
	private String device;
	private String roomID;
	
	public MovementRoomDAO() {}
	
	public MovementRoomDAO(String device, String roomID) {
		this.device = device;
		this.roomID = roomID;
	}
	
	public MovementRoomDAO(LogicTuple tup) {
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null);
	}
	
	public LogicTuple toTuple(String tDevice, String tRoom) throws InvalidLogicTupleException {
		String value = toProlog(this.getDevice(), tDevice);
		String room = toProlog(this.getRoomID(), tRoom);
		
		return LogicTuple.parse(asTupleString(value, room));
	}
	
	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.device = TucsonDataUtils.stringValue(tuple.getArg("device").getArg(0));
		this.roomID = TucsonDataUtils.stringValue(tuple.getArg("room").getArg(0));
	}
	
	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public static String asTupleString(String device, String room) {
		return "movementRoom(device(" + device + "),room(" + room + "))";
	}

	public String getDevice() {
		return this.device;
	}
	
	public String getRoomID() {
		return this.roomID;
	}
}
