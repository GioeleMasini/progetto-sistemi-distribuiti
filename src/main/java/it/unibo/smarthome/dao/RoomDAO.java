package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class RoomDAO extends BaseDAO {

	private static final long serialVersionUID = 4399888088961343708L;

	public static final String TEMPLATE = asTupleString("X", "Y", "Z");

	private String id;
	private String name;
	private int floor;
	
	public RoomDAO(String roomName) {
		this(null, roomName, 0);
	}
	
	public RoomDAO(String id, String name, int floor) {
		super();
		this.setId(id);
		this.name = name;
		this.floor = floor;
	}

	public RoomDAO(LogicTuple tupla) {
		super(tupla);
	}

	public RoomDAO() {
		this((String) null);
	}

	public String getName() {
		return name;
	}

	public int getFloor() {
		return floor;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null, null);
	}
	
	public LogicTuple toTuple(String tId, String tName, String tFloor) throws InvalidLogicTupleException {
		String id = toProlog(this.getId(), tId);
		String name = toProlog(this.getName(), tName);
		String floor = toProlog(this.getFloor(), tFloor);
		
		return LogicTuple.parse(asTupleString(id, name, floor));
	}
	
	public static String asTupleString(String id, String name, String floor) {
		return "room(id(" + id + "),name(" + name + "),floor(" + floor + "))";
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.setId(TucsonDataUtils.stringValue(tuple.getArg("id").getArg(0)));
		this.name = TucsonDataUtils.stringValue(tuple.getArg("name").getArg(0));
		this.floor = tuple.getArg("floor").getArg(0).intValue();
	}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}

}
