package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class TemperatureRoomDAO extends BaseDAO {

	private static final long serialVersionUID = -4979486048389411047L;

	public static final String TEMPLATE = asTupleString("X", "Y");
	
	private Double value;
	private String roomID;
	
	public TemperatureRoomDAO() {}
	
	public TemperatureRoomDAO(Double value, String roomID) {
		this.value = value;
		this.roomID = roomID;
	}
	
	public TemperatureRoomDAO(LogicTuple tup) {
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null, null);
	}
	
	public LogicTuple toTuple(String tSample, String tRoom) throws InvalidLogicTupleException {
		String value = toProlog(this.getValue(), tSample);
		String room = toProlog(this.getRoomID(), tRoom);
		
		return LogicTuple.parse(asTupleString(value, room));
	}
	
	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.value = TucsonDataUtils.doubleValue(tuple.getArg("value").getArg(0));
		this.roomID = TucsonDataUtils.stringValue(tuple.getArg("room").getArg(0));
	}
	
	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public static String asTupleString(String value, String room) {
		return "temperatureRoom(value(" + value + "),room(" + room + "))";
	}

	public Double getValue() {
		return this.value;
	}
	
	public String getRoomID() {
		return this.roomID;
	}
}
