package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;

public class AlarmHomeActiveDAO extends BaseDAO {

	private static final long serialVersionUID = -2329657853473109048L;
	public static final String TEMPLATE = asTupleString();

	public AlarmHomeActiveDAO() {}

	public AlarmHomeActiveDAO(LogicTuple tup){
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return LogicTuple.parse(asTupleString());
	}
	
	public static String asTupleString() {
		return "homeAlarmActiveState";
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
}
