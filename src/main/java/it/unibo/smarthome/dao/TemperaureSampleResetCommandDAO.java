package it.unibo.smarthome.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import it.unibo.smarthome.utils.TucsonDataUtils;

public class TemperaureSampleResetCommandDAO extends BaseDAO {

	private static final long serialVersionUID = -7107765418527527528L;

	public static final String TEMPLATE = asTupleString("R");
	
	private String roomID;
	
	public TemperaureSampleResetCommandDAO() {}
	
	public TemperaureSampleResetCommandDAO(String roomID) {
		this.roomID = roomID;
	}
	
	public TemperaureSampleResetCommandDAO(LogicTuple tup){
		super(tup);
	}
	
	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return toTuple(null);
	}
	
	public LogicTuple toTuple(String tRoom) throws InvalidLogicTupleException {
		String room = toProlog(this.getRoomID(), tRoom);
		
		return LogicTuple.parse(asTupleString(room));
	}
	
	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.roomID = TucsonDataUtils.stringValue(tuple.getArg("room").getArg(0));
	}
	
	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	public static String asTupleString(String room) {
		return "tempSampleResetCommand(room(" + room + "))";
	}
	
	public String getRoomID() {
		return roomID;
	}
}
