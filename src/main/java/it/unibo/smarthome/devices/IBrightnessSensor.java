package it.unibo.smarthome.devices;

public interface IBrightnessSensor extends IDevice {
	Double readBrightness();
}
