package it.unibo.smarthome.devices;

public interface IThermometer extends IDevice {

	public void updateTemperature(double temp);
}
