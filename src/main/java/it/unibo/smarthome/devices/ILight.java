package it.unibo.smarthome.devices;

import it.unibo.smarthome.constants.LightConstants.LIGHT_ACTIVATION;

public interface ILight extends IDevice {

	public void turnOn();
	public void turnOff();
	public boolean isOn();
	public boolean isAutomatic();
	public void setAutomaticMode();
	public void setManualMode();
	public void setActivationType(LIGHT_ACTIVATION activationType);
}
