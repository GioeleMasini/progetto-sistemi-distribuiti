package it.unibo.smarthome.devices;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.DeviceAgent;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.devices.events.IDeviceEventListener;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public abstract class BaseDevice implements IDevice {

	private static final Logger log = LogManager.getLogger();
	public static final String DEVICE_ID_KEY = "deviceId";
	public static final String ROOM_ID_KEY = "roomId";
	public static final String SIMULATED_KEY = "isSimulated";
	private static final String DEVICE_NAME_KEY = "name";
	public static final String EVENT_ROOMID_UPDATED = "roomid-updated";
	
	private AgentController deviceAgentCtrl;
	private final AgentContainer agentsContainer;
	private final String tname;
	
	private ConcurrentHashMap<String, Object> dataStore;
	private ConcurrentHashMap<String, String> configs;
	private List<IDeviceEventListener> listeners;

	public BaseDevice(AgentContainer agentsContainer, String tname) {
		this.agentsContainer = agentsContainer;
		this.tname = tname;

		this.listeners = new ArrayList<>();
		this.dataStore = new ConcurrentHashMap<>();
		this.configs = new ConcurrentHashMap<>();

		this.setName(this.getClass().getSimpleName());
		this.setRoomId("1");
	}
	
	public AgentContainer getAgentsContainer() {
		return agentsContainer;
	}

	public String getTname() {
		return tname;
	}

	@Override
	public boolean start() {
		boolean ret = true;
		
		try {
			// Avvio l'agente che gestisce il device
			this.deviceAgentCtrl = JadeUtils.createNewAgent(
					this.getAgentsContainer(),
					"deviceAgent",
					DeviceAgent.class,
					new Object[] { this.getTname(), this }
			);
			this.deviceAgentCtrl.start();
		} catch (StaleProxyException e) {
			log.error("Errore durante creazione agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}
	
	public abstract void setupAgent();
	
	public void notifyBaseDeviceSetupCompleted() {
		setupAgent();
		log.info(this.getName() + " - Setup dispositivo completato. Id: " + this.getId());
	}

	@Override
	public boolean dispose() {
		boolean ret = true;
		try {
			this.deviceAgentCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore nella terminazione del deviceAgent: " + e.getMessage());
			ret = false;
		}
		return ret;
	}

	@Override
	public synchronized void addListener(IDeviceEventListener listener) {
		this.listeners.add(listener);
	}
	
	public synchronized void removeListener(IDeviceEventListener listener) {
		this.listeners.remove(listener);
	}
	
	protected void notifyListeners(DeviceEvent e) {
		this.listeners.forEach(listener -> listener.onNotify(e)); 
	}

	@Override
	public String getId() {
		return this.dataStore.get(DEVICE_ID_KEY).toString();
	}

	@Override
	public void setId(String id) {
		this.dataStore.put(DEVICE_ID_KEY, id);
	}

	public String getRoomId() {
		return this.dataStore.get(ROOM_ID_KEY).toString();
	}

	public String getName() {
		return Optional.ofNullable(this.dataStore.get(DEVICE_NAME_KEY)).orElse("unkonwn").toString();
	}

	public void setRoomId(String roomId) {
		if (roomId != null && !roomId.equals(this.dataStore.get(ROOM_ID_KEY))) {
			this.dataStore.put(ROOM_ID_KEY, roomId);
			this.notifyListeners(new DeviceEvent(this, EVENT_ROOMID_UPDATED));
			log.info(this.getName() + " - Stanza aggiornata. Id: " + roomId);
		}
	}

	public void setName(String deviceName) {
		Object oldName = this.dataStore.put(DEVICE_NAME_KEY, deviceName);
		log.info("Cambio nome dispositivo: " + oldName + " -> " + deviceName);
	}
	
	public ConcurrentHashMap<String, Object> getDataStore() {
		return this.dataStore;
	}
	
	public ConcurrentHashMap<String, String> getConfigs() {
		return this.configs;
	}

	public abstract void doTestAction(String action);
}
