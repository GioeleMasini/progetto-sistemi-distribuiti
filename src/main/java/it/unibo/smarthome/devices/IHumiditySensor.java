package it.unibo.smarthome.devices;

public interface IHumiditySensor extends IDevice {
	Double readHumidity();
}
