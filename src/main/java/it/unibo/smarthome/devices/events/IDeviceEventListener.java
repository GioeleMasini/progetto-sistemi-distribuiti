package it.unibo.smarthome.devices.events;

import java.util.EventListener;

public interface IDeviceEventListener extends EventListener {
	public void onNotify(DeviceEvent e);
}
