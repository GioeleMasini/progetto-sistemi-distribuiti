package it.unibo.smarthome.devices.events;

import it.unibo.smarthome.devices.IDevice;

public class DeviceEvent {
	private final IDevice source;
	private final String eventName;
	
	public DeviceEvent(IDevice source, String eventName) {
		super();
		this.source = source;
		this.eventName = eventName;
	}

	public IDevice getSource() {
		return source;
	}

	public String getName() {
		return eventName;
	}
}
