package it.unibo.smarthome.devices;

public interface ITemperatureSensor extends IDevice {
	
	Double readTemperature();
	double getRangeUpdateChange();
}
