package it.unibo.smarthome.devices;

public interface IMotionSensor extends IDevice {
	public boolean isThereMovement();
	public void setMovement(boolean movement);
}
