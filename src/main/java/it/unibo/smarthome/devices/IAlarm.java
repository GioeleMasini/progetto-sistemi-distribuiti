package it.unibo.smarthome.devices;

import it.unibo.smarthome.constants.AlarmConstants.ALARM_ACTIVITY_STATE;
import it.unibo.smarthome.exception.AuthenticationFailException;

public interface IAlarm extends IDevice {

	public void enableAlarm();
	public void disableAlarm(String pw) throws AuthenticationFailException;
	public boolean isAlarmEnable();
	public String getPassword();
	public void changePassword(String oldPw, String newPw) throws AuthenticationFailException;
	public void setState(ALARM_ACTIVITY_STATE act, String pw) throws AuthenticationFailException;	
	public boolean isActive();
	public String getEmergencyNumber();
	public String getEmergencyText();
	public long getRingingTime();
}
