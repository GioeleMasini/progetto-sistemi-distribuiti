package it.unibo.smarthome.devices.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.MotionSensorAgent;
import it.unibo.smarthome.constants.MovementSensorConstants.MOVEMENT_SENSOR_DATA_STORE_KEYS;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IMotionSensor;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class MotionSensor extends BaseDevice implements IMotionSensor {

	private static Logger log = LogManager.getLogger();
	private AgentController sensorAgentCtrl;
	
	public MotionSensor(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
		this.initDataStore();
	}

	@Override
	public boolean isThereMovement() {
		return (boolean) this.getDataStore().get(MOVEMENT_SENSOR_DATA_STORE_KEYS.THERE_IS_MOVEMENT.getValue());
	}

	@Override
	public void setMovement(boolean movement) {
		log.info(this.getName() + " - Cambio stato movimento: " + movement);
		this.getDataStore().put(MOVEMENT_SENSOR_DATA_STORE_KEYS.THERE_IS_MOVEMENT.getValue(), movement);
		String eventName = movement ? "movement-true" : "movement-false";
		this.notifyListeners(new DeviceEvent(this, eventName));
	}

	@Override
	public void setupAgent() {		
		try {
			// Avvio l'agente che gestisce il device
			sensorAgentCtrl = JadeUtils.createNewAgent(
					this.getAgentsContainer(),
					"motionSensorAgent",
					MotionSensorAgent.class,
					new Object[] { this.getTname(), this }
					);
			
			sensorAgentCtrl.start();
		} catch (StaleProxyException e) {
			log.error("Errore durante creazione agenti: " + e.getMessage());
		}
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			sensorAgentCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}
	
	/**
	 * Inizializza e setta i valori del data store
	 */	
	private void initDataStore() {
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(MOVEMENT_SENSOR_DATA_STORE_KEYS.THERE_IS_MOVEMENT.getValue(), false);
	}

	@Override
	public void updateConfigs() {		
	}

	@Override
	public void doTestAction(String action) {
		log.info("doTestAction, Test avviato, action: " + action);
		switch(action) {
		case "toggle-movement":
			this.setMovement(!this.isThereMovement());
			break;
		}
	}

}
