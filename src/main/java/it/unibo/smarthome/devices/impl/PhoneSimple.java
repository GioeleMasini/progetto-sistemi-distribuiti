package it.unibo.smarthome.devices.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.PhoneAgent;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IPhone;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class PhoneSimple extends BaseDevice implements IPhone {

	private static Logger log = LogManager.getLogger();
	private AgentController phoneAgentCtrl;
	
	public PhoneSimple(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
	}	

	@Override
	public void setupAgent() {
		try {
			setUp();
		} catch (StaleProxyException e) {
			log.error("Errore creazione agente: " + e.getMessage());
		}
	}
	
	private void setUp() throws StaleProxyException {
		phoneAgentCtrl = JadeUtils.createNewAgent(
				this.getAgentsContainer(), 
				"phoneAgent", 
				PhoneAgent.class, 
				new Object[] { this.getTname(), this });
		phoneAgentCtrl.start();
	}
	
	@Override
	public boolean call(String number) {
		// TODO Implementare funzionalità chiamata(?)
		return false;
	}

	@Override
	public void sendSMS(String number, String text) {
		log.info(this.getName() + " - Richiesto invio messaggio al numero: " + number
				+ ", testo inviato: " + text);
	}

	@Override
	public void sendMMS(String number, Byte[] foto) {
		// TODO Implementare funzionalità invio MMS
	}

	@Override
	public boolean checkSignal() {		
		return true;
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			phoneAgentCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}

	@Override
	public void updateConfigs() {
		// Per futuri parametri di configurazione
		
	}

	@Override
	public void doTestAction(String action) {
		// Per futuri test su ginetto
		
	}

}
