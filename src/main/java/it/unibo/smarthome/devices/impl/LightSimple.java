package it.unibo.smarthome.devices.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import it.unibo.smarthome.agents.impl.LightAgent;
import it.unibo.smarthome.constants.LightConstants.*;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.ILight;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class LightSimple extends BaseDevice implements ILight {

	private static Logger log = LogManager.getLogger();
	private AgentController lightAgentCtrl;
	
	public LightSimple(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
		this.init();
	}
	
	private void init() {
		this.initConfigs();
		this.initDataStore();
	}

	@Override
	public void setupAgent() {
		try {
			setUp();
		} catch (StaleProxyException e) {
			log.error("Errore creazione agente: " + e.getMessage());
		}
	}
	
	private void setUp() throws StaleProxyException {
		this.lightAgentCtrl = JadeUtils.createNewAgent(
				this.getAgentsContainer(), 
				"lightAgent", 
				LightAgent.class, 
				new Object[] { this.getTname(), this });
		this.lightAgentCtrl.start();
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			this.lightAgentCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}

	@Override
	public void turnOn() {
		this.getDataStore().put(LIGHT_DATA_STORE_KEYS.LIGHT_STATE.getValue(), LIGHT_STATE.ON);
		log.info(this.getName() + " - accensione luce.");
	}

	@Override
	public void turnOff() {
		this.getDataStore().put(LIGHT_DATA_STORE_KEYS.LIGHT_STATE.getValue(), LIGHT_STATE.OFF);
		log.info(this.getName() + " - spegnimento luce.");
	}

	@Override
	public boolean isOn() {
		return LIGHT_STATE.ON.equals(this.getDataStore().get(LIGHT_DATA_STORE_KEYS.LIGHT_STATE.getValue()));
	}
	
	@Override
	public boolean isAutomatic() {
		return this.getConfigs().get(LIGHT_CONFIG_KEYS.AUTOMATIC_ACTIVATION.getValue()).equals("true");
	}

	@Override
	public void setActivationType(LIGHT_ACTIVATION activationType) {
		this.getConfigs().put(LIGHT_CONFIG_KEYS.AUTOMATIC_ACTIVATION.getValue(), activationType.getValue());
		log.info(this.getName() + " - Modalità attivazione luce: " + activationType.getValue() + ".");
	}
	
	@Override
	public void setAutomaticMode() {
		this.getConfigs().put(LIGHT_CONFIG_KEYS.AUTOMATIC_ACTIVATION.getValue(), "true");
		this.notifyListeners(new DeviceEvent(this, "automatic-mode"));
		log.info(this.getName() + " - Modalità attivazione luce: automatica.");
	}
	
	@Override
	public void setManualMode() {
		this.getConfigs().put(LIGHT_CONFIG_KEYS.AUTOMATIC_ACTIVATION.getValue(), "false");
		this.notifyListeners(new DeviceEvent(this, "manual-mode"));
		log.info(this.getName() + " - Modalità attivazione luce: manuale.");
	}
	
	/**
	 * Inizializza e setta le config di base, che saranno disponibili
	 * in una tab specifica nella GUI
	 */
	private void initConfigs() {
		ConcurrentHashMap<String, String> configMap = this.getConfigs();
		configMap.put(LIGHT_CONFIG_KEYS.AUTOMATIC_ACTIVATION.getValue(), "false");
	}
	
	/**
	 * Inizializza e setta i valori del data store
	 */
	private void initDataStore() {
		this.turnOff();
	}
	
	@Override
	public void updateConfigs() {
		ConcurrentHashMap<String, String> configMap = this.getConfigs();
		configMap.forEach((key, value) -> {
			if (LIGHT_CONFIG_KEYS.AUTOMATIC_ACTIVATION.getValue().equals(key)) {
				if (!this.isAutomatic()) {
					this.notifyListeners(new DeviceEvent(this, "manual-mode"));
					log.info(this.getName() + " - Modalità attivazione luce: manuale.");
				} else if (this.isAutomatic()){
					this.notifyListeners(new DeviceEvent(this, "automatic-mode"));
					log.info(this.getName() + " - Modalità attivazione luce: automatica.");
				}
			}
		});
	}

	@Override
	public void doTestAction(String action) {
		switch(action) {
		case "toggle-light":
			if (this.isAutomatic()) {
				log.info(this.getName() + " - Modalità attivazione luce: automatica. Impossibile accendere/spegnere la luce manualmente.");
				return;
			}
			if (this.isOn()) {
				this.turnOff();
			} else {
				this.turnOn();
			}
			break;
		case "toggle-automatic-mode":
			if (this.isAutomatic()) {
				this.setManualMode();
			} else {
				this.setAutomaticMode();
			}
			break;
		}
	}

}
