package it.unibo.smarthome.devices.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.ThermometerSimpleAgent;
import it.unibo.smarthome.constants.ThermometerConstants.THERMOMETER_DATA_STORE_KEYS;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IThermometer;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class ThermometerSimple extends BaseDevice implements IThermometer {

	private static Logger log = LogManager.getLogger();
	private Double actualTemp;
	private AgentController thermometerCtrl;
	
	public ThermometerSimple(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
		this.actualTemp = null;
		this.initDataStore();
	}

	@Override
	public void setupAgent() {
		try {
			// Avvio l'agente che gestisce il device
			thermometerCtrl = JadeUtils.createNewAgent(
					this.getAgentsContainer(),
					"thermometerSimpleAgent",
					ThermometerSimpleAgent.class,
					new Object[] { this.getTname(), this }
					);
			thermometerCtrl.start();
		} catch (StaleProxyException e) {
			log.error("Errore durante creazione agenti: " + e.getMessage());
		}
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			thermometerCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}

	@Override
	public void updateTemperature(double temp) {
		//this.actualTemp = Math.round(temp * 100) / 100.0;
		this.actualTemp = temp;
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(THERMOMETER_DATA_STORE_KEYS.ACTUAL_TEMPERATURE.toString(), this.actualTemp);
		log.info(this.getName() + " - Nuova temperatura notificata: " + this.actualTemp + "°");
	}
	
	/**
	 * Inizializza e setta i valori del data store
	 */	
	private void initDataStore() {
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(THERMOMETER_DATA_STORE_KEYS.ACTUAL_TEMPERATURE.toString(), "None");
	}

	@Override
	public void updateConfigs() {		
	}

	@Override
	public void doTestAction(String action) {		
	}

}