package it.unibo.smarthome.devices.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.TemperatureSensorAgent;
import it.unibo.smarthome.constants.TemperatureConstants;
import it.unibo.smarthome.constants.TemperatureConstants.SENSOR_TEMP_CONFIG_KEYS;
import it.unibo.smarthome.constants.ThermometerConstants.THERMOMETER_DATA_STORE_KEYS;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.ITemperatureSensor;
import it.unibo.smarthome.simulation.Environment;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class TemperatureSensor extends BaseDevice implements ITemperatureSensor {

	private static Logger log = LogManager.getLogger();
	private final Environment env;
	private AgentController sensorAgentCtrl;
	private double tempRangeUpdate;

	public TemperatureSensor(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
		this.env = Environment.getInstance();
		this.tempRangeUpdate = TemperatureConstants.TEMP_RANGE_UPDATE_DEFAULT;
		this.initConfigs();
	}
	
	private void initConfigs() {
		ConcurrentHashMap<String, String> configMap = this.getConfigs();
		configMap.put(SENSOR_TEMP_CONFIG_KEYS.TEMP_RANGE_UPDATE_CHANGE.getValue(), Double.toString(this.tempRangeUpdate));
	}

	public synchronized Double readTemperature() {
		// leggo la temperatura dalla variabile globale Environment
		double temp = Math.round(this.env.getTemperature() * 100) / 100.0;
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(THERMOMETER_DATA_STORE_KEYS.ACTUAL_TEMPERATURE.toString(), temp);
		return temp;
	}
	
	@Override
	public double getRangeUpdateChange() {
		return this.tempRangeUpdate;
	}

	@Override
	public void setupAgent() {
		try {
			// Avvio l'agente che gestisce il device
			sensorAgentCtrl = JadeUtils.createNewAgent(
					this.getAgentsContainer(),
					"temperatureSensorAgent",
					TemperatureSensorAgent.class,
					new Object[] { this.getTname(), this }
					);
			sensorAgentCtrl.start();
		} catch (StaleProxyException e) {
			log.error("Errore durante creazione agenti: " + e.getMessage());
		}
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			sensorAgentCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}

	@Override
	public void updateConfigs() {
		ConcurrentHashMap<String, String> configMap = this.getConfigs();
		configMap.forEach((key, value) -> {
			if (SENSOR_TEMP_CONFIG_KEYS.TEMP_RANGE_UPDATE_CHANGE.getValue().equals(key)) {
				try {
					this.tempRangeUpdate = Double.parseDouble(value);
					log.debug("aggiornato valore di tempRangeUpdate: " + this.tempRangeUpdate);
				} catch (NumberFormatException nume) {
					log.error("Conversione nuovo valore di tempRangeUpdate fallita: " + nume.getMessage());
				}
			}
		});
	}

	@Override
	public void doTestAction(String action) {		
	}
}
