package it.unibo.smarthome.devices.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.AlarmAgent;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_ACTIVITY_STATE;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_CONFIG_KEYS;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_DATA_STORE_KEYS;
import it.unibo.smarthome.constants.AlarmConstants.ALARM_MODE_STATE;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IAlarm;
import it.unibo.smarthome.devices.events.DeviceEvent;
import it.unibo.smarthome.exception.AuthenticationFailException;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class AlarmSimple extends BaseDevice implements IAlarm {
	
	/*
	 * start e dispose rappresentano lo spegnimento e l'accensione del dispositivo hardware
	 * state rappresenta lo stato dell'allarme mentre è acceso quindi se è attivo sui sensori oppure se è acceso ma 
	 * in allerta di intrusioni alarmMode ci dice se siamo in uno stato di allarme e quindi se sirene o led
	 * presenti sul nostro hw sono attive
	 * 
	 */
	private static Logger log = LogManager.getLogger();
	private AgentController alarmAgentCtrl;
	private ALARM_ACTIVITY_STATE state;
	private String password;
	private String ringingTime = "10000";
	
	public AlarmSimple(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
		this.state = ALARM_ACTIVITY_STATE.OFF;
		this.password = "DEFAULT_PASS";
		this.init();
	}
	
	private void init() {
		this.initConfigs();
		this.initDataStore();
	}

	@Override
	public void setupAgent() {
		try {
			setUp();
		} catch (StaleProxyException e) {
			log.error("Errore creazione agente: " + e.getMessage());
		}
	}
	
	private void setUp() throws StaleProxyException {
		alarmAgentCtrl = JadeUtils.createNewAgent(
				this.getAgentsContainer(), 
				"alarmAgent", 
				AlarmAgent.class, 
				new Object[] { this.getTname(), this }
				);
		alarmAgentCtrl.start();
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			alarmAgentCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}

	@Override
	public void setState(ALARM_ACTIVITY_STATE act, String pw) throws AuthenticationFailException {
		if(this.checkPassword(pw)) {
			if(this.state != act) {
				this.state = act;
				this.getDataStore().put(ALARM_DATA_STORE_KEYS.ALARM_STATE.toString(), act);
				log.info(this.getName() + " - Cambio stato, impostato a: " + state.getValue());
			}
			
			// Quando spengo l'allarme lo imposto anche su disattivo
			if (ALARM_ACTIVITY_STATE.OFF.getValue().equals(this.state.getValue())) {
				this.getDataStore().put(ALARM_DATA_STORE_KEYS.ALARM_MODE.toString(), ALARM_MODE_STATE.DISABLE);
			}
		}
	}

	@Override
	public boolean isActive() {		
		return this.getDataStore().get(ALARM_DATA_STORE_KEYS.ALARM_STATE.toString()).equals(ALARM_ACTIVITY_STATE.ON);
	}

	@Override
	public void enableAlarm() {		
		this.getDataStore().put(ALARM_DATA_STORE_KEYS.ALARM_MODE.toString(), ALARM_MODE_STATE.ENABLE);
		log.info(this.getName() + " - Sistema di allarme attivato. Sto facendo lampeggiare la sirena e faccio un gran casino dal segnalatore acustico.");
	}

	@Override
	public void disableAlarm(String pw) throws AuthenticationFailException {
		if(checkPassword(pw)) {
			this.getDataStore().put(ALARM_DATA_STORE_KEYS.ALARM_MODE.toString(), ALARM_MODE_STATE.DISABLE);
		}
	}
	
	@Override
	public boolean isAlarmEnable() {		
		return this.getDataStore().get(ALARM_DATA_STORE_KEYS.ALARM_MODE.toString()).equals(ALARM_MODE_STATE.ENABLE);
	}
	
	@Override
	public String getPassword() {
//		return this.getConfigs().get(ALARM_CONFIG_KEYS.PASSWORD.getValue());
		return this.password;
	}

	@Override
	public void changePassword(String oldPw, String newPw) throws AuthenticationFailException {
		if(checkPassword(oldPw)) {
			this.password = newPw;
			this.getConfigs().put(ALARM_CONFIG_KEYS.PASSWORD.toString(), newPw);
		}
	}
	
	private boolean checkPassword(String pw) throws AuthenticationFailException {
		if(this.password.equals(pw)) {
			return true;
		} else {
			throw new AuthenticationFailException();
		}
	}
	
	/**
	 * Inizializza e setta le config di base, che saranno disponibili
	 * in una tab specifica nella GUI
	 */
	private void initConfigs() {
		ConcurrentHashMap<String, String> configMap = this.getConfigs();
//		configMap.put(ALARM_CONFIG_KEYS.PASSWORD.toString(), "DEFAULT_PASS");
		configMap.put(ALARM_CONFIG_KEYS.RINGING_TIME.toString(), this.ringingTime);
		configMap.put(ALARM_CONFIG_KEYS.EMERGENCY_NUMBER.toString(), "112");
		configMap.put(ALARM_CONFIG_KEYS.EMERGENCY_TEXT.toString(), "Allarme attivato");
	}
	
	/**
	 * Inizializza e setta i valori del data store
	 */	
	private void initDataStore() {
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(ALARM_DATA_STORE_KEYS.ALARM_STATE.toString(), ALARM_ACTIVITY_STATE.OFF);
		dataStoreMap.put(ALARM_DATA_STORE_KEYS.ALARM_MODE.toString(), ALARM_MODE_STATE.DISABLE);
	}

	@Override
	public void updateConfigs() {
		// TODO this.password può avere un senso solo per avere lo storico
		// della password precedente, se quella nuova è errata?
		ConcurrentHashMap<String, String> configMap = this.getConfigs();
		configMap.forEach((key, value) -> {
			if (ALARM_CONFIG_KEYS.PASSWORD.toString().equals(key)) {
				// TODO forse da usare il metodo changePassword del device?
				if (!this.password.equals(value)) {
					this.password = value;
					log.info(this.getName() + " - Password allarme aggiornata.");
				}
			}
			if (ALARM_CONFIG_KEYS.RINGING_TIME.getValue().equals(key)) {
				// Controllo che non sia attivo il dispositivo, in quel caso
				// annullo la modifica del tempo di ringing.
				if (this.isActive()) {
					log.info(this.getName() + " - Cambio tempo di suono dell'allarme possibile solo quando il dispositivo è spento.");
					this.getConfigs().put(ALARM_CONFIG_KEYS.RINGING_TIME.getValue(), this.ringingTime);
				} else {
					this.ringingTime = value;
					this.notifyListeners(new DeviceEvent(this, "ringing-time-updated"));
				}
			}
		});
	}

	@Override
	public void doTestAction(String action) {
		if (action.equals("toggle-state-alarm") && this.isAlarmEnable()) {
			log.info(this.getName() + " - AlarmSimple test: Impossibile modificare lo stato dell'allarme quando questo sta suonando.");
			return;
		}
		
		log.info("doTestAction, Test avviato, action: " + action);
		this.notifyListeners(new DeviceEvent(this, action));
	}

	@Override
	public String getEmergencyNumber() {
		return this.getConfigs().get(ALARM_CONFIG_KEYS.EMERGENCY_NUMBER.toString());
	}

	@Override
	public String getEmergencyText() {
		return this.getConfigs().get(ALARM_CONFIG_KEYS.EMERGENCY_TEXT.toString());
	}

	@Override
	public long getRingingTime() {
		return Long.valueOf(this.getConfigs().get(ALARM_CONFIG_KEYS.RINGING_TIME.toString()));
	}
}
