package it.unibo.smarthome.devices.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.smarthome.agents.impl.SendAlarmAgent;
import it.unibo.smarthome.agents.impl.WindowAlarmStateAgent;
import it.unibo.smarthome.constants.WindowConstants.ALARM_MODE_STATE;
import it.unibo.smarthome.constants.WindowConstants.WINDOW_DATA_STORE_KEYS;
import it.unibo.smarthome.constants.WindowConstants.WINDOW_STATE;
import it.unibo.smarthome.devices.BaseDevice;
import it.unibo.smarthome.devices.IWindowAlarmed;
import it.unibo.smarthome.utils.JadeUtils;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.AgentState;
import jade.wrapper.StaleProxyException;

public class WindowAlarmedSmart extends BaseDevice implements IWindowAlarmed {
	
	private static Logger log = LogManager.getLogger();
	private AgentController alarmSenderCtrl;
	private AgentController alarmStateCtrl;
	
	private WINDOW_STATE statoFinestra;
	private ALARM_MODE_STATE statoAllarme;
	
	public WindowAlarmedSmart(AgentContainer agentsContainer, String tname) {
		super(agentsContainer, tname);
		this.statoFinestra = WINDOW_STATE.CLOSE;
		this.statoAllarme = ALARM_MODE_STATE.DISABLE;
		this.initDataStore();
	}

	@Override
	public void setupAgent() {
		try {
			// Avvio l'agente che gestisce il cambio di stato
			alarmStateCtrl = JadeUtils.createNewAgent(
					this.getAgentsContainer(),
					"windowAlarmStateAgent",
					WindowAlarmStateAgent.class,
					new Object[] { this.getTname(), this }
					);
			alarmStateCtrl.start();
		} catch (StaleProxyException e) {
			log.error("sendAlarm, Errore durante l'avvio dell agente WindowAlarmStateAgent: " + e.getMessage());
		}
	}

	@Override
	public boolean dispose() {
		boolean ret = super.dispose();
		try {
			if(alarmSenderCtrl != null) {
				alarmSenderCtrl.kill();
			}
			alarmStateCtrl.kill();
		} catch (StaleProxyException e) {
			log.error("Errore durante lo stop degli agenti: " + e.getMessage());
			ret = false;
		}
		return ret;
	}
	
	private void sendAlarm() {
		try {
			if(this.alarmSenderCtrl == null || this.alarmSenderCtrl.getState().getCode() == AgentState.cAGENT_STATE_DELETED) {
				// Avvio l'agente che gestisce l'invio del messaggio di allarme
				alarmSenderCtrl = JadeUtils.createNewAgent(
						this.getAgentsContainer(),
						"sendAlarmAgent",
						SendAlarmAgent.class,
						new Object[] { this.getTname(), this }
						);
				alarmSenderCtrl.start();
			} else if(this.alarmSenderCtrl.getState().getCode() != AgentState.cAGENT_STATE_ACTIVE) {
				alarmSenderCtrl.activate();
			}
		} catch (StaleProxyException e) {
			log.error("sendAlarm, Errore durante l'invio del messaggio di allarme: " + e.getMessage());
		}
	}

	@Override
	public void doTestAction(String action) {
		log.debug("doTestAction, Test avviato, action: " + action);
		switch(action) {
		case "test-alarm":
			this.statoFinestra = WINDOW_STATE.CLOSE;
			this.statoAllarme = ALARM_MODE_STATE.ENABLE;
			updateDataStore();
			notifyOpenedWindow();
			break;
		case "open-window":
			notifyOpenedWindow();
			break;
		case "close-window":
			notifyClosedWindow();;
			break;
		}
	}

	@Override
	public void notifyOpenedWindow() {
		log.info(this.getName() + " - notifyOpenedWindow, finestra aperta");
		this.statoFinestra = WINDOW_STATE.OPEN;
		updateDataStoreWindowState();
		if(this.statoAllarme.equals(ALARM_MODE_STATE.ENABLE)) {
			sendAlarm();
			log.info(this.getName() + " - notifyOpenedWindow, alarmSenderCtrl attivato");
		}
	}

	@Override
	public void notifyClosedWindow() {
		log.info(this.getName() + " - notifyOpenedWindow, finestra chiusa");
		this.statoFinestra = WINDOW_STATE.CLOSE;
		updateDataStoreWindowState();
	}

	private void initDataStore() {
		updateDataStore();
	}
	
	private void updateDataStoreWindowState() {
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(WINDOW_DATA_STORE_KEYS.STATE_WINDOW.getValue(), this.statoFinestra.getValue());
	}
	
	private void updateDataStoreAlarmState() {
		ConcurrentHashMap<String, Object> dataStoreMap = this.getDataStore();
		dataStoreMap.put(WINDOW_DATA_STORE_KEYS.STATE_ALARM.getValue(), this.statoAllarme.getValue());
	}
	
	private void updateDataStore() {
		updateDataStoreWindowState();
		updateDataStoreAlarmState();
	}

	@Override
	public void updateConfigs() {		
	}

	@Override
	public void enableAlarm() {
		log.info(this.getName() + " - enableAlarm, finestra allarmata");
		this.statoAllarme = ALARM_MODE_STATE.ENABLE;
		updateDataStoreAlarmState();
		if(this.statoFinestra.equals(WINDOW_STATE.OPEN)) {
			sendAlarm();
			log.info(this.getName() + " - enableAlarm, alarmSenderCtrl attivato");
		}
	}

	@Override
	public void disableAlarm() {
		log.info(this.getName() + " - disableAlarm, finestra disallarmata");
		this.statoAllarme = ALARM_MODE_STATE.DISABLE;
		updateDataStoreAlarmState();
	}
}