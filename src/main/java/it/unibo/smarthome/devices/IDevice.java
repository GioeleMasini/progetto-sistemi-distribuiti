package it.unibo.smarthome.devices;

import it.unibo.smarthome.devices.events.IDeviceEventListener;

public interface IDevice {

	public boolean start();
	public boolean dispose();

	public String getId();
	public void setId(String id);
	
	public String getRoomId();
	public void setRoomId(String roomId);

	public String getName();
	public void setName(String deviceName);
	
	/**
	 * Funzione per aggiornamento valori di config.
	 * Vuota per device che non hanno config.
	 */
	public void updateConfigs();
	
	/**
	 * Permette di registrare dei listener agli eventi del device
	 */
	public void addListener(IDeviceEventListener listener);
	public void removeListener(IDeviceEventListener listener);
}
