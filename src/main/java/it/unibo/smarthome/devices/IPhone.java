package it.unibo.smarthome.devices;

public interface IPhone extends IDevice {

	public static final String TUPLE_PHONE_SMS_TEMPLATE = "num(X),msg(Y)";
	
	public boolean call(String number);
	public void sendSMS(String number, String text);
	public void sendMMS(String number, Byte[] foto);
	public boolean checkSignal();
}
