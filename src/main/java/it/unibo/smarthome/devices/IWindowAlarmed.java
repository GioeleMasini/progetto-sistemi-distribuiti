package it.unibo.smarthome.devices;

public interface IWindowAlarmed extends IDevice {
	
	public void notifyOpenedWindow();
	public void notifyClosedWindow();
	
	public void enableAlarm();
	public void disableAlarm();
}
